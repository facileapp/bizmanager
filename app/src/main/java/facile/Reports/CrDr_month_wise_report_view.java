package facile.Reports;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.Monthly_report_adapter;
import facile.invoice.R;
import facile.util.Constants;
import facile.util.SimpleDividerItemDecoration;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CrDr_month_wise_report_view.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CrDr_month_wise_report_view#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CrDr_month_wise_report_view extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RequestQueue requestQueue;
    public static final String UTF8_BOM = "\uFEFF";
    ProgressDialog progressDialog;
    String url;
    prefManager prefs;
    View layouttoast;
    TextView toasttext;
    String str_closuredate,str_sorp;
    ArrayList<HashMap<String, String>> reporthashmap;
    String str_year,str_month;

    private Month_wise_report.OnFragmentInteractionListener mListener;
    RecyclerView rv_monthlyreport;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    String str_crordr;

    public CrDr_month_wise_report_view() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CrDr_month_wise_report_view.
     */
    // TODO: Rename and change types and number of parameters
    public static CrDr_month_wise_report_view newInstance(String param1, String param2) {
        CrDr_month_wise_report_view fragment = new CrDr_month_wise_report_view();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_month_wise_report_view, container, false);
        rv_monthlyreport=(RecyclerView)view.findViewById(R.id.rv_monthlyreport);

        Bundle bundle = this.getArguments();
        str_month = bundle.getString("str_month", "");
        str_year= bundle.getString("str_year", "");
        str_crordr=bundle.getString("CRORDR", "");

        if(str_crordr.equals("DR")) {
            str_sorp = bundle.getString("DR_SORP", "");
        }


        prefs = new prefManager(getContext());
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);

        if(str_sorp.equals("P")){
            url = Constants.PURCHASE_DR_MONTH_WISE_REPORT;



        }else {
            url = Constants.CRDR_MONTH_WISE_REPORT;
        }
        requestQueue = Volley.newRequestQueue(view.getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Generating report...");
        progressDialog.show();
        str_closuredate=str_year+"/"+str_month+"/"+"01";
        rv_monthlyreport.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        rv_monthlyreport.setLayoutManager(mLayoutManager);
        rv_monthlyreport.addItemDecoration(new SimpleDividerItemDecoration(getContext()));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String m=response.toString();
                            String x=     removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                            String   val = jsonRequest.getString("status");
                            if (statuscode.equals("200")) {
                                JSONObject json = new JSONObject(jsonRequest.toString());
                                if (json.isNull("report")) {

                                } else {

                                    if(str_crordr.equals("CR")){


                                    JSONArray report_list = jsonRequest.getJSONArray("report");
                                    reporthashmap = new ArrayList<HashMap<String, String>>();
                                    for (int iStatusList = 0; iStatusList < report_list.length(); iStatusList++) {
                                        JSONObject report = report_list.getJSONObject(iStatusList);
                                        HashMap<String, String> userhashmap = new HashMap<String, String>();
                                        userhashmap.put("org_invoice_no", report.getString("org_invoice_no"));
                                        userhashmap.put("credit_note_no", report.getString("credit_note_no"));
                                        userhashmap.put("credit_note_date", report.getString("credit_note_date"));
                                        userhashmap.put("customer_name", report.getString("customer_name"));
                                        userhashmap.put("gst_no", report.getString("gst_no"));
                                        userhashmap.put("hsn_code", report.getString("hsn_code"));
                                        userhashmap.put("gst_rate", report.getString("gst_rate"));
                                        userhashmap.put("total_before_tax", report.getString("total_before_tax"));
                                        userhashmap.put("igst", report.getString("igst"));
                                        userhashmap.put("cgst", report.getString("cgst"));
                                        userhashmap.put("sgst", report.getString("sgst"));
                                        userhashmap.put("tat", report.getString("tat"));
                                        reporthashmap.add(userhashmap);
                                    }



                                    }
                                    else{
                                        JSONArray report_list = jsonRequest.getJSONArray("report");
                                        reporthashmap = new ArrayList<HashMap<String, String>>();
                                        for (int iStatusList = 0; iStatusList < report_list.length(); iStatusList++) {
                                            JSONObject report = report_list.getJSONObject(iStatusList);
                                            HashMap<String, String> userhashmap = new HashMap<String, String>();
                                            userhashmap.put("org_invoice_no", report.getString("org_invoice_no"));
                                            userhashmap.put("debit_note_no", report.getString("debit_note_no"));
                                            userhashmap.put("debit_note_date", report.getString("debit_note_date"));

                                                userhashmap.put("customer_name", report.getString("customer_name"));

                                            userhashmap.put("gst_no", report.getString("gst_no"));
                                            userhashmap.put("hsn_code", report.getString("hsn_code"));
                                            userhashmap.put("gst_rate", report.getString("gst_rate"));
                                            userhashmap.put("total_before_tax", report.getString("total_before_tax"));
                                            userhashmap.put("igst", report.getString("igst"));
                                            userhashmap.put("cgst", report.getString("cgst"));
                                            userhashmap.put("sgst", report.getString("sgst"));
                                            userhashmap.put("tat", report.getString("tat"));
                                            reporthashmap.add(userhashmap);
                                        }


                                    }
                                    mAdapter = new Monthly_report_adapter(getContext(),reporthashmap,str_crordr);
                                    rv_monthlyreport.setAdapter(mAdapter);
                                }

                                progressDialog.dismiss();


                            }
                            else   if (statuscode.equals("600")){
                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(val);
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();

                            }

                        } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                            progressDialog.dismiss();


                        }
                    }
                },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(view.getContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layouttoast);
                        progressDialog.dismiss();
                        toast.show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",prefs.getUserId());
                params.put("month",str_month);
                params.put("year",str_year);
                params.put("CRORDR",str_crordr);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);




        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.popup_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_csv:

                progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
                progressDialog.setMessage("Generating CSV...");
                progressDialog.show();

                File mypath;
                File dir;

                File filepath = Environment.getExternalStorageDirectory();

                // Create a new folder in SD Card
                if(str_crordr.equals("CR")) {
                     dir = new File(filepath.getAbsolutePath()
                            + "/BizManager/Reports/Monthly_report/Credit Note/");
                }else{
                    if(str_sorp.equals("P")){
                        dir = new File(filepath.getAbsolutePath()
                                + "/BizManager/Reports/Monthly_report/Debit Note/purchase");
                    }
                    else {
                        dir = new File(filepath.getAbsolutePath()
                                + "/BizManager/Reports/Monthly_report/Debit Note/sales");
                    }
                }
                dir.mkdirs();

                mypath = new File(dir, str_month+"_"+str_year+".csv");

                try {

                    FileWriter fw = new FileWriter(mypath);




                    fw.append("org_invoice_no");
                    fw.append(',');


                    if(str_crordr.equals("CR")){

                        fw.append("credit_note_no");
                        fw.append(',');

                        fw.append("credit_note_date");
                        fw.append(',');

                    }else{

                        fw.append("debit_note_no");
                        fw.append(',');

                        fw.append("debit_note_date");
                        fw.append(',');

                    }

                    if(str_sorp.equals("P")) {

                        fw.append("Supplier_name");
                        fw.append(',');
                    }
                    else{

                        fw.append("customer_name");
                        fw.append(',');
                    }

                    fw.append("gst_no");
                    fw.append(',');

                    fw.append("hsn_code");
                    fw.append(',');

                    fw.append("gst_rate");
                    fw.append(',');

                    fw.append("total_before_tax");
                    fw.append(',');

                    fw.append("igst");
                    fw.append(',');

                    fw.append("cgst");
                    fw.append(',');

                    fw.append("sgst");
                    fw.append(',');

                    fw.append("tat");
                    fw.append(',');

                    fw.append('\n');


                    for (int i = 0; i < reporthashmap.size(); i++) {
                        fw.append(reporthashmap.get(i).get("org_invoice_no").toString());
                        fw.append(',');

                        if(str_crordr.equals("CR")){

                            fw.append(reporthashmap.get(i).get("credit_note_no").toString());
                            fw.append(',');

                            fw.append(reporthashmap.get(i).get("credit_note_date").toString());
                            fw.append(',');

                        }



                       else if(str_crordr.equals("DR")){

                            fw.append(reporthashmap.get(i).get("debit_note_no").toString());
                            fw.append(',');

                            fw.append(reporthashmap.get(i).get("debit_note_date").toString());
                            fw.append(',');

                        }


                            fw.append(reporthashmap.get(i).get("customer_name").toString());

                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("gst_no").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("hsn_code").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("gst_rate").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("total_before_tax").toString());
                        fw.append(',');



                        fw.append(reporthashmap.get(i).get("igst").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("cgst").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("sgst").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("tat").toString());
                        fw.append(',');


                        fw.append('\n');

                    }
                    // fw.flush();
                    fw.close();
                    progressDialog.dismiss();

                } catch (Exception e) {
                    progressDialog.dismiss();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);


        }
    }
}


