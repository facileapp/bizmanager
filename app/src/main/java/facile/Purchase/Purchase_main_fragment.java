package facile.Purchase;

/**
 * Created by pradeep on 12/2/18.
 */

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Adapter.SalesgridAdapter;
import facile.Model.sales_model;
import facile.creditanddebitnote.Debit_note_purchase;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;



public class Purchase_main_fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView toasttext;
    Invoicedatabase invoicedatabase;
    GridView gv_sales;
    ArrayList<sales_model> salesArrayList;

    View layouttoast;
    Button btn_invoice,btn_debitnote,btn_creditnote;
    SalesgridAdapter customadapter;


    private OnFragmentInteractionListener mListener;

    public Purchase_main_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Purchase_main_fragment newInstance(String param1, String param2) {
        Purchase_main_fragment fragment = new Purchase_main_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sales_main, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.nav_item_purchase));
        invoicedatabase=new Invoicedatabase(getContext());
        invoicedatabase.deleteaddproductitems();
        gv_sales=(GridView)view.findViewById(R.id.gv_sales);
        salesArrayList=new ArrayList<>();
        String salestitle[]={"Purchase","Supplier","Debit Note","Purchase List"};
/*        String salesid[]={"1","2","3","4","5","6"};*/
        int salesitemsimg[]={R.drawable.purchase,R.drawable.supplier,R.drawable.debitnote,R.drawable.invoicelist
               };
        for(int i=0;i<salestitle.length;i++){
            sales_model m=new sales_model();
/*            m.setSales_id(salesid[i]);*/
            m.setSales_title(
                    salestitle[i]);
            m.setSalesimg(salesitemsimg[i]);
            salesArrayList.add(m);
        }
        customadapter=new SalesgridAdapter(getActivity(),salesArrayList);
        gv_sales.setAdapter(customadapter);
        gv_sales.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    Fragment fragment=new Purchase_bill_main();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("GET_INVOICENODATE","1");
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();
                }


                if(position==1){
                    Fragment fragment=new Supplier_main();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }

                if(position==2){
                    Fragment fragment=new Debit_note_purchase();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("GET_INVOICENODATE","1");
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();
                }
                if(position==3){
                    invoicedatabase.deleteinvoicelist();
                    Purchase_list fragment = new Purchase_list();
                    FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }



            }
        });
     /*   btn_invoice=(Button)view.findViewById(R.id.btn_invoice);
        btn_creditnote=(Button)view.findViewById(R.id.btn_creditnote);
        btn_debitnote=(Button)view.findViewById(R.id.btn_debitnote);
        btn_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new Invoice_main_Fragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
            *//*    Bundle bundle = new Bundle();
                bundle.putString("GET_INVOICENODATE","1");
                fragment.setArguments(bundle);*//*
                fragmentTransaction.commit();
            }
        });

        btn_creditnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new Credit_note_number();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
            *//*    Bundle bundle = new Bundle();
                bundle.putString("GET_INVOICENODATE","1");
                fragment.setArguments(bundle);*//*
                fragmentTransaction.commit();

            }
        });
        btn_debitnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new Debit_note_number();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
            *//*    Bundle bundle = new Bundle();
                bundle.putString("GET_INVOICENODATE","1");
                fragment.setArguments(bundle);*//*
                fragmentTransaction.commit();
            }
        });

*/

     /*   ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
             /*   MobileAds.initialize(getActivity(), "ca-app-pub-2213723827168530~3447777035");*/
        // Setting ViewPager for each Tabs

        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

