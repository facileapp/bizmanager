package facile.invoice;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import facile.Adapter.AutoCompleteCusomerAdapter;
import facile.Adapter.StatelistArrayAdapter;
import facile.Adapter.TransportlistArrayAdapter;
import facile.Adapter.additemadapterunreg;
import facile.Model.Invoice_Product_model;
import facile.Model.Invoice_calc;
import facile.Model.Invoice_details;
import facile.Model.invoice_customer_model;
import facile.localdb.Invoicedatabase;
import facile.pdf.ImagetoPdf;
import facile.util.DatePickerFragment;
import facile.util.SeparatorDecoration;
import facile.util.prefManager;

/**
 * Created by pradeep on 03/4/18.
 */

public class Invoice_fragment_unregistered extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView toasttext;
    String str_date;
    View layouttoast;
    Button submit, additem,btn_total;
    EditText edt_inv_date;

    AutoCompleteCusomerAdapter adapter;
    additemadapterunreg Itemadapter;
    RecyclerView mRecyclerView;
    Invoicedatabase invoicedb;
    Spinner spn_transport;
    ArrayList<Invoice_Product_model> invoicproductemodel;
    private RecyclerView.LayoutManager mLayoutManager;
    String str_transportmode;
    String str_customername;
    RadioGroup radioGroup;
    RadioButton rb_cash,rb_credit;
    String sales="x",transport_mode="1";
    TextView tv_invoiceno;
    CheckBox chk_rc;
    EditText edt_freight,edt_others,edt_address,edt_address1,edt_city,edt_pincode;
    prefManager prefs;
    String str_igst="N";
    Double freight=0.0,others=0.0,freightbt=0.0,othersbt=0.0;
    String Str_customer_name,gst_no="NA",address="",address1="",phone_no="",mail_id="",city="",state,pincode="",state_pos,
            str_invoiceno,str_p_invoiceno_s;
    RequestQueue requestQueue;
    public static final String UTF8_BOM = "\uFEFF";
    String  str_dateval;
    Spinner   spn_state;
    String str_statepos;
    ProgressDialog progressDialog;
    String url;
     double temp;
    TextView txt_totbeforetax,txt_totaftertax,txt_tax;
    TextView tv_totalaftertax,tv_tax,tv_totalbeforetax;

    private Invoice_fragment_unregistered.OnFragmentInteractionListener mListener;

    public Invoice_fragment_unregistered() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Invoice_fragment_unregistered newInstance(String param1, String param2) {
        Invoice_fragment_unregistered fragment = new Invoice_fragment_unregistered();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.invoice_main_consumer, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_invoice));
        prefs = new prefManager(getContext());
        edt_inv_date = (EditText) view.findViewById(R.id.edt_invoicedate);
        tv_invoiceno=(TextView)view.findViewById(R.id.tv_invoicenumber);
        additem = (Button) view.findViewById(R.id.btn_additem);
        btn_total = (Button) view.findViewById(R.id.btn_total);
        tv_totalaftertax=(TextView)view.findViewById(R.id.tv_totalaftertax);
        tv_totalbeforetax=(TextView)view.findViewById(R.id.tv_totalbeforetax);
        tv_tax=(TextView)view.findViewById(R.id.tv_tax);
        txt_totaftertax=(TextView)view.findViewById(R.id.txt_totalaftertax);
        txt_totbeforetax=(TextView)view.findViewById(R.id.txt_totalbeforetax);
        txt_tax=(TextView)view.findViewById(R.id.txt_tax);
        spn_transport=(Spinner)view.findViewById(R.id.spn_transport);
        spn_state=(Spinner)view.findViewById(R.id.spn_state);
        radioGroup=(RadioGroup)view.findViewById(R.id.radioGroup);
        rb_cash=(RadioButton)view.findViewById(R.id.rb_cash);
        rb_credit=(RadioButton)view.findViewById(R.id.rb_credit);
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_invoiceitems);
        chk_rc=(CheckBox)view.findViewById(R.id.chk_reversecharge);
        edt_freight=(EditText)view.findViewById(R.id.edt_freight);
        edt_others=(EditText)view.findViewById(R.id.edt_others);
        edt_address=(EditText)view.findViewById(R.id.edt_address);
        edt_address1=(EditText)view.findViewById(R.id.edt_address1);
        edt_city=(EditText)view.findViewById(R.id.edt_city);
        edt_pincode=(EditText)view.findViewById(R.id.edt_pincode);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        SeparatorDecoration decoration = new SeparatorDecoration(getContext(), Color.GRAY,0.5f);
        mRecyclerView.addItemDecoration(decoration);
        final EditText edt_customername = (EditText) view.findViewById(R.id.edt_customername);

        if(prefs.getSales().equals("1")){
            rb_cash.setChecked(true);
            sales="1";
        }
        else{
            rb_credit.setChecked(true);
            sales="2";
        }

        if(prefs.getBusinessNature().equals("2")){
            if(prefs.getServiceType().equals("1")){

                additem.setText(R.string.add_service);
            }
            else   if(prefs.getServiceType().equals("2")){
                additem.setText(R.string.add_service);

            }
            else  if(prefs.getServiceType().equals("3")){
                additem.setText(R.string.add_service);
            }



        }

        if(prefs.getBusinessType().equals("2")){
            txt_totaftertax.setText("Total:");
            txt_totbeforetax.setVisibility(View.GONE);
            txt_tax.setVisibility(View.GONE);
            tv_tax.setVisibility(View.GONE);
            tv_totalbeforetax.setVisibility(View.GONE);
        }
        str_p_invoiceno_s=prefs.getPrefix()+prefs.getInvoiceNo()+prefs.getSuffix();

        tv_invoiceno.setText(str_p_invoiceno_s);
        invoicedb = new Invoicedatabase(getContext());
        invoicproductemodel=new ArrayList<Invoice_Product_model>();
        long date = System.currentTimeMillis();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
        dateFormat.applyPattern("dd/MM/yy");
        String dateString = dateFormat.format(date);
        edt_inv_date.setText(dateString);
     /*   SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy:MM:dd");
        dateFormat1.applyPattern("yyyy/MM/dd");
        str_dateval=dateFormat1.format(date);
*/

        final StatelistArrayAdapter adapter = new StatelistArrayAdapter(getActivity(),
                R.layout.state_list_row, invoicedb.getstates());
        spn_state.setAdapter(adapter);

        spn_state.setSelection(Integer.parseInt(prefs.getConsumerState())-1);





        final TransportlistArrayAdapter adapter1 = new TransportlistArrayAdapter(getContext(),
                R.layout.dealer_list_row, invoicedb.gettransportmode());
        spn_transport.setAdapter(adapter1);
        spn_transport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                /*str_transportmode=((TextView) v.findViewById(R.id.tv_dealerid)).getText().toString();*/
                str_transportmode=String.valueOf(position+1);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.rb_cash:
                        // switch to fragment 1
                        sales="1";
                        break;
                    case R.id.rb_credit:
                        // Fragment 2
                        sales="2";
                        break;
                }
            }
        });

       /*Bundle bundle = this.getArguments();
        if (bundle != null) {*/

        Itemadapter = new additemadapterunreg(invoicedb.getaddproductitems(),getActivity());
        mRecyclerView.setAdapter(Itemadapter);
        ((additemadapterunreg) Itemadapter).setOnItemClickListener(new additemadapterunreg.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Additem_fragment fragment = new Additem_fragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("prod_code",invoicedb.getaddproductitems().get(position).productcode);
                bundle.putString("prod_desc",invoicedb.getaddproductitems().get(position).productdesc);
                bundle.putString("prod_fees",invoicedb.getaddproductitems().get(position).amount);
                bundle.putString("prod_qty",invoicedb.getaddproductitems().get(position).quantity);
                bundle.putString("prod_disc",invoicedb.getaddproductitems().get(position).discount);
                bundle.putString("prod_disc_type",invoicedb.getaddproductitems().get(position).discountype);
                bundle.putString("prod_unit",invoicedb.getaddproductitems().get(position).unit);
                bundle.putString("gst_rate",invoicedb.getaddproductitems().get(position).gstrate);
                bundle.putString("Igst",str_igst);
                bundle.putString("type","I");
                bundle.putString("business_type",prefs.getBusinessType());
                    /*    bundle.putString("ITEM_CODE", );
                bundle.putString("ITEM_DESC", invoicedb.getcustomerdetails().get(position).gst_no);
                bundle.putString("SP", invoicedb.getcustomerdetails().get(position).address);
                bundle.putString("QTY", invoicedb.getcustomerdetails().get(position).city);
                bundle.putString("DISCOUNT", invoicedb.getcustomerdetails().get(position).pincode);*/
                fragment.setArguments(bundle);
                fragmentTransaction.commit();


            }
        });


        spn_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

               /* str_state=((TextView) v.findViewById(R.id.tv_statecode)).getText().toString();*/
                str_statepos=String.valueOf(position+1);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        //  }



        edt_inv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showDatePicker();

            }
        });

     /*   adapter = new AutoCompleteCusomerAdapter(getContext(),R.layout.autocomplete_row,invoicedb.getcustomerdetails());
        edt_customername.setAdapter(adapter);
        edt_customername.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                Customer_model model = (Customer_model) view.getTag();

                edt_customername.setText(model.customer_name);


                if(model.state.equals(prefs.getState())){
                    str_igst="N";
                }
                else{
                    str_igst="Y";
                }


                Str_customer_name=model.customer_name;
                gst_no=model.gst_no;
                address=model.address;
                phone_no=model.phone_no;
                mail_id=model.mail_id;
                city=model.city;
                state=model.state;
                pincode=model.pincode;
                state_pos=model.state_pos;





            }
        });
*/

        additem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(str_statepos.equals(prefs.getState())){
                    str_igst="N";
                }
                else{
                    str_igst="Y";
                }


                    Additem_fragment fragment = new Additem_fragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("Igst",str_igst);
                    bundle.putString("type","I");
                    bundle.putString("business_type",prefs.getBusinessType());
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();


            }
        });

       submit=(Button)view.findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Str_customer_name=edt_customername.getText().toString();
                state=invoicedb.getvaluebystateposition(str_statepos);
                int m=invoicedb.getaddproductitems().size();
                address=edt_address.getText().toString();
                address1=edt_address1.getText().toString();
                city=edt_city.getText().toString();
                pincode=edt_pincode.getText().toString();
                for(int i=0;i<m;i++){
                    int x= Integer.parseInt(invoicedb.getaddproductitems().get(i).gstrate);
                    if(x>temp){
                        temp=x;

                    }
               }

               if(invoicedb.getaddproductitemscount()==0){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Add Items can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    return;
                }
                else {
                   String str_invoice_date = edt_inv_date.getText().toString();


                   SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
                   dateFormat.applyPattern("dd/MM/yy");
                   SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy:MM:dd");
                   dateFormat1.applyPattern("yyyy/MM/dd");

                   try {
                       Date date = dateFormat.parse(str_invoice_date);
                       str_dateval = dateFormat1.format(date);




                   }
                   catch(Exception e){

                   }
                    String str_rc;
                    String str_freight;
                    String str_others;
                    String total_before_tax;
                    String total_after_tax;
                    String str_cgst, str_sgst, str_igstval;
                    str_invoiceno=prefs.getInvoiceNo();
                    double totbeforetax = 0, totaftertax = 0, cgst = 0, sgst = 0, igst = 0;
                    str_freight = edt_freight.getText().toString();
                    str_others = edt_others.getText().toString();
                    for (int i = 0; i < invoicedb.getaddproductitemscount(); i++) {
                        double TOT_BT = totbeforetax + Double.parseDouble(invoicedb.getaddproductitems().get(i).grossamount);
                        double TOT_AT = totaftertax + Double.parseDouble(invoicedb.getaddproductitems().get(i).tat);
                        double CGST = cgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).cgst);
                        double SGST = sgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).sgst);
                        double IGST = igst + Double.parseDouble(invoicedb.getaddproductitems().get(i).igst);
                        totbeforetax = TOT_BT;
                        totaftertax = TOT_AT;
                        cgst = CGST;
                        sgst = SGST;
                        igst = IGST;
                    }
                   if(prefs.getBusinessType().equals("2")) {

                       if (TextUtils.isEmpty(str_freight)) {
                           freightbt = 0.0;
                       } else {
                           freightbt = Double.parseDouble(str_freight);
                           freight=freightbt;
                       }

                       if (TextUtils.isEmpty(str_others)) {
                           othersbt = 0.0;
                       } else {
                           othersbt = Double.parseDouble(str_freight);
                           others=freightbt;
                       }
                   }
                   else{

                   if (TextUtils.isEmpty(str_freight)) {
                       freightbt=0.0;
                   } else {
                       freightbt=Double.parseDouble(str_freight);
                       double xy= temp/100;
                       double freighttax= Double.parseDouble(str_freight)*xy;
                       Double strfreightval=Double.parseDouble(str_freight)+freighttax;


                       if(str_igst.equals("N")){

                           double CGST = cgst + Double.parseDouble(String.valueOf(freighttax/2));
                           double SGST = sgst + Double.parseDouble(String.valueOf(freighttax/2));
                           cgst=CGST;
                           sgst=SGST;
                           freight=freightbt+Double.parseDouble(String.valueOf(freighttax/2))+Double.parseDouble(String.valueOf(freighttax/2));
                       }else{
                           double IGST = igst + Double.parseDouble(String.valueOf(freighttax));
                           igst=IGST;

                           freight=freightbt+Double.parseDouble(String.valueOf(freighttax));

                       }



                   }
                   if (TextUtils.isEmpty(str_others)){
                       othersbt=0.0;
                   }
                   else {
                       othersbt = Double.parseDouble(str_others);
                       double xy = temp / 100;
                       double otherstax = Double.parseDouble(str_others) * xy;
                       Double strothersval = Double.parseDouble(str_others) + otherstax;

                       if (str_igst.equals("N")) {

                           double CGST = cgst + Double.parseDouble(String.valueOf(otherstax / 2));
                           double SGST = sgst + Double.parseDouble(String.valueOf(otherstax / 2));
                           cgst = CGST;
                           sgst = SGST;
                           others = othersbt + Double.parseDouble(String.valueOf(otherstax / 2)) + Double.parseDouble(String.valueOf(otherstax / 2));
                       } else {
                           double IGST = igst + Double.parseDouble(String.valueOf(otherstax));
                           igst = IGST;

                           others = othersbt + Double.parseDouble(String.valueOf(otherstax));

                       }
                   }
                   }

                     total_before_tax=String.valueOf(Math.round((totbeforetax+freightbt+othersbt)*100.0)/100.0);
                    total_after_tax=String.valueOf(Math.round((totaftertax+freight+others)*100.0)/100.0);
                    str_cgst=String.valueOf(Math.round(cgst*100.0)/100.0);
                    str_sgst=String.valueOf(Math.round(sgst*100.0)/100.0);
                    str_igstval=String.valueOf(Math.round(igst*100.0)/100.0);
                    invoice_customer_model invoice_customer_model=new invoice_customer_model
                            (Str_customer_name,gst_no,address,address1,phone_no,mail_id,city,state,pincode,str_statepos,str_igst);
                    Invoice_details invoice=new Invoice_details(sales,str_invoice_date,str_transportmode,"1",str_invoiceno,str_p_invoiceno_s,str_dateval,"C");
                    Invoice_calc invoiceCalc=new Invoice_calc(str_freight,str_others,str_cgst,str_sgst,str_igstval,total_before_tax,total_after_tax);
                    Intent myIntent = new Intent(getActivity(),ImagetoPdf.class);
                    myIntent.putExtra("invoice",invoice);
                    myIntent.putExtra("invoice_calc",invoiceCalc);
                    myIntent.putExtra("customer",invoice_customer_model);
                    getActivity().startActivity(myIntent);


                }
            }
        });





        btn_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int m=invoicedb.getaddproductitems().size();

                for(int i=0;i<m;i++){
                    int x= Integer.parseInt(invoicedb.getaddproductitems().get(i).gstrate);
                    if(x>temp){
                        temp=x;
                    }
                }


                String str_freight;
                String str_others;
                String total_before_tax;
                String total_after_tax;
                String str_cgst, str_sgst, str_igstval;



                double totbeforetax = 0, totaftertax = 0, cgst = 0, sgst = 0, igst = 0;

                str_freight = edt_freight.getText().toString();
                str_others = edt_others.getText().toString();
                for (int i = 0; i < invoicedb.getaddproductitemscount(); i++) {
                    double TOT_BT = totbeforetax + Double.parseDouble(invoicedb.getaddproductitems().get(i).grossamount);
                    double TOT_AT = totaftertax + Double.parseDouble(invoicedb.getaddproductitems().get(i).tat);
                    double CGST = cgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).cgst);
                    double SGST = sgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).sgst);
                    double IGST = igst + Double.parseDouble(invoicedb.getaddproductitems().get(i).igst);
                    totbeforetax = TOT_BT;
                    totaftertax = TOT_AT;
                    cgst = CGST;
                    sgst = SGST;
                    igst = IGST;
                }
                if (TextUtils.isEmpty(str_freight)) {
                    freightbt=0.0;
                } else {
                    freightbt=Double.parseDouble(str_freight);
                    double xy= temp/100;
                    double freighttax= Double.parseDouble(str_freight)*xy;



                    if(str_igst.equals("N")){

                        double CGST = cgst + Double.parseDouble(String.valueOf(freighttax/2));
                        double SGST = sgst + Double.parseDouble(String.valueOf(freighttax/2));
                        cgst=CGST;
                        sgst=SGST;
                        freight=freightbt+Double.parseDouble(String.valueOf(freighttax/2))+Double.parseDouble(String.valueOf(freighttax/2));
                    }else{
                        double IGST = igst + Double.parseDouble(String.valueOf(freighttax));
                        igst=IGST;

                        freight=freightbt+Double.parseDouble(String.valueOf(freighttax));

                    }



                }
                if (TextUtils.isEmpty(str_others)){
                    othersbt=0.0;
                }
                else{
                    othersbt=Double.parseDouble(str_others);
                    double xy=temp/100;
                    double otherstax=Double.parseDouble(str_others)*xy;


                    if(str_igst.equals("N")){

                        double CGST = cgst + Double.parseDouble(String.valueOf(otherstax/2));
                        double SGST = sgst + Double.parseDouble(String.valueOf(otherstax/2));
                        cgst=CGST;
                        sgst=SGST;
                        others=othersbt+Double.parseDouble(String.valueOf(otherstax/2))+Double.parseDouble(String.valueOf(otherstax/2));
                    }else{
                        double IGST = igst + Double.parseDouble(String.valueOf(otherstax));
                        igst=IGST;

                        others=othersbt+Double.parseDouble(String.valueOf(otherstax));

                    }
                }

                if(prefs.getBusinessType().equals("2")){
                    total_before_tax = String.valueOf(Math.round((totbeforetax + freightbt + othersbt) * 100.0) / 100.0);

                    tv_totalaftertax.setText(total_before_tax);



                }else {

                    total_before_tax = String.valueOf(Math.round((totbeforetax + freightbt + othersbt) * 100.0) / 100.0);
                    total_after_tax = String.valueOf(Math.round((totaftertax + freight + others) * 100.0) / 100.0);


                    tv_totalaftertax.setText(total_after_tax);
                    tv_totalbeforetax.setText(total_before_tax);
                    if (str_igst.equals("N")) {
                        double taxval = cgst + sgst;
                        str_cgst = String.valueOf(Math.round(taxval * 100.0) / 100.0);
                        tv_tax.setText(str_cgst);
                    } else {
                        str_igstval = String.valueOf(Math.round(igst * 100.0) / 100.0);
                        tv_tax.setText(str_igstval);

                    }

                }


            }
        });



        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        args.putString("Invoicemindate",prefs.getInvoiceMinDate());
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */

        date.setCallBack(ondate);

        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
                dateFormat.applyPattern("dd/MM/yy");
                str_date = dateFormat.format(calendar.getTime());
                edt_inv_date.setText(str_date);
             /*   SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy:MM:dd");
                dateFormat1.applyPattern("yyyy/MM/dd");
                str_dateval=dateFormat1.format(calendar.getTime());*/
            }
            catch (Exception e){

            }


        }
    };



    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.popup_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


  


}

