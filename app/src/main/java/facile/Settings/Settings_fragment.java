package facile.Settings;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import facile.Signin.SigninActivity;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.util.prefManager;

/**
 * Created by pradeep on 14/2/18.
 */

public class Settings_fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    TextView profile,changepassword,bankdetails,appprerences,logout,email,privacy_policy,terms_of_use;
    prefManager prefs;


    private Settings_fragment.OnFragmentInteractionListener mListener;

    public Settings_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Settings_fragment newInstance(String param1, String param2) {
        Settings_fragment fragment = new Settings_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_settings));
        prefs=new prefManager(getContext());
        profile=(TextView)view.findViewById(R.id.tv_profile);
        changepassword=(TextView)view.findViewById(R.id.tv_changepassword);
        bankdetails=(TextView)view.findViewById(R.id.txt_bankdetails);
        appprerences=(TextView)view.findViewById(R.id.tv_preferences);
        logout=(TextView)view.findViewById(R.id.tv_logout);
        email=(TextView)view.findViewById(R.id.txt_email);
        privacy_policy=(TextView)view.findViewById(R.id.txt_privacy);
        terms_of_use=(TextView)view.findViewById(R.id.txt_termsofuse);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(getActivity(), profile.class);
                getActivity().startActivity(myIntent);

            }
        });
        appprerences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(getActivity(), App_Preference.class);
                getActivity().startActivity(myIntent);

            }
        });

        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(getActivity(),Change_password.class);
                getActivity().startActivity(myIntent);

            }
        });
        bankdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(getActivity(),Bank_details.class);
                getActivity().startActivity(myIntent);

            }
        });


        privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(getActivity(),Privacy_policy.class);
                getActivity().startActivity(myIntent);

            }
        });

        terms_of_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(getActivity(),Terms_of_use.class);
                getActivity().startActivity(myIntent);

            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(getActivity(),SigninActivity.class);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                prefs.setIsLoggedIn("1");
                getActivity().startActivity(myIntent);
                getActivity().finish();

            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Customer Support");

                emailIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"facilebizmanager@gmail.com"});

                emailIntent.setType("message/rfc822");

                startActivity(Intent.createChooser(emailIntent, "Choose an Email client :"));


                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));


                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }


            }
        });


     /*   ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
             /*   MobileAds.initialize(getActivity(), "ca-app-pub-2213723827168530~3447777035");*/
        // Setting ViewPager for each Tabs

        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

