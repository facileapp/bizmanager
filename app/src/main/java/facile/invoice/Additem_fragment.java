package facile.invoice;

/**
 * Created by pradeep on 12/2/18.
 */

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import facile.Adapter.AutoCompleteItemAdapter;
import facile.Model.Product_model;
import facile.creditanddebitnote.Creditnote;
import facile.creditanddebitnote.Debit_note_purchase;
import facile.localdb.Invoicedatabase;
import facile.util.prefManager;


/**
 * Created by pradeep on 27/8/17.
 */

public class Additem_fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    AutoCompleteItemAdapter Itemadapter;
    Invoicedatabase invoicedb;
    ArrayList<HashMap<String, String>> addproductHashMap;
    private OnFragmentInteractionListener mListener;
    String str_prodouctcode,str_productdesc,str_productunit,str_hsn,str_gstrate,str_cess,str_productdiscount,str_qty,
            str_productrate,str_producttotal;
    EditText edt_itemdesc,edt_itemamnt,edt_itemqty,edt_discount,edt_gstrate;
    AutoCompleteTextView edt_itemcode;
    TextView units;
    RadioGroup radioGroup;
    String discount_type="A";
    TextView toasttext;
    View layouttoast;
    String  flag="T";
    HashMap<String,String> val;
    RadioButton rb_amnt,rb_percent;
    TextView tv_qty,tv_gstrate;
    String insertorupdate="I";
    String str_igst;
    double total;
    prefManager prefs;
    String cgst,sgst,igst,total_after_tax;
    String cgst_percent,sgst_percent,igst_percent,str_type;
    String str_businesstype="NV";

    public Additem_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory m
     * ethod to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Additem_fragment newInstance(String param1, String param2) {
        Additem_fragment fragment = new Additem_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.additem_invoice, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_add_items));
        invoicedb = new Invoicedatabase(getContext());
        prefs = new prefManager(getContext());
        Button btn_add = (Button) view.findViewById(R.id.btn_add);
        edt_itemcode = (AutoCompleteTextView) view.findViewById(R.id.edt_itemcode);
        edt_itemdesc = (EditText) view.findViewById(R.id.edt_itemdesc);
        edt_itemamnt = (EditText) view.findViewById(R.id.edt_prodfees);
        edt_itemqty= (EditText) view.findViewById(R.id.edt_prodqty);
        edt_discount=(EditText)view.findViewById(R.id.edt_proddiscount);
        edt_gstrate=(EditText)view.findViewById(R.id.edt_gstrate) ;
        tv_qty=(TextView)view.findViewById(R.id.tv_prodqty) ;
        radioGroup = (RadioGroup)view. findViewById(R.id.radioGroup);
        rb_amnt=(RadioButton)view.findViewById(R.id.rb_amnt);
        rb_percent=(RadioButton)view.findViewById(R.id.rb_percent);
        units = (TextView) view.findViewById(R.id.tv_units);
        tv_gstrate=(TextView)view.findViewById(R.id.tv_prodgst);
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);



        if(prefs.getBusinessNature().equals("2")){
            if(prefs.getServiceType().equals("1")){

                tv_qty.setText(R.string.producttime);
            }
            else   if(prefs.getServiceType().equals("2")){
                tv_qty.setText(R.string.productweight);

            }
            else  if(prefs.getServiceType().equals("3")){
                tv_qty.setText(R.string.productdist);
            }



        }


        Bundle bundle = this.getArguments();
        if(bundle!=null){
       str_type= bundle.getString("type");

                str_businesstype=bundle.getString("business_type");



            if(bundle.containsKey("prod_code")) {
          /*  val=new HashMap<String,String>();
            val=invoicedb.getadditembycode(code);*/
                str_prodouctcode = bundle.getString("prod_code");
                str_productdesc = bundle.getString("prod_desc");
                String prod_fees = bundle.getString("prod_fees");
                String prod_qty = bundle.getString("prod_qty");
                String prod_disc = bundle.getString("prod_disc");
                discount_type = bundle.getString("prod_disc_type");
                String prod_unit = bundle.getString("prod_unit");
                String gst_rate = bundle.getString("gst_rate");
                str_igst= bundle.getString("Igst");
                edt_itemcode.setText(str_prodouctcode);
                edt_itemdesc.setText(str_productdesc);
                edt_itemamnt.setText(prod_fees);
                edt_itemqty.setText(prod_disc);
                edt_itemqty.setText(prod_qty);
                edt_itemcode.setEnabled(false);
                edt_discount.setText(prod_disc);
                edt_gstrate.setText(gst_rate);
                units.setText(prod_unit);
                insertorupdate = "U";
                btn_add.setText("Update");
                if (discount_type.equals("P")) {
                    edt_discount.setFilters(new InputFilter[] {
                            new DigitsKeyListener(Boolean.FALSE, Boolean.TRUE) {
                                int beforeDecimal = 2, afterDecimal = 2;

                                @Override
                                public CharSequence filter(CharSequence source, int start, int end,
                                                           Spanned dest, int dstart, int dend) {
                                    String temp = edt_discount.getText() + source.toString();

                                    if (temp.equals(".")) {
                                        return "0.";
                                    }
                                    else if (temp.toString().indexOf(".") == -1) {
                                        // no decimal point placed yet
                                        if (temp.length() > beforeDecimal) {
                                            return "";
                                        }
                                    } else {
                                        temp = temp.substring(temp.indexOf(".") + 1);
                                        if (temp.length() > afterDecimal) {
                                            return "";
                                        }
                                    }

                                    return super.filter(source, start, end, dest, dstart, dend);
                                }
                            }
                    });

                    rb_percent.setChecked(true);
                }
            }
            else{
                edt_itemcode.requestFocus();

                str_igst= bundle.getString("Igst");
            }

        }
        edt_gstrate.setFilters(new InputFilter[] {
                new DigitsKeyListener(Boolean.FALSE, Boolean.TRUE) {
                    int beforeDecimal = 2, afterDecimal = 2;

                    @Override
                    public CharSequence filter(CharSequence source, int start, int end,
                                               Spanned dest, int dstart, int dend) {
                        String temp = edt_gstrate.getText() + source.toString();

                        if (temp.equals(".")) {
                            return "0.";
                        }
                        else if (temp.toString().indexOf(".") == -1) {
                            // no decimal point placed yet
                            if (temp.length() > beforeDecimal) {
                                return "";
                            }
                        } else {
                            temp = temp.substring(temp.indexOf(".") + 1);
                            if (temp.length() > afterDecimal) {
                                return "";
                            }
                        }

                        return super.filter(source, start, end, dest, dstart, dend);
                    }
                }
        });



        if(str_businesstype.equals("2")){
            edt_gstrate.setVisibility(View.GONE);
            tv_gstrate.setVisibility(View.GONE);

        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.rb_amnt:
                        // switch to fragment 1
                        discount_type="A";
                        edt_discount.setText("");
                        edt_discount.setFilters(new InputFilter[] {});

                        break;
                    case R.id.rb_percent:
                        // Fragment 2
                        discount_type="P";

                        edt_discount.setText("");
                        edt_discount.setFilters(new InputFilter[] {
                                new DigitsKeyListener(Boolean.FALSE, Boolean.TRUE) {
                                    int beforeDecimal = 2, afterDecimal = 2;

                                    @Override
                                    public CharSequence filter(CharSequence source, int start, int end,
                                                               Spanned dest, int dstart, int dend) {
                                        String temp = edt_discount.getText() + source.toString();

                                        if (temp.equals(".")) {
                                            return "0.";
                                        }
                                        else if (temp.toString().indexOf(".") == -1) {
                                            // no decimal point placed yet
                                            if (temp.length() > beforeDecimal) {
                                                return "";
                                            }
                                        } else {
                                            temp = temp.substring(temp.indexOf(".") + 1);
                                            if (temp.length() > afterDecimal) {
                                                return "";
                                            }
                                        }

                                        return super.filter(source, start, end, dest, dstart, dend);
                                    }
                                }
                        });

                        break;
                }
            }
        });


        edt_itemcode.setThreshold(1);

        Itemadapter = new AutoCompleteItemAdapter(getContext(), R.layout.autocomplete_row, invoicedb.getproductdetails());
        edt_itemcode.setAdapter(Itemadapter);


        edt_itemcode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                                              @Override
                                                              public void onFocusChange(View v, boolean hasFocus) {

                                                                  if (!hasFocus) {
                                                                    /*   if (invoicedb.searchproductcode(edt_itemcode.getText().toString())==false) {
                                                                           Toast toast = new Toast(getContext());
                                                                           toast.setDuration(Toast.LENGTH_SHORT);
                                                                           toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                                                           toasttext.setText("Item not found");
                                                                           edt_itemcode.setText("");
                                                                           edt_itemdesc.setText("");
                                                                           units.setText(null);

                                                                           toast.setView(layouttoast);
                                                                           toast.show();
                                                                           flag="N";
                                                                           // clear your TextView
                                                                      }*/


                                                              if (invoicedb.searchproductcodeexists(edt_itemcode.getText().toString())==true){

                                                                          Toast toast = new Toast(getContext());
                                                                          toast.setDuration(Toast.LENGTH_SHORT);
                                                                          toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                                                          toasttext.setText("Item already exists");
                                                                          edt_itemcode.setText("");
                                                                          edt_itemdesc.setText("");
                                                                          units.setText(null);

                                                                          toast.setView(layouttoast);
                                                                          toast.show();

                                                                      }

                                                                  }

                                                              }
                                                              });

        edt_itemcode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                 Product_model model = (Product_model) view.getTag();
                 edt_itemcode.setText(model.productcode);
                 edt_itemdesc.setText(model.productdesc);
                edt_gstrate.setText(model.gstrate);
                edt_itemamnt.setText(model.productrate);
                 units.setText(model.unit);
                 edt_itemqty.requestFocus();
                 str_prodouctcode=model.productcode;
                 str_productdesc=model.productdesc;
                 str_productunit=model.unit;
                 str_hsn=model.hsncode;
                 str_gstrate=model.gstrate;

                 str_cess=model.cess;

            }
        });


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              if(edt_itemcode.getText().toString().equals(null)||edt_itemcode.getText().toString().isEmpty()){

                  Toast toast = new Toast(getContext());
                  toast.setDuration(Toast.LENGTH_SHORT);
                  toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                  toasttext.setText("No Item added");


                  toast.setView(layouttoast);
                  toast.show();
              }

             else    if(edt_itemamnt.getText().toString().equals(null)||edt_itemamnt.getText().toString().isEmpty()){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Amount can't be empty");


                    toast.setView(layouttoast);
                    toast.show();
                }
               else if(edt_itemqty.getText().toString().equals(null)||edt_itemqty.getText().toString().isEmpty()){

                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Quantity can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                }


        /*   else     if(flag.equals("N")){

                    flag="T";
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Item not found");
                    edt_itemcode.setText("");
                    edt_itemdesc.setText("");
                    units.setText(null);

                    toast.setView(layouttoast);
                    toast.show();

                }*/
                else {
                  if (invoicedb.searchproductcode(edt_itemcode.getText().toString()) == false) {
                      Toast toast = new Toast(getContext());
                      toast.setDuration(Toast.LENGTH_SHORT);
                      toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                      toasttext.setText("Item not found");
                      edt_itemcode.setText("");
                      edt_itemdesc.setText("");
                      edt_gstrate.setText("");
                      edt_itemamnt.setText("");
                      units.setText(null);
                      toast.setView(layouttoast);
                      toast.show();
                      flag = "N";
                      // clear your TextView
                  } else {

                      str_qty = edt_itemqty.getText().toString();
                      str_productrate = edt_itemamnt.getText().toString();
                      str_productdiscount = edt_discount.getText().toString();
                      str_gstrate=edt_gstrate.getText().toString();
                      if(str_gstrate.equals(null)||str_gstrate.isEmpty()){
                          str_gstrate="0";
                      }

                      addproductHashMap = new ArrayList<HashMap<String, String>>();
                      HashMap<String, String> userhashmap = new HashMap<String, String>();
                      userhashmap.put("product_code", str_prodouctcode);
                      userhashmap.put("prod_qty", str_qty);
                      userhashmap.put("prod_rate", str_productrate);



                      userhashmap.put("prod_gst_rate", str_gstrate);
                      if(insertorupdate.equals("I")) {
                         userhashmap.put("product_desc", str_productdesc);
                         userhashmap.put("product_unit", str_productunit);
                         userhashmap.put("Hsn", str_hsn);

                         userhashmap.put("cess", str_cess);
                     }
                      if (str_productdiscount.equals(null) || str_productdiscount.isEmpty()) {
                           total = Double.parseDouble(str_productrate) * Double.parseDouble(str_qty);
                          userhashmap.put("total_amount", String.valueOf(Math.round(total*100.0)/100.0));
                          userhashmap.put("discount_type", "N");
                          userhashmap.put("prod_disc", str_productdiscount);

                      } else {



                          double calc = Double.parseDouble(str_productrate) * Double.parseDouble(str_qty);
                          if (discount_type.equals("P")) {
                              double discount = calc*(Double.parseDouble(str_productdiscount)/100);
                              total= calc - discount;
                              userhashmap.put("total_amount", String.valueOf(Math.round(total*100.0)/100.0));
                              userhashmap.put("discount_type", discount_type);
                              userhashmap.put("prod_disc", str_productdiscount+"%");


                          } else {

                              total = calc - Double.parseDouble(str_productdiscount);
                              userhashmap.put("total_amount", String.valueOf(Math.round(total*100.0)/100.0));
                              userhashmap.put("discount_type", discount_type);
                              userhashmap.put("prod_disc", str_productdiscount);


                          }


                      }


 if(str_businesstype.equals("2")){
                          userhashmap.put("cgst", "0");
                          userhashmap.put("sgst", "0");
                          userhashmap.put("igst", "0");
                          userhashmap.put("cgst_percent", "0");
                          userhashmap.put("sgst_percent", "0");
                          userhashmap.put("igst_percent", "0");
                          userhashmap.put("TAT", String.valueOf(Math.round(total * 100.0) / 100.0));

}else {
    if (str_igst.equals("N")) {

        double gst = Double.parseDouble(str_gstrate) / 2;
        double gstcalc = total * (gst / 100);
        cgst = String.valueOf(Math.round(gstcalc * 100.0) / 100.0);
        sgst = String.valueOf(Math.round(gstcalc * 100.0) / 100.0);
        igst = "0";
        cgst_percent = String.valueOf(gst);
        sgst_percent = String.valueOf(gst);
        igst_percent = "NA";
        double TAT = total + (gstcalc * 2);
        total_after_tax = String.valueOf(Math.round(TAT * 100.0) / 100.0);
        userhashmap.put("cgst", cgst);
        userhashmap.put("sgst", sgst);
        userhashmap.put("igst", igst);
        userhashmap.put("cgst_percent", cgst_percent);
        userhashmap.put("sgst_percent", sgst_percent);
        userhashmap.put("igst_percent", igst_percent);
        userhashmap.put("TAT", total_after_tax);
    } else {
        double gst = Double.parseDouble(str_gstrate);
        double gstcalc = total * (gst / 100);
        double TAT = total + gstcalc;
        cgst = "0";
        sgst = "0";
        igst = String.valueOf(Math.round(gstcalc * 100.0) / 100.0);
        cgst_percent = "NA";
        sgst_percent = "NA";
        igst_percent = String.valueOf(gst);
        total_after_tax = String.valueOf(Math.round(TAT * 100.0) / 100.0);
        userhashmap.put("cgst", cgst);
        userhashmap.put("sgst", sgst);
        userhashmap.put("igst", igst);
        userhashmap.put("cgst_percent", cgst_percent);
        userhashmap.put("sgst_percent", sgst_percent);
        userhashmap.put("igst_percent", igst_percent);
        userhashmap.put("TAT", total_after_tax);
    }
}
                      addproductHashMap.add(userhashmap);
                      if(insertorupdate.equals("I")){
                          invoicedb.addProductitems(addproductHashMap);
                      }
                      else{
                          invoicedb.updateProductitems(addproductHashMap);
                      }
                      if(str_type.equals("I")) {
                          Invoice_fragment fragment = new Invoice_fragment();
                          FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                          fragmentManager.popBackStack();
                      }

                    else  if(str_type.equals("cr_note")){

                          Creditnote fragment = new Creditnote();
                          FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                          fragmentManager.popBackStack();

                      }

                      else  if(str_type.equals("IE")){

                          Invoice_edit fragment = new Invoice_edit();
                          FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                          fragmentManager.popBackStack();

                      }
                      else  if(str_type.equals("P")){

                          Invoice_edit fragment = new Invoice_edit();
                          FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                          fragmentManager.popBackStack();

                      }

                      else  if(str_type.equals("dr_note_p")){

                          Debit_note_purchase fragment = new Debit_note_purchase();
                          FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                          fragmentManager.popBackStack();

                      }


                      /*FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                      fragmentTransaction.replace(R.id.container_body, fragment);
                      fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                      Bundle bundle = new Bundle();
                      bundle.putString("ITEM_CODE", edt_itemcode.getText().toString());
                      fragment.setArguments(bundle);
                      fragmentTransaction.commit();*/
                  }
              }

            }
        });


     /*   ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
             /*   MobileAds.initialize(getActivity(), "ca-app-pub-2213723827168530~3447777035");*/
        // Setting ViewPager for each Tabs

        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}

