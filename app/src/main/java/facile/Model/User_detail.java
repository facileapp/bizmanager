package facile.Model;

/**
 * Created by pradeep on 20/2/18.
 */

public class User_detail {

    public String id;
    public String address;
    public String address1;
    public String phone_no;
    public String mail_id;
    public String city;
    public String state;
    public String pincode;
    public String dealer_type;
    public String dealer_nature;
    public String gst_no;
    public String terms_of_use;

    public String user_id;
    public String user_trade_name;
    public String state_pos;
    public String invoice_start_no;
    public String prefix;
    public String suffix;

}
