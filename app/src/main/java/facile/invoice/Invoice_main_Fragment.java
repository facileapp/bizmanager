package facile.invoice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 27/8/17.
 */

public class Invoice_main_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String url;
    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    String str_invoiceno,str_invoicemindate,str_prefix,str_suffix ;
    public  String check_first_time="0";
    Invoicedatabase invoicedb;


    prefManager prefs;
    public static final String UTF8_BOM = "\uFEFF";
    private OnFragmentInteractionListener mListener;

    public Invoice_main_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Invoice_main_Fragment newInstance(String param1, String param2) {
        Invoice_main_Fragment fragment = new Invoice_main_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invoice_main_page,container, false);
        invoicedb=new Invoicedatabase(getContext());
        prefs=new prefManager(getContext());
        if(check_first_time.equals("0")){
            getinvoicenodate(view);
            check_first_time="1";
        }
        else{
            // Setting ViewPager for each Tabs
            ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
            setupViewPager(viewPager);

            // Set Tabs inside Toolbar
            TabLayout tabs = (TabLayout) view.findViewById(R.id.tabs);
            tabs.setupWithViewPager(viewPager);


        }


        return view;

    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        PagerAdapterview adapter = new PagerAdapterview(getChildFragmentManager());
          adapter.addFragment(new Invoice_fragment(), "Business");
           adapter.addFragment(new Invoice_fragment_unregistered(), "Consumer");



        viewPager.setAdapter(adapter);




   }

    static class PagerAdapterview extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapterview(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }





    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
    @Override
    public void onResume() {
        super.onResume();
      /*  AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle("Invoice");*/
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

      public  void getinvoicenodate(final View view){
        url = Constants.INVOICE_NUMBER;
        requestQueue = Volley.newRequestQueue(getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Generating invoice no...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String m=response.toString();
                            String x=     removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                            str_invoiceno = jsonRequest.getString("Invoice_no");
                            str_prefix=jsonRequest.getString("Prefix");
                            str_suffix=jsonRequest.getString("Suffix");
                            str_invoicemindate = jsonRequest.getString("Invoice_min_date");
                            if (statuscode.equals("200")) {
                                prefs.setInvoiceMinDate(str_invoicemindate);
                                prefs.setInvoiceNo(str_invoiceno);
                                prefs.setPrefix(str_prefix);
                                prefs.setSuffix(str_suffix);
                                // Setting ViewPager for each Tabs
                                ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
                                setupViewPager(viewPager);
                                // Set Tabs inside Toolbar
                                TabLayout tabs = (TabLayout) view.findViewById(R.id.tabs);
                                tabs.setupWithViewPager(viewPager);
                                progressDialog.dismiss();

                            }


                            else   if (statuscode.equals("400")){
                                Toast.makeText(getContext(),jsonRequest.getString("status"),Toast.LENGTH_LONG).show();
                            }


                        } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                            progressDialog.dismiss();


                        }
                    }
                },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    /*    errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layout);
                        progressDialog.dismiss();
                        toast.show();*/

                        errorhandler e = new errorhandler();
                        String msg = e.errorhandler(error);
                        progressDialog.dismiss();
                        Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",prefs.getUserId());
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }



}
