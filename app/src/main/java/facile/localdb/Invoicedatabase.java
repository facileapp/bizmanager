package facile.localdb;

/**
 * Created by pradeep on 26/8/17.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.HashMap;

import facile.Model.Bank_details_model;
import facile.Model.Customer_model;
import facile.Model.Dealer_model;
import facile.Model.Dealer_nature_model;
import facile.Model.Invoice_Product_model;
import facile.Model.Month_model;
import facile.Model.Product_model;
import facile.Model.Service_model;
import facile.Model.Shipping_model;
import facile.Model.State_model;
import facile.Model.Supplier_model;
import facile.Model.Transport_model;
import facile.Model.Units_model;
import facile.Model.User_detail;
import facile.Model.invoice_list_model;

public class Invoicedatabase extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "invoice.db";
    private static final int DATABASE_VERSION = 2;

    //CUSTOMER
    private static final String CUSTOMER_ID="customer_id";
    private static final String CUSTOMER_NAME="customer_name";
    private static final String CUSTOMER_GSTNO="Gst_no";
    private static final String USER_ID="user_id";
    private static final String CUSTOMER_ADDRESS="address";
    private static final String CUSTOMER_ADDRESS1="address1";
    private static final String CUSTOMER_CITY="city";
    private static final String CUSTOMER_STATE="state";
    private static final String CUSTOMER_PINCODE="Pincode";
    private static final String CUSTOMER_PHONENO="phone_no";
    private static final String CUSTOMER_MAILID="mail_id";
    private static final String CUSTOMER_STATE_POS="state_pos";
    private static final String CUSTOMER_OPENING_BALANCE="opening_balance";
    private static final String CUSTOMER_CRORDR="opening_balance_crordr";

    //SUPPLIER
    private static final String SUPPLIER_ID="supplier_id";
    private static final String SUPPLIER_NAME="supplier_name";


    //SHIPPING ADDRESS
    private static final String   SHIPPING_ID ="shipping_id";
    private static final String   SHIPPING_CUST_ID ="cust_id";
    private static final String   SHIPPING_ADDRESS_LINE1="address_line_1";
    private static final String   SHIPPING_ADDRESS_LINE2 ="address_line_2";
    private static final String   SHIPPING_CITY="s_city";
    private static final String   SHIPPING_PINCODE="s_pincode";
    private static final String   SHIPPING_STATE ="s_state";
    private static final String   SHIPPING_COUNTRY ="s_country";

    //ADD_SHIPPING ADDRESS
    private static final String  ADD_SHIPPING_ID ="id";
    private static final String   ADD_SHIPPING_ADDRESS_LINE1="shipping_address1";
    private static final String   ADD_SHIPPING_ADDRESS_LINE2 ="shipping_address2";
    private static final String   ADD_SHIPPING_CITY="shipping_city";
    private static final String   ADD_SHIPPING_PINCODE="shipping_pincode";
    private static final String   ADD_SHIPPING_STATE ="shipping_state";
    private static final String   ADD_SHIPPING_COUNTRY ="shipping_country";


    //USER
    private static final String USER_DETAILS_ID="id";
    private static final String USER_NAME="user_trade_name";
    private static final String USER_GSTNO="gst_no";
    private static final String USER_ADDRESS="address";
    private static final String USER_ADDRESS1="address1";
    private static final String USER_CITY="city";
    private static final String USER_STATE="state";
    private static final String USER_PINCODE="pincode";
    private static final String USER_PHONENO="phone_no";
    private static final String USER_MAILID="mail_id";
    private static final String USER_DEALEERTYPE="dealer_type";
    private static final String USER_DEALERNATURE="dealer_nature";
    private static final String USER_STATE_POS="spn_state_position";
    private static final String INVOICE_START_NO="invoice_start_no";
    private static final String INVOICE_NO_PREFIX="invoice_no_prefix";
    private static final String INVOICE_NO_SUFFIX="invoice_no_suffix";
    private static final String TERMS_OF_USE="terms_of_use";


    //PRODUCT
    private static final String PRODUCT_DETAILS_ID="id";
    private static final String PRODUCT_CODE="product_code";
    private static final String PRODUCT_DESC="prod_desc";
    private static final String PRODUCT_ID="prod_id";
    private static final String PRODUCT_RATE="prod_rate";
    private static final String PRODUCT_UNIT="prod_unit";
    private static final String PRODUCT_HSN="Hsn";
    private static final String PRODUCT_GSTRATE="prod_gst_rate";
    private static final String PRODUCT_CESS="cess";
    private static final String PRODUCT_UNIT_POS="unit_pos";


    //ADD PRODUCT ITEMS
    private static final String ADD_PRODUCT_DETAILS_ID="id";
    private static final String ADD_PRODUCT_CODE="product_code";
    private static final String ADD_PRODUCT_DESC="prod_desc";
    private static final String ADD_PRODUCT_UNIT="prod_unit";
    private static final String ADD_PRODUCT_HSN="Hsn";
    private static final String ADD_PRODUCT_GSTRATE="prod_gst_rate";
    private static final String ADD_PRODUCT_CESS="cess";
    private static final String ADD_PRODUCT_DISCOUNT="prod_disc";
    private static final String ADD_PRODUCT_QUANTITY="prod_qty";
    private static final String ADD_PRODUCT_RATE="prod_rate";
    private static final String ADD_PRODUCT_TOTAL="total_amount";
    private static final String ADD_PRODUCT_DISCOUNT_TYPE="discount_type";
    private static final String ADD_PRODUCT_CGST="cgst";
    private static final String ADD_PRODUCT_SGST="sgst";
    private static final String ADD_PRODUCT_IGST="igst";
    private static final String ADD_PRODUCT_CGST_PERCENT="cgst_percent";
    private static final String ADD_PRODUCT_SGST_PERCENT="sgst_percent";
    private static final String ADD_PRODUCT_IGST_PERCENT="igst_percent";
    private static final String ADD_PRODUCT_TAT="TAT";

//bank details
    private static final String USER_BANK_ID="id";
    private static final String USER_BANK_ACNO="ac_no";
    private static final String USER_BANK_NAME="bank_name";
    private static final String USER_BANK_BRANCH="branch";
    private static final String USER_BANK_IFSC="ifsc";

   //STATE
    private static final String STATE_CODE="State_code";
    private static final String STATE_NAME="State_name";
    private static final String STATE_ID="id";

    //UNIT
    private static final String UNIT_ID="id";
    private static final String UNIT_CODE="unit_code";
    private static final String UNIT_DESC="unit_desc";

    //DEALER TYPE
    private static final String DEALER_ID="id";
    private static final String DEALER_TYPE="dealer_type";

    //DEALER NATURE
    private static final String DEALER_NATURE_ID="id";
    private static final String DEALER_NATURE="nature";

    //DEALER TYPE
    private static final String SERVICE_ID="id";
    private static final String SERVICE_TYPE="service";

    //TRANSPORT MODE
    private static final String TRANSPORT_ID="id";
    private static final String TRANSPORT_MODE="mode";

    //MONTH
    private static final String MONTH_ID="id";
    private static final String MONTH_NAME="month";

    //Invoice list

    private static final String INVOICE_LIST_ID="id";
    private static final String INVOICE_LIST_INVOICE_NO="invoiveno";
    private static final String INVOICE_LIST_CUSTOMER_NAME="customer_name";
    private static final String INVOICE_LIST_TAT="tat";
    private static final String INVOICE_LIST_PERSON_TYPE="person_type";


    //TABLE
    private static final String CUSTOMER_DETAILS="Customer_details";
    private static final String SUPPLIER_DETAILS="Supplier_details";
    private static final String SHIPPING_ADDRESS="Shipping_address";
    private static final String PRODUCT_DETAILS="Product_details";
    private static final String BANK_DETAILS="bank_details";
    private static final String USER_DETAILS="User_details";
    private static final String STATE_TABLE="state_table";
    private static final String UNIT_TABLE="units_table";
    private static final String DEALER_TYPE_TABLE="dealer_type";
    private static final String SERVICE_TYPE_TABLE="Service_type";
    private static final String DEALER_NATURE_TABLE="dealer_nature";
    private static final String ADD_ITEMS="Add_Item";
    private static final String TRANSPORT="Transport_mode";
    private static final String MONTH_TABLE="month_table";
    private static final String INVOICE_LIST_TABLE="invoice_list";








    public Invoicedatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public ArrayList<State_model> getstates(){
        SQLiteDatabase db=getWritableDatabase();

        db.beginTransaction();
        String[] columns1={Invoicedatabase.STATE_CODE, Invoicedatabase.STATE_NAME};
        Cursor cursor=db.query(Invoicedatabase.STATE_TABLE, columns1, null, null, null, null, null);

        ArrayList<State_model> stateArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            State_model states=new State_model();
            states.state_code=cursor.getString(cursor.getColumnIndex(Invoicedatabase.STATE_CODE));
            states.state_name=cursor.getString(cursor.getColumnIndex(Invoicedatabase.STATE_NAME));


            stateArrayList.add(states);
        }
        cursor.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return stateArrayList;


    }
    public ArrayList<String> getstatebyposition(String code){
        SQLiteDatabase db=getWritableDatabase();


        String[] columns1={Invoicedatabase.STATE_CODE, Invoicedatabase.STATE_NAME};
        Cursor cursor = db.rawQuery("SELECT id,State_name FROM " + Invoicedatabase.STATE_TABLE
                + " WHERE " + STATE_CODE + "='" + code+ "'", null);

        if ( cursor.getCount() > 0 )
        {
            cursor.moveToFirst();

           ArrayList<String> x=new ArrayList<String>();

            String data = cursor.getString(cursor.getColumnIndex("id"));
            String data1 = cursor.getString(cursor.getColumnIndex("State_name"));

            x.add(data);
            x.add(data1);


            return x;
        }

        cursor.close();


        db.close();
        return null;
    }


    public String getvaluebystateposition(String pos){
        SQLiteDatabase db=getWritableDatabase();
       Cursor cursor = db.rawQuery("SELECT State_name FROM " + Invoicedatabase.STATE_TABLE
                + " WHERE " + "id" + "='" + pos+ "'", null);
        if ( cursor.getCount() > 0 )
        {
            cursor.moveToFirst();
            String data1 = cursor.getString(cursor.getColumnIndex("State_name"));
           return data1;
        }

        cursor.close();


        db.close();
        return null;
    }


    public ArrayList<Units_model> getunits(){
        SQLiteDatabase db=getWritableDatabase();
        db.beginTransaction();
        String[] columns1={Invoicedatabase.UNIT_ID, Invoicedatabase.UNIT_CODE,Invoicedatabase.UNIT_DESC};
        Cursor cursor=db.query(Invoicedatabase.UNIT_TABLE, columns1, null, null, null, null, null);
        ArrayList<Units_model> UnitsArrayList=new ArrayList<>();
        while(cursor.moveToNext()){
            Units_model units=new Units_model();
            units.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.UNIT_ID));
            units.unit_code=cursor.getString(cursor.getColumnIndex(Invoicedatabase.UNIT_CODE));
            units.unit_desc=cursor.getString(cursor.getColumnIndex(Invoicedatabase.UNIT_DESC));
             UnitsArrayList.add(units);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return UnitsArrayList;


    }


    public ArrayList<Dealer_model> getdealertype(){
        SQLiteDatabase db=getWritableDatabase();


        String[] columns1={Invoicedatabase.DEALER_ID, Invoicedatabase.DEALER_TYPE};
        Cursor cursor=db.query(Invoicedatabase.DEALER_TYPE_TABLE, columns1, null, null, null, null, null);

        ArrayList<Dealer_model> dealerArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            Dealer_model dealertype=new Dealer_model();
            dealertype.dealer_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.DEALER_ID));
            dealertype.dealer_type=cursor.getString(cursor.getColumnIndex(Invoicedatabase.DEALER_TYPE));
            dealerArrayList.add(dealertype);
        }
        return dealerArrayList;


    }


    public ArrayList<Dealer_nature_model> getdealernature(){
        SQLiteDatabase db=getWritableDatabase();


        String[] columns1={Invoicedatabase.DEALER_NATURE_ID, Invoicedatabase.DEALER_NATURE};
        Cursor cursor=db.query(Invoicedatabase.DEALER_NATURE_TABLE, columns1, null, null, null, null, null);

        ArrayList<Dealer_nature_model> dealerArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            Dealer_nature_model dealertype=new Dealer_nature_model();
            dealertype.dealer_nature_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.DEALER_NATURE_ID));
            dealertype.dealer_nature=cursor.getString(cursor.getColumnIndex(Invoicedatabase.DEALER_NATURE));
            dealerArrayList.add(dealertype);
        }
        return dealerArrayList;


    }


    public ArrayList<Service_model> getservicetype(){
        SQLiteDatabase db=getWritableDatabase();


        String[] columns1={Invoicedatabase.SERVICE_ID, Invoicedatabase.SERVICE_TYPE};
        Cursor cursor=db.query(Invoicedatabase.SERVICE_TYPE_TABLE, columns1, null, null, null, null, null);

        ArrayList<Service_model> serviceArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            Service_model servicetype=new Service_model();
            servicetype.service_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.SERVICE_ID));
            servicetype.service_type=cursor.getString(cursor.getColumnIndex(Invoicedatabase.SERVICE_TYPE));
            serviceArrayList.add(servicetype);
        }
        return serviceArrayList;


    }

    public ArrayList<Transport_model> gettransportmode(){
        SQLiteDatabase db=getWritableDatabase();


        String[] columns1={Invoicedatabase.TRANSPORT_ID, Invoicedatabase.TRANSPORT_MODE};
        Cursor cursor=db.query(Invoicedatabase.TRANSPORT, columns1, null, null, null, null, null);

        ArrayList<Transport_model> dealerArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            Transport_model transport=new Transport_model();
            transport.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.TRANSPORT_ID));
            transport.mode=cursor.getString(cursor.getColumnIndex(Invoicedatabase.TRANSPORT_MODE));
            dealerArrayList.add(transport);
        }
        return dealerArrayList;


    }


    public ArrayList<Month_model> getmonth(int thismonth){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.MONTH_ID, Invoicedatabase.MONTH_NAME};
        Cursor cursor=db.query(Invoicedatabase.MONTH_TABLE, columns1, null, null, null, null, null);
        ArrayList<Month_model> monthArrayList=new ArrayList<>();
        while(cursor.moveToNext()){

          if(cursor.getPosition()<thismonth){
                Month_model month=new Month_model();
                month.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.MONTH_ID));
                month.month=cursor.getString(cursor.getColumnIndex(Invoicedatabase.MONTH_NAME));
                monthArrayList.add(month);
            }
        }
        return monthArrayList;
    }


    public ArrayList<User_detail> getuserdetails(){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.USER_DETAILS_ID, Invoicedatabase.USER_ADDRESS,Invoicedatabase.USER_ADDRESS1,Invoicedatabase.USER_PHONENO,Invoicedatabase.USER_MAILID,
                Invoicedatabase.USER_CITY,Invoicedatabase.USER_STATE,Invoicedatabase.USER_PINCODE,Invoicedatabase.USER_DEALEERTYPE,Invoicedatabase.USER_DEALERNATURE,
                Invoicedatabase.USER_GSTNO,Invoicedatabase.USER_ID,Invoicedatabase.USER_NAME,Invoicedatabase.USER_STATE_POS,
        Invoicedatabase.INVOICE_START_NO,Invoicedatabase.INVOICE_NO_PREFIX,Invoicedatabase.INVOICE_NO_SUFFIX,Invoicedatabase.TERMS_OF_USE};
        db.beginTransaction();
        Cursor cursor=db.query(Invoicedatabase.USER_DETAILS, columns1, null, null, null, null, null);
        ArrayList<User_detail> dealerArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            User_detail userDetail=new User_detail();
            userDetail.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_ID));
            userDetail.address=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_ADDRESS));
            userDetail.address1=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_ADDRESS1));
            userDetail.phone_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_PHONENO));
            userDetail.mail_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_MAILID));
            userDetail.city=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_CITY));
            userDetail.state=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_STATE));
            userDetail.pincode=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_PINCODE));
            userDetail.dealer_type=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_DEALEERTYPE));
            userDetail.dealer_nature=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_DEALERNATURE));
            userDetail.gst_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_GSTNO));

            userDetail.user_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_ID));
            userDetail.user_trade_name=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_NAME));
            userDetail.state_pos=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_STATE_POS));
            userDetail.invoice_start_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.INVOICE_START_NO));
            userDetail.prefix=cursor.getString(cursor.getColumnIndex(Invoicedatabase.INVOICE_NO_PREFIX));
            userDetail.suffix=cursor.getString(cursor.getColumnIndex(Invoicedatabase.INVOICE_NO_SUFFIX));
            userDetail.terms_of_use=cursor.getString(cursor.getColumnIndex(Invoicedatabase.TERMS_OF_USE));
            dealerArrayList.add(userDetail);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        return dealerArrayList;


    }



    public void insertUserdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
           database.beginTransaction();
            for(int i=0;i<queryValues.size();i++){
                ContentValues values = new ContentValues();
                values.put(USER_DETAILS_ID, queryValues.get(i).get("id"));
                values.put(USER_ADDRESS, queryValues.get(i).get("address"));
                values.put(USER_ADDRESS1, queryValues.get(i).get("address1"));
                values.put(USER_PHONENO, queryValues.get(i).get("phone_no"));
                values.put(USER_MAILID, queryValues.get(i).get("mail_id"));
                values.put(USER_CITY, queryValues.get(i).get("city"));
                values.put(USER_STATE, queryValues.get(i).get("state"));
                values.put(USER_PINCODE, queryValues.get(i).get("pincode"));
                values.put(USER_DEALEERTYPE, queryValues.get(i).get("dealer_type"));
                values.put(USER_DEALERNATURE, queryValues.get(i).get("dealer_nature"));
                values.put(USER_GSTNO, queryValues.get(i).get("gst_no"));
                values.put(USER_ID, queryValues.get(i).get("user_id"));
                values.put(USER_NAME, queryValues.get(i).get("user_trade_name"));
                values.put(USER_STATE_POS, queryValues.get(i).get("spn_state_position"));
                values.put(INVOICE_START_NO, queryValues.get(i).get("invoice_start_no"));
                values.put(INVOICE_NO_PREFIX, queryValues.get(i).get("invoice_no_prefix"));
                values.put(INVOICE_NO_SUFFIX, queryValues.get(i).get("invoice_no_suffix"));
                values.put(TERMS_OF_USE, queryValues.get(i).get("terms_of_use"));
                database.insert(USER_DETAILS, null, values);

           }
            database.setTransactionSuccessful();
            database.endTransaction();



        database.close();

    }


    public void updateUserdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
               ContentValues values = new ContentValues();
               values.put(USER_ADDRESS, queryValues.get(i).get("address"));
               values.put(USER_ADDRESS1, queryValues.get(i).get("address1"));
                values.put(USER_PHONENO, queryValues.get(i).get("phone_no"));
                values.put(USER_MAILID, queryValues.get(i).get("mail_id"));
                values.put(USER_CITY, queryValues.get(i).get("city"));
                values.put(USER_STATE, queryValues.get(i).get("state"));
                values.put(USER_PINCODE, queryValues.get(i).get("pincode"));
                values.put(USER_DEALEERTYPE, queryValues.get(i).get("dealer_type"));
                values.put(USER_DEALERNATURE, queryValues.get(i).get("dealer_nature"));
                values.put(USER_GSTNO, queryValues.get(i).get("gst_no"));
                values.put(USER_ID, queryValues.get(i).get("user_id"));
                values.put(USER_NAME, queryValues.get(i).get("user_trade_name"));
                values.put(USER_STATE_POS, queryValues.get(i).get("spn_state_position"));
                values.put(INVOICE_NO_PREFIX, queryValues.get(i).get("invoice_no_prefix"));
                values.put(INVOICE_NO_SUFFIX, queryValues.get(i).get("invoice_no_suffix"));
                    values.put(TERMS_OF_USE, queryValues.get(i).get("terms_of_use"));
                database.update(USER_DETAILS, values,"user_id = ?",new String[] { queryValues.get(i).get("user_id") });
            }
            database.setTransactionSuccessful();
            database.endTransaction();
            database.close();

    }



    public ArrayList<Bank_details_model> getbankdetails(){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.USER_BANK_ID,Invoicedatabase.USER_BANK_ACNO,Invoicedatabase.USER_BANK_NAME,
                Invoicedatabase.USER_BANK_BRANCH,Invoicedatabase.USER_BANK_IFSC};
        db.beginTransaction();
        Cursor cursor=db.query(Invoicedatabase.BANK_DETAILS, columns1, null, null, null, null, null);
        ArrayList<Bank_details_model> bankArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            Bank_details_model bankdetail=new Bank_details_model();
            bankdetail.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_BANK_ID));
            bankdetail.ac_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_BANK_ACNO));
            bankdetail.branch=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_BANK_BRANCH));
            bankdetail.ifsc=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_BANK_IFSC));
            bankdetail.bank_name=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_BANK_NAME));
            bankArrayList.add(bankdetail);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return bankArrayList;


    }




    public void insertbankdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(USER_BANK_ID, queryValues.get(i).get("id"));
            values.put(USER_BANK_ACNO, queryValues.get(i).get("ac_no"));
            values.put(USER_BANK_BRANCH, queryValues.get(i).get("branch"));
            values.put(USER_BANK_IFSC, queryValues.get(i).get("ifsc"));
            values.put(USER_BANK_NAME, queryValues.get(i).get("bank_name"));
            database.insert(BANK_DETAILS, null, values);


        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();

    }


    public void updatebankdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(USER_BANK_ACNO, queryValues.get(i).get("ac_no"));
            values.put(USER_BANK_BRANCH, queryValues.get(i).get("branch"));
            values.put(USER_BANK_IFSC, queryValues.get(i).get("ifsc"));
            values.put(USER_BANK_NAME, queryValues.get(i).get("bank_name"));
            database.update(BANK_DETAILS, values,"id = ?",new String[] { queryValues.get(i).get("id") });
        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();

    }






    public ArrayList<Customer_model> getcustomerdetails(){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.CUSTOMER_ID,Invoicedatabase.USER_ID, Invoicedatabase.CUSTOMER_ADDRESS,
                Invoicedatabase.CUSTOMER_ADDRESS1,Invoicedatabase.CUSTOMER_PHONENO,
                Invoicedatabase.CUSTOMER_MAILID,Invoicedatabase.CUSTOMER_CITY,Invoicedatabase.CUSTOMER_STATE,
                Invoicedatabase.CUSTOMER_PINCODE,Invoicedatabase.CUSTOMER_GSTNO,Invoicedatabase.CUSTOMER_NAME,
                Invoicedatabase.CUSTOMER_STATE_POS,Invoicedatabase.CUSTOMER_OPENING_BALANCE,Invoicedatabase.CUSTOMER_CRORDR,
        Invoicedatabase.SHIPPING_ID,Invoicedatabase.SHIPPING_CUST_ID,Invoicedatabase.SHIPPING_ADDRESS_LINE1,Invoicedatabase.SHIPPING_ADDRESS_LINE2,
        Invoicedatabase.SHIPPING_CITY,Invoicedatabase.SHIPPING_PINCODE,Invoicedatabase.SHIPPING_STATE,
        Invoicedatabase.SHIPPING_COUNTRY};
               db.beginTransaction();
        Cursor cursor=db.query(Invoicedatabase.CUSTOMER_DETAILS, columns1, null, null, null, null, null);
        ArrayList<Customer_model> dealerArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            Customer_model customerdetail=new Customer_model();
            customerdetail.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_ID));
            customerdetail.address=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_ADDRESS));
            customerdetail.address1=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_ADDRESS1));
            customerdetail.user_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_ID));
            customerdetail.phone_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_PHONENO));
            customerdetail.mail_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_MAILID));
            customerdetail.city=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_CITY));
            customerdetail.state=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_STATE));
            customerdetail.pincode=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_PINCODE));
            customerdetail.gst_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_GSTNO));
            customerdetail.customer_name=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_NAME));
            customerdetail.state_pos=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_STATE_POS));
            customerdetail.openingbal=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_OPENING_BALANCE));
            customerdetail.crordr=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_CRORDR));
            customerdetail.shipping_id = cursor.getString(cursor.getColumnIndex(Invoicedatabase.SHIPPING_ID));
            customerdetail.cust_id = cursor.getString(cursor.getColumnIndex(Invoicedatabase.SHIPPING_CUST_ID));
            customerdetail.shipping_address1 = cursor.getString(cursor.getColumnIndex(Invoicedatabase.SHIPPING_ADDRESS_LINE1));
            customerdetail.shipping_address2 = cursor.getString(cursor.getColumnIndex(Invoicedatabase.SHIPPING_ADDRESS_LINE2));
            customerdetail.s_country = cursor.getString(cursor.getColumnIndex(Invoicedatabase.SHIPPING_COUNTRY));
            customerdetail.s_city = cursor.getString(cursor.getColumnIndex(Invoicedatabase.SHIPPING_CITY));
            customerdetail.s_state = cursor.getString(cursor.getColumnIndex(Invoicedatabase.SHIPPING_STATE));
            customerdetail.s_pincode = cursor.getString(cursor.getColumnIndex(Invoicedatabase.SHIPPING_PINCODE));
            dealerArrayList.add(customerdetail);
        }
        cursor.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return dealerArrayList;
    }

    public void insertCustomerdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
            database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
                ContentValues values = new ContentValues();
            values.put(SHIPPING_ID, queryValues.get(i).get("shipping_id"));
            values.put(SHIPPING_CUST_ID, queryValues.get(i).get("cust_id"));
            values.put(SHIPPING_ADDRESS_LINE1, queryValues.get(i).get("s_address_line_1"));
            values.put(SHIPPING_ADDRESS_LINE2, queryValues.get(i).get("s_address_line_2"));
            values.put(SHIPPING_CITY,queryValues.get(i).get("s_city"));
            values.put(SHIPPING_PINCODE, queryValues.get(i).get("s_pincode"));
            values.put(SHIPPING_STATE,queryValues.get(i).get("s_state"));
            values.put(SHIPPING_COUNTRY, queryValues.get(i).get("s_country"));
                values.put(CUSTOMER_ID, queryValues.get(i).get("id"));
                values.put(CUSTOMER_NAME, queryValues.get(i).get("customer_name"));
                values.put(CUSTOMER_GSTNO, queryValues.get(i).get("gst_no"));
                values.put(USER_ID, queryValues.get(i).get("user_id"));
                values.put(CUSTOMER_ADDRESS,queryValues.get(i).get("address"));
            values.put(CUSTOMER_ADDRESS1,queryValues.get(i).get("address1"));
                values.put(CUSTOMER_CITY, queryValues.get(i).get("city"));
                values.put(CUSTOMER_STATE, queryValues.get(i).get("state"));
                values.put(CUSTOMER_PINCODE, queryValues.get(i).get("pincode"));
                values.put(CUSTOMER_PHONENO, queryValues.get(i).get("phone_no"));
                values.put(CUSTOMER_MAILID, queryValues.get(i).get("mail_id"));
                values.put(CUSTOMER_STATE_POS, queryValues.get(i).get("spn_state_position"));
                values.put(CUSTOMER_OPENING_BALANCE, queryValues.get(i).get(CUSTOMER_OPENING_BALANCE));
                values.put(CUSTOMER_CRORDR, queryValues.get(i).get("opening_balance_crordr"));

                database.insert(CUSTOMER_DETAILS, null, values);
            }
            database.setTransactionSuccessful();
            database.endTransaction();
            database.close();

    }


    public void updateCustomerdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
           database.beginTransaction();
            for(int i=0;i<queryValues.size();i++){
                ContentValues values = new ContentValues();
                values.put(CUSTOMER_NAME, queryValues.get(i).get("customer_name"));
                values.put(CUSTOMER_ADDRESS, queryValues.get(i).get("address"));
                values.put(CUSTOMER_ADDRESS1, queryValues.get(i).get("address1"));
                values.put(CUSTOMER_CITY, queryValues.get(i).get("city"));
                values.put(CUSTOMER_STATE, queryValues.get(i).get("state"));
                values.put(CUSTOMER_PINCODE, queryValues.get(i).get("pincode"));
                values.put(CUSTOMER_PHONENO, queryValues.get(i).get("phone_no"));
                values.put(CUSTOMER_MAILID, queryValues.get(i).get("mail_id"));
                values.put(CUSTOMER_STATE_POS, queryValues.get(i).get("spn_state_position"));
                values.put(CUSTOMER_OPENING_BALANCE, queryValues.get(i).get(CUSTOMER_OPENING_BALANCE));
                values.put(CUSTOMER_CRORDR, queryValues.get(i).get("opening_balance_crordr"));
                values.put(SHIPPING_ADDRESS_LINE1, queryValues.get(i).get("s_address_line_1"));
                values.put(SHIPPING_ADDRESS_LINE2, queryValues.get(i).get("s_address_line_2"));
                values.put(SHIPPING_CITY,queryValues.get(i).get("s_city"));
                values.put(SHIPPING_PINCODE, queryValues.get(i).get("s_pincode"));
                values.put(SHIPPING_STATE,queryValues.get(i).get("s_state"));
                values.put(SHIPPING_COUNTRY, queryValues.get(i).get("s_country"));
                database.update(CUSTOMER_DETAILS, values,"customer_id = ?",new String[] { queryValues.get(i).get("id") });

            }
            database.setTransactionSuccessful();
            database.endTransaction();
            database.close();

    }

    public ArrayList<Supplier_model> getsupplierdetails(){
        SQLiteDatabase db=getWritableDatabase();


            String[] columns1={Invoicedatabase.SUPPLIER_ID,Invoicedatabase.SUPPLIER_NAME, Invoicedatabase.CUSTOMER_GSTNO,
                Invoicedatabase.USER_ID,Invoicedatabase.CUSTOMER_ADDRESS,
                Invoicedatabase.CUSTOMER_ADDRESS1,Invoicedatabase.CUSTOMER_CITY,Invoicedatabase.CUSTOMER_STATE,
                Invoicedatabase.CUSTOMER_PINCODE,Invoicedatabase.CUSTOMER_PHONENO,Invoicedatabase.CUSTOMER_MAILID,
                Invoicedatabase.CUSTOMER_STATE_POS,Invoicedatabase.CUSTOMER_OPENING_BALANCE,Invoicedatabase.CUSTOMER_CRORDR
              };
        db.beginTransaction();
        Cursor cursor=db.query(Invoicedatabase.SUPPLIER_DETAILS, columns1, null, null, null, null, null);
        ArrayList<Supplier_model> dealerArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            Supplier_model customerdetail=new Supplier_model();
            customerdetail.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.SUPPLIER_ID));
            customerdetail.address=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_ADDRESS));
            customerdetail.address1=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_ADDRESS1));
            customerdetail.user_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_ID));
            customerdetail.phone_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_PHONENO));
            customerdetail.mail_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_MAILID));
            customerdetail.city=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_CITY));
            customerdetail.state=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_STATE));
            customerdetail.pincode=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_PINCODE));
            customerdetail.gst_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_GSTNO));
            customerdetail.supplier_name=cursor.getString(cursor.getColumnIndex(Invoicedatabase.SUPPLIER_NAME));
            customerdetail.state_pos=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_STATE_POS));
            customerdetail.openingbal=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_OPENING_BALANCE));
            customerdetail.crordr=cursor.getString(cursor.getColumnIndex(Invoicedatabase.CUSTOMER_CRORDR));

            dealerArrayList.add(customerdetail);
        }
        cursor.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return dealerArrayList;
    }




    public void insertSupplierdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();

            values.put(SUPPLIER_ID, queryValues.get(i).get("id"));
            values.put(SUPPLIER_NAME, queryValues.get(i).get("supplier_name"));
            values.put(CUSTOMER_GSTNO, queryValues.get(i).get("gst_no"));
            values.put(USER_ID, queryValues.get(i).get("user_id"));
            values.put(CUSTOMER_ADDRESS,queryValues.get(i).get("address"));
            values.put(CUSTOMER_ADDRESS1,queryValues.get(i).get("address1"));
            values.put(CUSTOMER_CITY, queryValues.get(i).get("city"));
            values.put(CUSTOMER_STATE, queryValues.get(i).get("state"));
            values.put(CUSTOMER_PINCODE, queryValues.get(i).get("pincode"));
            values.put(CUSTOMER_PHONENO, queryValues.get(i).get("phone_no"));
            values.put(CUSTOMER_MAILID, queryValues.get(i).get("mail_id"));
            values.put(CUSTOMER_STATE_POS, queryValues.get(i).get("spn_state_position"));
            values.put(CUSTOMER_OPENING_BALANCE, queryValues.get(i).get(CUSTOMER_OPENING_BALANCE));
            values.put(CUSTOMER_CRORDR, queryValues.get(i).get("opening_balance_crordr"));

            database.insert(SUPPLIER_DETAILS, null, values);
        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();

    }


    public void updateSupplierdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(SUPPLIER_NAME, queryValues.get(i).get("supplier_name"));
            values.put(CUSTOMER_ADDRESS, queryValues.get(i).get("address"));
            values.put(CUSTOMER_ADDRESS1, queryValues.get(i).get("address1"));
            values.put(CUSTOMER_CITY, queryValues.get(i).get("city"));
            values.put(CUSTOMER_STATE, queryValues.get(i).get("state"));
            values.put(CUSTOMER_PINCODE, queryValues.get(i).get("pincode"));
            values.put(CUSTOMER_PHONENO, queryValues.get(i).get("phone_no"));
            values.put(CUSTOMER_MAILID, queryValues.get(i).get("mail_id"));
            values.put(CUSTOMER_STATE_POS, queryValues.get(i).get("spn_state_position"));
            values.put(CUSTOMER_OPENING_BALANCE, queryValues.get(i).get(CUSTOMER_OPENING_BALANCE));
            values.put(CUSTOMER_CRORDR, queryValues.get(i).get("opening_balance_crordr"));

            database.update(SUPPLIER_DETAILS, values,"supplier_id = ?",new String[] { queryValues.get(i).get("id") });

        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();

    }

    public ArrayList<Shipping_model> getaddshippingaddress(){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.ADD_SHIPPING_ID,Invoicedatabase.ADD_SHIPPING_ADDRESS_LINE1,
                Invoicedatabase.ADD_SHIPPING_ADDRESS_LINE2,Invoicedatabase.ADD_SHIPPING_CITY,
                Invoicedatabase.ADD_SHIPPING_STATE,Invoicedatabase.ADD_SHIPPING_PINCODE,
                Invoicedatabase.ADD_SHIPPING_COUNTRY};
        db.beginTransaction();
        Cursor cursor=db.query(Invoicedatabase.SHIPPING_ADDRESS, columns1, null, null, null, null, null);
        ArrayList<Shipping_model> dealerArrayList=new ArrayList<>();
        while(cursor.moveToNext()){
            Shipping_model shipping=new Shipping_model();
            shipping.shipping_id = cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_SHIPPING_ID));
            shipping.shipping_address1 = cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_SHIPPING_ADDRESS_LINE1));
            shipping.shipping_address2 = cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_SHIPPING_ADDRESS_LINE2));
            shipping.shipping_country = cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_SHIPPING_COUNTRY));
            shipping.shipping_city = cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_SHIPPING_CITY));
            shipping.shipping_state = cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_SHIPPING_STATE));
            shipping.shipping_pincode = cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_SHIPPING_PINCODE));
            dealerArrayList.add(shipping);
        }
        cursor.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return dealerArrayList;
    }

    public void insertaddshippingaddress(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(ADD_SHIPPING_ID, queryValues.get(i).get("shipping_id"));
            values.put(ADD_SHIPPING_ADDRESS_LINE1, queryValues.get(i).get("shipping_address"));
            values.put(ADD_SHIPPING_ADDRESS_LINE2, queryValues.get(i).get("shipping_address1"));
            values.put(ADD_SHIPPING_CITY,queryValues.get(i).get("shipping_city"));
            values.put(ADD_SHIPPING_PINCODE, queryValues.get(i).get("shipping_pincode"));
            values.put(ADD_SHIPPING_STATE,queryValues.get(i).get("shipping_state"));
            values.put(ADD_SHIPPING_COUNTRY, queryValues.get(i).get("shipping_country"));
            database.insert(SHIPPING_ADDRESS, null, values);
        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();

    }


    public void updateaddshippingdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(SHIPPING_ADDRESS_LINE1, queryValues.get(i).get("s_address_line_1"));
            values.put(SHIPPING_ADDRESS_LINE2, queryValues.get(i).get("s_address_line_2"));
            values.put(SHIPPING_CITY,queryValues.get(i).get("s_city"));
            values.put(SHIPPING_PINCODE, queryValues.get(i).get("s_pincode"));
            values.put(SHIPPING_STATE,queryValues.get(i).get("s_state"));
            values.put(SHIPPING_COUNTRY, queryValues.get(i).get("s_country"));
            database.update(SHIPPING_ADDRESS, values,"id = ?",new String[] { queryValues.get(i).get("id") });

        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();

    }

    public void deletecustomerdetails(String id){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        database.delete(CUSTOMER_DETAILS, "customer_id = ?",new String[] {id });

        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }


    public void deletesupplierdetails(String id){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        database.delete(SUPPLIER_DETAILS, "supplier_id = ?",new String[] {id });

        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }





    public ArrayList<Product_model> getproductdetails(){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.PRODUCT_DETAILS_ID,Invoicedatabase.PRODUCT_CODE,
                Invoicedatabase.PRODUCT_DESC,Invoicedatabase.PRODUCT_RATE,Invoicedatabase.PRODUCT_UNIT,Invoicedatabase.PRODUCT_HSN,
        Invoicedatabase.PRODUCT_GSTRATE,Invoicedatabase.USER_ID,Invoicedatabase.PRODUCT_CESS,Invoicedatabase.PRODUCT_UNIT_POS};
        db.beginTransaction();
        Cursor cursor=db.query(Invoicedatabase.PRODUCT_DETAILS, columns1, null, null, null, null, null);
        ArrayList<Product_model> productArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            Product_model productdetail=new Product_model();
            productdetail.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_DETAILS_ID));
            productdetail.productcode=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_CODE));
            productdetail.productdesc=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_DESC));

            productdetail.productrate=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_RATE));
            productdetail.unit=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_UNIT));
            productdetail.hsncode=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_HSN));
            productdetail.gstrate=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_GSTRATE));
            productdetail.cess=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_CESS));
            productdetail.user_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.USER_ID));
            productdetail.unit_pos=cursor.getString(cursor.getColumnIndex(Invoicedatabase.PRODUCT_UNIT_POS));
            productArrayList.add(productdetail);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return productArrayList;


    }




    public void insertProductdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(PRODUCT_DETAILS_ID, queryValues.get(i).get("id"));
            values.put(PRODUCT_CODE, queryValues.get(i).get("product_code"));
            values.put(PRODUCT_DESC, queryValues.get(i).get("product_desc"));
            values.put(PRODUCT_RATE, queryValues.get(i).get("product_rate"));

            values.put(PRODUCT_UNIT, queryValues.get(i).get("product_unit"));
            values.put(PRODUCT_HSN,queryValues.get(i).get("Hsn"));
            values.put(PRODUCT_GSTRATE, queryValues.get(i).get("prod_gst_rate"));
            values.put(USER_ID, queryValues.get(i).get("user_id"));
            values.put(PRODUCT_CESS, queryValues.get(i).get("cess"));
            values.put(PRODUCT_UNIT_POS, queryValues.get(i).get("unit_pos"));
            database.insert(PRODUCT_DETAILS, null, values);


        }
        database.setTransactionSuccessful();
        database.endTransaction();



        database.close();

    }


    public void updateProductdetails(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();


        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(PRODUCT_DETAILS_ID, queryValues.get(i).get("id"));
            values.put(PRODUCT_RATE, queryValues.get(i).get("product_rate"));

            values.put(PRODUCT_UNIT, queryValues.get(i).get("product_unit"));
            values.put(PRODUCT_HSN,queryValues.get(i).get("Hsn"));
            values.put(PRODUCT_GSTRATE, queryValues.get(i).get("prod_gst_rate"));
            values.put(USER_ID, queryValues.get(i).get("user_id"));
            values.put(PRODUCT_CESS, queryValues.get(i).get("cess"));
            values.put(PRODUCT_UNIT_POS, queryValues.get(i).get("unit_pos"));
            database.update(PRODUCT_DETAILS, values,"PRODUCT_CODE = ?",new String[] { queryValues.get(i).get("product_code") });
        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();

    }



    public void updateProductitems(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();

           /* values.put(ADD_PRODUCT_CODE, queryValues.get(i).get("product_code"));
            values.put(ADD_PRODUCT_DESC, queryValues.get(i).get("product_desc"));*/
            values.put(ADD_PRODUCT_QUANTITY, queryValues.get(i).get("prod_qty"));
            values.put(PRODUCT_GSTRATE, queryValues.get(i).get("prod_gst_rate"));
            values.put(ADD_PRODUCT_RATE, queryValues.get(i).get("prod_rate"));
            values.put(ADD_PRODUCT_DISCOUNT, queryValues.get(i).get("prod_disc"));
            values.put(ADD_PRODUCT_TOTAL, queryValues.get(i).get("total_amount"));
            values.put(ADD_PRODUCT_DISCOUNT_TYPE, queryValues.get(i).get("discount_type"));
            values.put(ADD_PRODUCT_CGST, queryValues.get(i).get("cgst"));
            values.put(ADD_PRODUCT_SGST, queryValues.get(i).get("sgst"));
            values.put(ADD_PRODUCT_IGST, queryValues.get(i).get("igst"));
            values.put(ADD_PRODUCT_CGST_PERCENT, queryValues.get(i).get("cgst_percent"));
            values.put(ADD_PRODUCT_SGST_PERCENT, queryValues.get(i).get("sgst_percent"));
            values.put(ADD_PRODUCT_IGST_PERCENT, queryValues.get(i).get("igst_percent"));
            values.put(ADD_PRODUCT_TAT, queryValues.get(i).get("TAT"));

            database.update(ADD_ITEMS, values,"product_code = ?",new String[] { queryValues.get(i).get("product_code") });

        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }



    public void deleteProductdetails(String product_code){



        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        database.delete(PRODUCT_DETAILS, "product_code = ?",new String[] {product_code });
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }




    public void delete(){
    SQLiteDatabase database = this.getWritableDatabase();
    database.beginTransaction();
    database.delete(CUSTOMER_DETAILS, null, null);
        database.delete(SHIPPING_ADDRESS, null, null);
    database.delete(USER_DETAILS,null,null);
        database.delete(PRODUCT_DETAILS,null,null);
        database.delete(BANK_DETAILS,null,null);
        database.delete(SUPPLIER_DETAILS,null,null);
    database.setTransactionSuccessful();
    database.endTransaction();
    database.close();
}



    public void addProductitems(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(ADD_PRODUCT_DETAILS_ID, queryValues.get(i).get("id"));
            values.put(ADD_PRODUCT_CODE, queryValues.get(i).get("product_code"));
            values.put(ADD_PRODUCT_DESC, queryValues.get(i).get("product_desc"));
            values.put(ADD_PRODUCT_UNIT, queryValues.get(i).get("product_unit"));
            values.put(ADD_PRODUCT_HSN,queryValues.get(i).get("Hsn"));
            values.put(ADD_PRODUCT_GSTRATE, queryValues.get(i).get("prod_gst_rate"));
            values.put(ADD_PRODUCT_CESS, queryValues.get(i).get("cess"));
            values.put(ADD_PRODUCT_QUANTITY, queryValues.get(i).get("prod_qty"));
            values.put(ADD_PRODUCT_RATE, queryValues.get(i).get("prod_rate"));
            values.put(ADD_PRODUCT_DISCOUNT, queryValues.get(i).get("prod_disc"));
            values.put(ADD_PRODUCT_TOTAL, queryValues.get(i).get("total_amount"));
            values.put(ADD_PRODUCT_DISCOUNT_TYPE, queryValues.get(i).get("discount_type"));
            values.put(ADD_PRODUCT_CGST, queryValues.get(i).get("cgst"));
            values.put(ADD_PRODUCT_SGST, queryValues.get(i).get("sgst"));
            values.put(ADD_PRODUCT_IGST, queryValues.get(i).get("igst"));
            values.put(ADD_PRODUCT_CGST_PERCENT, queryValues.get(i).get("cgst_percent"));
            values.put(ADD_PRODUCT_SGST_PERCENT, queryValues.get(i).get("sgst_percent"));
            values.put(ADD_PRODUCT_IGST_PERCENT, queryValues.get(i).get("igst_percent"));
            values.put(ADD_PRODUCT_TAT, queryValues.get(i).get("TAT"));
            database.insert(ADD_ITEMS, null, values);

        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }


    public ArrayList<Invoice_Product_model> getaddproductitems(){

       SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.ADD_PRODUCT_DETAILS_ID,Invoicedatabase.ADD_PRODUCT_CODE,
                Invoicedatabase.ADD_PRODUCT_DESC,Invoicedatabase.ADD_PRODUCT_UNIT,Invoicedatabase.ADD_PRODUCT_HSN,
                Invoicedatabase.ADD_PRODUCT_GSTRATE,Invoicedatabase.ADD_PRODUCT_CESS,Invoicedatabase.ADD_PRODUCT_DISCOUNT,Invoicedatabase.ADD_PRODUCT_QUANTITY,
        Invoicedatabase.ADD_PRODUCT_RATE,Invoicedatabase.ADD_PRODUCT_TOTAL,Invoicedatabase.ADD_PRODUCT_DISCOUNT_TYPE,
        Invoicedatabase.ADD_PRODUCT_CGST,Invoicedatabase.ADD_PRODUCT_SGST,Invoicedatabase.ADD_PRODUCT_IGST,
        Invoicedatabase.ADD_PRODUCT_CGST_PERCENT,Invoicedatabase.ADD_PRODUCT_SGST_PERCENT,Invoicedatabase.ADD_PRODUCT_IGST_PERCENT,
                Invoicedatabase.ADD_PRODUCT_TAT
        };
        db.beginTransaction();
        Cursor cursor=db.query(Invoicedatabase.ADD_ITEMS, columns1, null, null, null, null, null);
        ArrayList<Invoice_Product_model> addItemsArraylist=new ArrayList<>();
        while(cursor.moveToNext()){
            Invoice_Product_model product=new Invoice_Product_model();
            product.id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_DETAILS_ID));
            product.productcode=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_CODE));
            product.productdesc=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_DESC));
            product.unit=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_UNIT));
            product.hsncode=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_HSN));
            product.gstrate=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_GSTRATE));
            product.cess=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_CESS));
            product.quantity=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_QUANTITY));
            product.discount=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_DISCOUNT));
            product.amount=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_RATE));
            product.grossamount=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_TOTAL));
            product.discountype=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_DISCOUNT_TYPE));
            product.cgst=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_CGST));
            product.sgst=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_SGST));
            product.igst=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_IGST));
            product.cgst_percent=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_CGST));
            product.sgst_percent=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_SGST_PERCENT));
            product.igst_percent=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_IGST_PERCENT));
            product.tat=cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_TAT));
            addItemsArraylist.add(product);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return addItemsArraylist;
    }


    public ArrayList<invoice_list_model> getinvoicedetails(){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.INVOICE_LIST_ID,Invoicedatabase.INVOICE_LIST_INVOICE_NO,
                Invoicedatabase.INVOICE_LIST_CUSTOMER_NAME,Invoicedatabase.INVOICE_LIST_TAT,Invoicedatabase.INVOICE_LIST_PERSON_TYPE
              };
        db.beginTransaction();
        Cursor cursor=db.query(Invoicedatabase.INVOICE_LIST_TABLE, columns1, null, null, null, null, null);
        ArrayList<invoice_list_model> invoiceArrayList=new ArrayList<>();

        while(cursor.moveToNext()){
            invoice_list_model invlist=new invoice_list_model();
            invlist.invoice_id=cursor.getString(cursor.getColumnIndex(Invoicedatabase.INVOICE_LIST_ID));
            invlist.invoice_no=cursor.getString(cursor.getColumnIndex(Invoicedatabase.INVOICE_LIST_INVOICE_NO));
            invlist.customer_name=cursor.getString(cursor.getColumnIndex(Invoicedatabase.INVOICE_LIST_CUSTOMER_NAME));

            invlist.tat=cursor.getString(cursor.getColumnIndex(Invoicedatabase.INVOICE_LIST_TAT));
            invlist.person_type=cursor.getString(cursor.getColumnIndex(Invoicedatabase.INVOICE_LIST_PERSON_TYPE));

            invoiceArrayList.add(invlist);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return invoiceArrayList;
    }

    public void insertinvoicelist(ArrayList<HashMap<String, String>> queryValues){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        for(int i=0;i<queryValues.size();i++){
            ContentValues values = new ContentValues();
            values.put(INVOICE_LIST_ID, queryValues.get(i).get("id"));
            values.put(INVOICE_LIST_INVOICE_NO, queryValues.get(i).get("invoiveno"));
            values.put(INVOICE_LIST_CUSTOMER_NAME, queryValues.get(i).get("customer_name"));
            values.put(INVOICE_LIST_TAT, queryValues.get(i).get("tat"));
            values.put(INVOICE_LIST_PERSON_TYPE, queryValues.get(i).get("person_type"));
            database.insert(INVOICE_LIST_TABLE, null, values);
        }
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }








    public boolean searchproductcodeexists(String code){
        SQLiteDatabase db=getWritableDatabase();
       Cursor cursor = db.rawQuery("SELECT product_code FROM " + Invoicedatabase.ADD_ITEMS
                + " WHERE " + "product_code" + "='" + code+ "'", null);
        if ( cursor.getCount() > 0 )
        {
          return true;
        }

        cursor.close();
        db.close();
        return false;
    }



    public boolean searchproductcode(String code){
        SQLiteDatabase db=getWritableDatabase();



        Cursor cursor = db.rawQuery("SELECT * FROM " + Invoicedatabase.PRODUCT_DETAILS
                + " WHERE " + "product_code" + "='" + code+ "'", null);

        if ( cursor.getCount() > 0 )
        {
            return true;
        }

        cursor.close();


        db.close();
        return false;
    }


    public boolean searchcustomername(String name){
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Invoicedatabase.CUSTOMER_DETAILS
                + " WHERE " + "customer_name" + "='" + name+ "'", null);
        if ( cursor.getCount() > 0 )
        {
            return true;
        }
        cursor.close();
        db.close();
        return false;
    }

    public boolean searchsuppliername(String name){
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Invoicedatabase.SUPPLIER_DETAILS
                + " WHERE " + "supplier_name" + "='" + name+ "'", null);
        if ( cursor.getCount() > 0 )
        {
            return true;
        }
        cursor.close();
        db.close();
        return false;
    }



    public int getaddproductitemscount(){
        SQLiteDatabase db=getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+Invoicedatabase.ADD_ITEMS
          ,null);
        if ( cursor.getCount() > 0 )
        {
            return cursor.getCount();
        }
        cursor.close();
        db.close();
        return cursor.getCount();
    }

    public void deleteaddproductitem(String code){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        database.delete(ADD_ITEMS, "product_code = ?",new String[] {code });
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }

    /*public HashMap<String,String> getadditembycode(String code){
        SQLiteDatabase db=getWritableDatabase();
        String[] columns1={Invoicedatabase.ADD_PRODUCT_DETAILS_ID,Invoicedatabase.ADD_PRODUCT_CODE,
                Invoicedatabase.ADD_PRODUCT_DESC,Invoicedatabase.ADD_PRODUCT_UNIT,Invoicedatabase.ADD_PRODUCT_HSN,
                Invoicedatabase.ADD_PRODUCT_GSTRATE,Invoicedatabase.ADD_PRODUCT_CESS,Invoicedatabase.ADD_PRODUCT_DISCOUNT,Invoicedatabase.ADD_PRODUCT_QUANTITY,
                Invoicedatabase.ADD_PRODUCT_RATE,Invoicedatabase.ADD_PRODUCT_TOTAL};
        db.beginTransaction();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Invoicedatabase.PRODUCT_DETAILS
                + " WHERE " + "product_code" + "='" + code+ "'", null);
        HashMap<String,String> prodvals=new HashMap<>();

        while(cursor.moveToNext()){

            prodvals.put("id",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_DETAILS_ID)));
            prodvals.put("prod_code",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_CODE)));
            prodvals.put("prod_desc",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_DESC)));
            prodvals.put("unit",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_UNIT)));
            prodvals.put("hsn_code",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_HSN)));
            prodvals.put("gst_rate",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_GSTRATE)));
            prodvals.put("cess",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_CESS)));
            prodvals.put("prod_qty",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_QUANTITY)));
            prodvals.put("prod_disc",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_DISCOUNT)));
            prodvals.put("prod_fees",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_RATE)));
            prodvals.put("prod_total",cursor.getString(cursor.getColumnIndex(Invoicedatabase.ADD_PRODUCT_TOTAL)));


        }

        db.setTransactionSuccessful();
        db.endTransaction();
        return prodvals;

    }*/


   /* public ArrayList<String> getaddproductitembyposition(String position){
        SQLiteDatabase db=getWritableDatabase();



        Cursor cursor = db.rawQuery("SELECT rowid,product_code FROM " + Invoicedatabase.ADD_ITEMS
                + " WHERE " + "rowid" + "='" + position+ "'", null);

        if ( cursor.getCount() > 0 )
        {
            cursor.moveToFirst();

            ArrayList<String> x=new ArrayList<String>();

            String data = cursor.getString(cursor.getColumnIndex("rowid"));
            String data1 = cursor.getString(cursor.getColumnIndex("product_code"));

            x.add(data);
            x.add(data1);


            return x;
        }

        cursor.close();


        db.close();
        return null;
    }

*/


    public void deleteaddproductitems(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        database.delete(ADD_ITEMS, null, null);
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }

    public void deleteaaddshippingaddress(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        database.delete(SHIPPING_ADDRESS, null, null);
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }
    public void deleteinvoicelist(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.beginTransaction();
        database.delete(INVOICE_LIST_TABLE, null, null);
        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }


}
