package facile.creditanddebitnote;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.Invoiceitemsadapter;
import facile.Model.Bank_details_model;
import facile.Model.Credit_note_details;
import facile.Model.Debit_note_details;
import facile.Model.Debit_note_purchase_details;
import facile.Model.Invoice_Product_model;
import facile.Model.Invoice_calc;
import facile.Model.User_detail;
import facile.Model.invoice_customer_model;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.EnglishNumberToWords;
import facile.util.SimpleDividerItemDecoration;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 8/2/18.
 */


public class Credit_note_preview  extends AppCompatActivity {
    RelativeLayout relativeLayout;
    String dirpath;
    File file;
    TextView tv_phoneno,tv_companyname,tv_address,tv_gstno,tv_date,tv_invoiceno,tv_customername,
            tv_customeraddress,tv_buyergstno,tv_placeofsupply,tv_tat,tv_cgst,tv_sgst,tv_igst,tv_tot,
            tv_bankname,tv_branch,tv_acno,tv_ifsc,tv_forcustomer,tv_termsofuse,tv_amountinwords;
    String str_phoneno,str_companyname,str_address,str_gstno,str_date,str_invoiceno,str_p_invoiceno_s,str_customername,
            str_customeraddress,str_customeraddress1,str_buyergstno,str_placeofsupply,str_tat,str_total,str_cgst,str_sgst,str_igst,
            str_salestype,str_transportmode,str_rc,str_customerphno,str_customercity,str_customerstate,str_customerstatepos,
            str_customermailid,str_customerigst,str_customerpincode,str_freight,str_other_charges,str_dateval;
    public String str_invoice_id,str_credit_note_date,str_credit_note_no,str_credit_note_p_no;
    TextView txt_inv_no,txt_tax_invoice;
    String Filename;
    Button btn_save;
    Invoicedatabase invoicedb;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    RequestQueue requestQueue;
    public static final String UTF8_BOM = "\uFEFF";
    ProgressDialog progressDialog;
    String url;
    TextView toasttext,tv_title;
    View layout;
    prefManager prefs;
    Context context;
    ImageView iv_companylogo;
    Bitmap bitmap;
    String crordr;
    String progressmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invoice_layout);
        invoicedb=new Invoicedatabase(this);
        prefs = new prefManager(this);
        context=this;
        tv_title=(TextView)findViewById(R.id.title);
        Intent intent = getIntent();
       if(intent.hasExtra("Debit")){
            tv_title.setText(R.string.dn_preview);
            crordr="D";

        }
        else if(intent.hasExtra("Debitp")){
           tv_title.setText(R.string.dn_preview);
           crordr="P";

       }
        else{
            tv_title.setText(R.string.cn_preview);
            crordr="C";
        }
        relativeLayout=(RelativeLayout) findViewById(R.id.rlmain);
        txt_inv_no=(TextView)findViewById(R.id.tv_invoicenumber);
        txt_tax_invoice=(TextView)findViewById(R.id.tv_taxinvoice);
        btn_save=(Button)findViewById(R.id.btn_save);
        tv_phoneno=(TextView)findViewById(R.id.tv_phoneno);
        tv_companyname=(TextView)findViewById(R.id.tv_companyname);
        tv_address=(TextView)findViewById(R.id.tv_address);
        tv_gstno=(TextView)findViewById(R.id.tv_gstnotxt);
        tv_invoiceno=(TextView)findViewById(R.id.tv_invoicenotxt);
        tv_date=(TextView)findViewById(R.id.tv_datetxt);
        tv_customername=(TextView)findViewById(R.id.tv_customername);
        tv_customeraddress=(TextView)findViewById(R.id.tv_customeraddress);
        tv_buyergstno=(TextView)findViewById(R.id.tv_buyergstnotxt);
        tv_placeofsupply=(TextView)findViewById(R.id.tv_placeofsupplytxt);
        tv_tot=(TextView)findViewById(R.id.tv_totalbeforetaxtxt);
        tv_amountinwords = (TextView) findViewById(R.id.txt_amountinwordstxt);
        tv_cgst=(TextView)findViewById(R.id.tv_cgsttxt);
        tv_sgst=(TextView)findViewById(R.id.tv_sgsttxt);
        tv_igst=(TextView)findViewById(R.id.tv_igsttxt);
        tv_tat=(TextView)findViewById(R.id.tv_totaltxt);
        tv_bankname=(TextView)findViewById(R.id.tv_banknametxt);
        tv_branch=(TextView)findViewById(R.id.tv_branchnametxt);
        tv_acno=(TextView)findViewById(R.id.tv_acnotxt);
        tv_ifsc=(TextView)findViewById(R.id.tv_ifsctxt);
        tv_termsofuse=(TextView)findViewById(R.id.tv_termsofusetxt) ;
        iv_companylogo=(ImageView)findViewById(R.id.iv_companylogo);
        tv_forcustomer=(TextView)findViewById(R.id.tv_forcustomer);
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        layout = li.inflate(R.layout.customtoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layout.findViewById(R.id.custom_toast_message);
        ArrayList<User_detail> items= invoicedb.getuserdetails();
        tv_companyname.setText(items.get(0).user_trade_name);
        tv_gstno.setText(items.get(0).gst_no);
        if(items.get(0).address1.isEmpty()||items.get(0).address1==null) {
            tv_address.setText(items.get(0).address +  "\n" + items.get(0).city + "," + items.get(0).state
                    + "-" + items.get(0).pincode);
        }
        else{
            tv_address.setText(items.get(0).address + "\n" + items.get(0).address1 + "\n" + items.get(0).city + "," + items.get(0).state
                    + "-" + items.get(0).pincode);

        }
        tv_phoneno.setText(items.get(0).phone_no);
        tv_placeofsupply.setText(items.get(0).state);
        tv_forcustomer.setText(items.get(0).user_trade_name);
        tv_termsofuse.setText(items.get(0).terms_of_use);
        ArrayList<Bank_details_model> bankdetails = invoicedb.getbankdetails();
        if(bankdetails.size()>0) {
            tv_bankname.setText(bankdetails.get(0).bank_name);
            tv_branch.setText(bankdetails.get(0).branch);
            tv_ifsc.setText(bankdetails.get(0).ifsc);
            tv_acno.setText(bankdetails.get(0).ac_no);
        }


        if(crordr.equals("P")) {
            Debit_note_purchase_details debit_note = intent.getParcelableExtra("debit_note");
            str_credit_note_no = debit_note.debit_note_no;
            str_dateval = debit_note.str_dateval;
            str_credit_note_date=debit_note.str_dateval;
            str_invoiceno=debit_note.invoice_no;
            txt_inv_no.setText(R.string.debitnote_num_preview);
            txt_tax_invoice.setText(R.string.title_debitnote);
            str_credit_note_p_no=str_credit_note_no;
        }


          else  if(crordr.equals("D")){
    Debit_note_details debit_note=intent.getParcelableExtra("debit_note");
    str_date=debit_note.debit_note_date;
    str_invoiceno=debit_note.invoice_no;
    str_invoice_id=debit_note.invoice_id;
    str_credit_note_date=debit_note.debit_note_date;
    str_credit_note_no=debit_note.debit_note_no;
    str_credit_note_p_no=debit_note.debit_note_p_no;
    str_dateval=debit_note.str_dateval;
    txt_inv_no.setText(R.string.debitnote_num_preview);
    txt_tax_invoice.setText(R.string.title_debitnote);

}else{
    Credit_note_details credit_note=intent.getParcelableExtra("credit_note");
    str_date=credit_note.credit_note_date;
    str_invoiceno=credit_note.invoice_no;
    str_invoice_id=credit_note.invoice_id;
    str_credit_note_date=credit_note.credit_note_date;
    str_credit_note_no=credit_note.credit_note_no;
    str_credit_note_p_no=credit_note.credit_note_p_no;
    str_dateval=credit_note.str_dateval;
    txt_inv_no.setText(R.string.creditnote_num_preview);
    txt_tax_invoice.setText(R.string.title_creditnote);
}
        Invoice_calc calc=intent.getParcelableExtra("credit_note_calc");
        invoice_customer_model customer = intent.getParcelableExtra("customer");
        str_customername=customer.customer_name;
        str_customeraddress=customer.address;
        str_customeraddress1=customer.address1;
        str_buyergstno=customer.gst_no;
        str_customerphno=customer.phone_no;
        str_customerstate=customer.state;
        str_customerstatepos=customer.state_pos;
        str_customercity=customer.city;
        str_customermailid=customer.mail_id;
        str_customerigst=customer.igst;
        str_customerpincode=customer.pincode;

        tv_invoiceno.setText(str_credit_note_p_no);

        str_cgst=calc.cgst;
        str_sgst=calc.sgst;
        str_igst=calc.igst;
        str_tat=calc.total_after_tax;
        str_total=calc.total_before_tax;
        str_freight=calc.freight;
        str_other_charges=calc.other_charges;
        String[] arr = String.valueOf(str_tat).split("\\.");
        int[] intArr = new int[2];
        intArr[0] = Integer.parseInt(arr[0]);
        intArr[1] = Integer.parseInt(arr[1]);
        Long amnt_whole_no = Long.valueOf(intArr[0]);
        Long amnt_decimal_no = Long.valueOf(intArr[1]);
        String amntinwrds = EnglishNumberToWords.convert(amnt_whole_no);
        String decwrds = EnglishNumberToWords.convert(amnt_decimal_no);
        String rupees_amnt = amntinwrds.substring(0, 1).toUpperCase() + amntinwrds.substring(1);

        if (intArr[1] == 0) {
            tv_amountinwords.setText(rupees_amnt + " rupees  only");

        } else {


            tv_amountinwords.setText(rupees_amnt + " rupees and " + decwrds + " paise only");
        }

        tv_customername.setText(str_customername);
        if((str_customeraddress==null)||str_customeraddress.isEmpty()){
            tv_customeraddress.setText(str_customerstate);
        }


        else  if((str_customeraddress1==null)||str_customeraddress1.isEmpty()){
            tv_customeraddress.setText(str_customeraddress+"\n"+str_customercity+","+str_customerstate
                    +"-"+str_customerpincode);
        }
        else {
            tv_customeraddress.setText(str_customeraddress + "," + str_customeraddress1 + "\n" + str_customercity + "," + str_customerstate
                    + "-" + str_customerpincode);
        }
        tv_buyergstno.setText(str_buyergstno);
        tv_date.setText(str_credit_note_date);
        tv_cgst.setText(str_cgst);
        tv_sgst.setText(str_sgst);
        tv_igst.setText(str_igst);
        tv_tat.setText(str_tat);
        tv_tot.setText(str_total);
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(prefs.getLogo()));
        }
        catch(Exception e){
        }
        iv_companylogo.setImageBitmap(bitmap);
        mRecyclerView = (RecyclerView)findViewById(R.id.rv_proddetail);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new Invoiceitemsadapter(this,invoicedb.getaddproductitems());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(crordr.equals("C")) {
                    url = Constants.CREDIT_NOTE_MAIN;
                    progressmsg="Saving Credit note values..";
                }
                else  if(crordr.equals("D")) {
                    url = Constants.DEBIT_NOTE_MAIN;
                    progressmsg="Saving Debit note values..";
                }
                else  if(crordr.equals("P")) {
                    url = Constants.DEBIT_NOTE_PURCHASE_MAIN;
                    progressmsg="Saving Debit note values..";
                }


                requestQueue = Volley.newRequestQueue(getApplicationContext());
                progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
                progressDialog.setMessage(progressmsg);
                progressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    String m=response.toString();
                                    String x=     removeUTF8BOM(m);
                                    JSONObject jsonRequest = new JSONObject(x);
                                    String statuscode = jsonRequest.getString("status_code");
                                    if (statuscode.equals("200")) {
                                        Bitmap map = ConvertToBitmap(relativeLayout);
                                        /* saveToInternalStorage(map);*/
                                        saveToExternalStorage(map);
                                    }
                                    else   if (statuscode.equals("400")){
                                        Toast toast = new Toast(getApplicationContext());
                                        toast.setDuration(Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                        toasttext.setText(jsonRequest.getString("status"));
                                        toast.setView(layout);
                                        progressDialog.dismiss();
                                        toast.show();

                                    }

                                    //    Toast.makeText(getApplicationContext(), "" +  cityPost.getCities() ,Toast.LENGTH_LONG).show();


                                } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                                    progressDialog.dismiss();


                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                errorhandler e=new errorhandler();
                                String msg=e.errorhandler(error);
                                Toast toast = new Toast(getApplicationContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(msg);
                                toast.setView(layout);
                                progressDialog.dismiss();
                                toast.show();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        //invoice details params


                          if(crordr.equals("P")) {
                              params.put("debit_note_no", str_credit_note_no);
                              params.put("debit_note_date", str_dateval);
                              params.put("invoice_no", str_invoiceno);

                              params.put("user_id", prefs.getUserId());

                              params.put("supplier_name",str_customername);
                              params.put("gst_no", str_buyergstno);
                              params.put("address", str_customeraddress);

                              params.put("address1", str_customeraddress1);
                              params.put("phone_no", str_customerphno);
                              params.put("mail_id", str_customermailid);

                              params.put("city", str_customercity);
                              params.put("state",str_customerstate);
                              params.put("state_pos", str_customerstatepos);
                              params.put("igst", str_customerigst);

                              params.put("pincode", str_customerpincode);


                          }
else {

                              params.put("invoice_id", str_invoice_id);
                              params.put("user_id", prefs.getUserId());

                              if (crordr.equals("C")) {
                                  params.put("credit_note_no", str_credit_note_no);
                                  params.put("credit_note_date", str_dateval);
                              } else if (crordr.equals("D")) {
                                  params.put("debit_note_no", str_credit_note_no);
                                  params.put("debit_note_date", str_dateval);

                              }
                              params.put("invoice_no", str_invoiceno);
                          }

                        // Product details params
                        ArrayList<Invoice_Product_model> items= invoicedb.getaddproductitems();
                        for(int i=0; i < items.size(); i++)
                        {
                            params.put("product_code["+i+"]",items.get(i).productcode);
                            params.put("product_desc["+i+"]",items.get(i).productdesc);
                            params.put("unit["+i+"]",items.get(i).unit);
                            params.put("hsn_code["+i+"]",items.get(i).hsncode);
                            params.put("gst_rate["+i+"]",items.get(i).gstrate);
                            params.put("cess["+i+"]",items.get(i).cess);
                            params.put("quantity["+i+"]",items.get(i).quantity);
                            params.put("amount["+i+"]",items.get(i).amount);
                            params.put("discount["+i+"]",items.get(i).discount);
                            params.put("discount_type["+i+"]",items.get(i).discountype);
                            params.put("totalbeforetax["+i+"]",items.get(i).grossamount);
                            params.put("cgst["+i+"]",items.get(i).cgst);
                            params.put("sgst["+i+"]",items.get(i).sgst);
                            params.put("igst["+i+"]",items.get(i).igst);
                            params.put("cgst_percent["+i+"]",items.get(i).cgst_percent);
                            params.put("sgst_percent["+i+"]",items.get(i).sgst_percent);
                            params.put("igst_percent["+i+"]",items.get(i).igst_percent);
                            params.put("tat["+i+"]",items.get(i).tat);
                        }

                        //calc params
                        params.put("total_before_tax_f",str_total);
                        params.put("cgst_f",str_cgst);
                        params.put("sgst_f",str_sgst);
                        params.put("igst_f",str_igst);
                        params.put("tat_f",str_tat);
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                        return params;
                    }

                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            }
        });
    }

    public void layoutToImage(Bitmap bmp) {

        try {
            imageToPDF();
        }
        catch (Exception e){

        }

    }


    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(this);
        File mypath;
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Invoice", Context.MODE_PRIVATE);
        // Create imageDir
     /*   if(flag==1) {
            mypath = new File(directory, "shareimgcache.png");
        }
        else{*/
        mypath = new File(directory, System.currentTimeMillis()+"image.jpg");

        // }

     /*   File mypath=new File(directory,"profile.jpg");*/

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);

          /*  addImageToGallery(mypath.getPath(),context);*/
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                imageToPDF();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }


    public void imageToPDF() throws FileNotFoundException {
        try {
            Document document = new Document();

             Filename= "/BizManager/CreditNote/"+str_credit_note_no+".pdf";
            dirpath = android.os.Environment.getExternalStorageDirectory().toString();
            PdfWriter.getInstance(document, new FileOutputStream(dirpath + Filename)); //  Change pdf's name.
            document.open();
            Image img = Image.getInstance(Environment.getExternalStorageDirectory() + File.separator + "/BizManager/image.png");
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / img.getWidth()) * 100;
            img.scalePercent(scaler);
            img.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
            document.add(img);
            document.close();
            progressDialog.dismiss();
            Toast.makeText(this, "Credit note saved successfully!..", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Credit note Generation failed!.."+e, Toast.LENGTH_SHORT).show();

        }
        finally {
            Intent i=new Intent(Credit_note_preview.this,Credit_note_printandshare.class);
            i.putExtra("filename",Filename);
            i.putExtra("DRORCR","CR");
            startActivity(i);
            finish();
           /* Intent intent = new Intent();
            intent.setPackage("com.adobe.reader");
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
*/
        }
    }



    public void imageToPDFdebit() throws FileNotFoundException {
        try {
            Document document = new Document();

            Filename= "/BizManager/DebitNote/"+str_credit_note_no+".pdf";
            dirpath = android.os.Environment.getExternalStorageDirectory().toString();
            PdfWriter.getInstance(document, new FileOutputStream(dirpath + Filename)); //  Change pdf's name.
            document.open();
            Image img = Image.getInstance(Environment.getExternalStorageDirectory() + File.separator + "/BizManager/image.png");
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / img.getWidth()) * 100;
            img.scalePercent(scaler);
            img.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
            document.add(img);
            document.close();
            progressDialog.dismiss();
            Toast.makeText(this, "Debit note saved successfully!..", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Debit note Generation failed!.."+e, Toast.LENGTH_SHORT).show();

        }
        finally {
            Intent i=new Intent(Credit_note_preview.this,Credit_note_printandshare.class);
            i.putExtra("filename",Filename);
            i.putExtra("DRORCR","DR");
            startActivity(i);
            finish();
           /* Intent intent = new Intent();
            intent.setPackage("com.adobe.reader");
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
*/
        }
    }




    private String saveToExternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File mypath;

        File filepath = Environment.getExternalStorageDirectory();

        // Create a new folder in SD Card
        File dir = new File(filepath.getAbsolutePath()
                + "/BizManager/");
        dir.mkdirs();

        if(crordr.equals("C")) {

            File dir1 = new File(filepath.getAbsolutePath()
                    + "/BizManager/CreditNote/");
            dir1.mkdirs();
        }
        else{

            File dir1 = new File(filepath.getAbsolutePath()
                    + "/BizManager/DebitNote/");
            dir1.mkdirs();


        }

        mypath = new File(dir, "image.png");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                fos.close();

                if(crordr.equals("C")) {
                    imageToPDF();
                }
                else{
                    imageToPDFdebit();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dir.getAbsolutePath();
    }

    protected Bitmap ConvertToBitmap(RelativeLayout layout) {
        layout.setDrawingCacheEnabled(true);
        layout.buildDrawingCache();
        return  layout.getDrawingCache();


    }
    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }


}
