package facile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.Dealer_nature_model;
import facile.invoice.R;

/**
 * Created by pradeep on 6/04/18.
 */
public class DealernatureArrayAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<Dealer_nature_model> items;
    private final int mResource;

    public DealernatureArrayAdapter(Context context, int resource,
                                  ArrayList<Dealer_nature_model> objects) {

        items = objects;
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;

    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView dealerid = (TextView) view.findViewById(R.id.tv_dealerid);
        TextView dealername = (TextView) view.findViewById(R.id.tv_dealertype);
        dealername.setText(items.get(position).dealer_nature);
        dealerid.setText(items.get(position).dealer_nature_id);

        dealername.setTextSize(12);
        return view;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}

