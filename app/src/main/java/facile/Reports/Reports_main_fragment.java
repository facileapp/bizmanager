package facile.Reports;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;

/**
 * Created by pradeep on 12/2/18.
 */

public class Reports_main_fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView tv_purchasereport,tv_reportclosure,tv_salesreport,tv_partywisereport,tv_creditnote,tv_debitnote;

Invoicedatabase invoicedatabase;

    private Reports_main_fragment.OnFragmentInteractionListener mListener;

    public Reports_main_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Reports_main_fragment newInstance(String param1, String param2) {
        Reports_main_fragment fragment = new Reports_main_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reports_main, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_report));
        invoicedatabase=new Invoicedatabase(getContext());

        tv_purchasereport=(TextView)view.findViewById(R.id.tv_purchasereport);

        tv_salesreport=(TextView)view.findViewById(R.id.tv_salereport);
        tv_creditnote=(TextView)view.findViewById(R.id.tv_creditnote);
        tv_debitnote=(TextView)view.findViewById(R.id.tv_debitnote);

        tv_salesreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sales_report fragment = new Sales_report();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("SORP","S");
                fragment.setArguments(bundle);
                fragmentTransaction.commit();
            }
        });
        tv_purchasereport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Sales_report fragment = new Sales_report();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("SORP", "P");
                fragment.setArguments(bundle);
                fragmentTransaction.commit();


            }
        });

        tv_creditnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CrDr_report_main fragment = new CrDr_report_main();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("CRORDR", "CR");
                fragment.setArguments(bundle);
                fragmentTransaction.commit();

            }
        });
        tv_debitnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Debit_note_salesorpurchase fragment = new Debit_note_salesorpurchase();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
              /*  Bundle bundle = new Bundle();
                bundle.putString("CRORDR", "DR");
                fragment.setArguments(bundle);*/
                fragmentTransaction.commit();


            }
        });




        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

