package facile.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pradeep on 12/3/18.
 */

public class invoice_customer_model implements Parcelable {


    public String address;
    public String address1;
    public String phone_no;
    public String mail_id;
    public String city;
    public String state;
    public String customer_name;
    public String pincode;
    public String gst_no;
    public String state_pos;
    public String igst;




    public invoice_customer_model(Parcel in) {
        customer_name = in.readString();
        gst_no=in.readString();
        address = in.readString();
        address1 = in.readString();
        phone_no = in.readString();
        mail_id = in.readString();
        city = in.readString();
        state = in.readString();
        pincode = in.readString();
        state_pos=in.readString();
        igst=in.readString();
    }

    public invoice_customer_model( String customer_name,String gst_no,String address,String address1, String phone_no,
                                   String mail_id, String city,String state, String pincode,String state_pos,String igst){
        this.customer_name = customer_name;
        this.gst_no = gst_no;
        this.address = address;
        this.address1 = address1;
        this.phone_no = phone_no;
        this.mail_id=mail_id;
        this.city=city;
        this.state=state;
        this.pincode=pincode;
        this.state_pos=state_pos;
        this.igst=igst;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(customer_name);
        dest.writeString(gst_no);
        dest.writeString(address);
        dest.writeString(address1);
        dest.writeString(phone_no);
        dest.writeString(mail_id);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(pincode);
        dest.writeString(state_pos);
        dest.writeString(igst);

    }

    public static final Parcelable.Creator<invoice_customer_model> CREATOR = new Parcelable.Creator<invoice_customer_model>() {

        public invoice_customer_model createFromParcel(Parcel in) {
            return new invoice_customer_model(in);
        }

        public invoice_customer_model[] newArray(int size) {
            return new invoice_customer_model[size];
        }

    };


}
