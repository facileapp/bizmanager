package facile.Purchase;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.AutocompletesupplierAdapter;
import facile.Adapter.additemadapter;
import facile.Model.Invoice_Product_model;
import facile.Model.Supplier_model;
import facile.invoice.Additem_fragment;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.DatePickerFragment;
import facile.util.SeparatorDecoration;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 12/2/18.
 */

public class Purchase_bill_main extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView toasttext;
    String str_date;
    View layouttoast;
    Button submit, additem,btn_total;
    EditText edt_inv_date;
    TextView txt_gstno;
    AutocompletesupplierAdapter adapter;
    additemadapter Itemadapter;
    RecyclerView mRecyclerView;
    Invoicedatabase invoicedb;

    ArrayList<Invoice_Product_model> invoicproductemodel;
    private RecyclerView.LayoutManager mLayoutManager;

    String str_customername,str_gstno,str_address;
    EditText edt_address,edt_address1,edt_pincode,edt_city,edt_state;

    EditText edt_freight,edt_others;
    prefManager prefs;
    String str_igst="N";
    Double freight=0.0,others=0.0,freightbt=0.0,othersbt=0.0;
    String Str_customer_name,gst_no,address,address1,phone_no,mail_id,city,state,pincode,state_pos,
       str_invoiceno;
    RequestQueue requestQueue;
    public static final String UTF8_BOM = "\uFEFF";
    String  str_dateval;
    EditText edt_invoiceno;
    ProgressDialog progressDialog;
    String url;
    double temp;


    private Purchase_bill_main.OnFragmentInteractionListener mListener;
    TextView tv_totalaftertax,tv_igst,tv_cgst,tv_sgst,tv_totalbeforetax;




    public Purchase_bill_main() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Purchase_bill_main newInstance(String param1, String param2) {
        Purchase_bill_main fragment = new Purchase_bill_main();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.purchase_bill_main, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.nav_item_purchase));
        prefs = new prefManager(getContext());
        edt_inv_date = (EditText) view.findViewById(R.id.edt_invoicedate);

        additem = (Button) view.findViewById(R.id.btn_additem);
        btn_total = (Button) view.findViewById(R.id.btn_total);

        edt_invoiceno=(EditText)view.findViewById(R.id.tv_invoicenumber);
        tv_totalaftertax=(TextView)view.findViewById(R.id.tv_totalaftertax);
        tv_totalbeforetax=(TextView)view.findViewById(R.id.tv_totalbeforetax);
        tv_cgst=(TextView)view.findViewById(R.id.tv_cgst);
        tv_sgst=(TextView)view.findViewById(R.id.tv_sgst);
        tv_igst=(TextView)view.findViewById(R.id.tv_igst);

        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_invoiceitems);

        txt_gstno = (TextView) view.findViewById(R.id.txt_gstno);
        txt_gstno.setText(gst_no);
        edt_address=(EditText)view.findViewById(R.id.edt_address);
        edt_address1=(EditText)view.findViewById(R.id.edt_address1);
        edt_city=(EditText)view.findViewById(R.id.edt_city);
        edt_pincode=(EditText)view.findViewById(R.id.edt_pincode);
        edt_state=(EditText)view.findViewById(R.id.edt_state);

        edt_freight=(EditText)view.findViewById(R.id.edt_freight);
        edt_others=(EditText)view.findViewById(R.id.edt_others);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        SeparatorDecoration decoration = new SeparatorDecoration(getContext(), Color.GRAY,0.5f);
        mRecyclerView.addItemDecoration(decoration);
        final AutoCompleteTextView edt_customername = (AutoCompleteTextView) view.findViewById(R.id.edt_suppliername);
        edt_customername.setThreshold(1);
        invoicedb = new Invoicedatabase(getContext());
        invoicproductemodel=new ArrayList<Invoice_Product_model>();
        long date = System.currentTimeMillis();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
        dateFormat.applyPattern("dd/MM/yy");
        String dateString = dateFormat.format(date);
        edt_inv_date.setText(dateString);


        Itemadapter = new additemadapter(invoicedb.getaddproductitems(),getActivity());
        mRecyclerView.setAdapter(Itemadapter);
        ((additemadapter) Itemadapter).setOnItemClickListener(new additemadapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Additem_fragment fragment = new Additem_fragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("prod_code",invoicedb.getaddproductitems().get(position).productcode);
                bundle.putString("prod_desc",invoicedb.getaddproductitems().get(position).productdesc);
                bundle.putString("prod_fees",invoicedb.getaddproductitems().get(position).amount);
                bundle.putString("prod_qty",invoicedb.getaddproductitems().get(position).quantity);
                bundle.putString("prod_disc",invoicedb.getaddproductitems().get(position).discount);
                bundle.putString("prod_disc_type",invoicedb.getaddproductitems().get(position).discountype);
                bundle.putString("prod_unit",invoicedb.getaddproductitems().get(position).unit);
                bundle.putString("gst_rate",invoicedb.getaddproductitems().get(position).gstrate);
                bundle.putString("Igst",str_igst);
                bundle.putString("type","I");
                bundle.putString("business_type","1");

                fragment.setArguments(bundle);
                fragmentTransaction.commit();


            }
        });






        edt_inv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showDatePicker();

            }
        });

        adapter = new AutocompletesupplierAdapter(getContext(),R.layout.autocomplete_row,invoicedb.getsupplierdetails());
        edt_customername.setAdapter(adapter);
        edt_customername.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                Supplier_model model = (Supplier_model) view.getTag();

                edt_customername.setText(model.supplier_name);
                if(model.state_pos.equals(prefs.getState())){
                    str_igst="N";
                }
                else{
                    str_igst="Y";
                }
                Str_customer_name=model.supplier_name;
                gst_no=model.gst_no;
                phone_no=model.phone_no;
                mail_id=model.mail_id;
                address=model.address;
                address1=model.address1;
                city=model.city;
                state=model.state;
                pincode=model.pincode;
                state_pos=model.state_pos;


                invoicedb.deleteaddproductitems();
                Itemadapter = new additemadapter(invoicedb.getaddproductitems(),getActivity());
                mRecyclerView.setAdapter(Itemadapter);


                txt_gstno.setText(model.gst_no);


                    edt_address.setText(model.address);
                    edt_address1.setText(model.address1);
                    edt_city.setText(model.city);
                    edt_state.setText(model.state);
                    edt_pincode.setText(model.pincode);



            }
        });


        additem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strcustomer=edt_customername.getText().toString();
                if(TextUtils.isEmpty(strcustomer)){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Please select customer to add items");
                    toast.setView(layouttoast);
                    toast.show();
                    return;

                }
                else  if (invoicedb.searchsuppliername(edt_customername.getText().toString()) == false) {
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Supplier not found");
                    edt_address.setText("");
                    edt_address1.setText("");
                    edt_city.setText("");
                    edt_state.setText("");
                    edt_pincode.setText("");
                    txt_gstno.setText("");
                    toast.setView(layouttoast);
                    toast.show();
                    return;
                }
                else {
                    Additem_fragment fragment = new Additem_fragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("Igst",str_igst);
                    bundle.putString("type","P");
                    bundle.putString("business_type","1");
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();
                }

            }
        });


        submit=(Button)view.findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_customername=edt_customername.getText().toString();
                str_gstno=txt_gstno.getText().toString();
                str_invoiceno=edt_invoiceno.getText().toString();
                int m=invoicedb.getaddproductitems().size();
                for(int i=0;i<m;i++){
                    int x= Integer.parseInt(invoicedb.getaddproductitems().get(i).gstrate);
                    if(x>temp){
                        temp=x;
                    }
                }
                if(TextUtils.isEmpty(str_customername)){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Supplier can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    invoicedb.deleteaddproductitems();
                    Itemadapter.notifyDataSetChanged();
                    return;

                }





                else  if (invoicedb.searchsuppliername(str_customername) == false) {
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Supplier not found");
                    txt_gstno.setText("");
                    edt_address.setText("");
                    edt_address1.setText("");
                    edt_city.setText("");
                    edt_state.setText("");
                    edt_pincode.setText("");
                    toast.setView(layouttoast);
                    invoicedb.deleteaddproductitems();
                    Itemadapter.notifyDataSetChanged();
                    toast.show();
                    return;
                }

                else  if(TextUtils.isEmpty(str_invoiceno)){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Invoice number can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    return;
                }



                else  if(invoicedb.getaddproductitemscount()==0){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Add Items can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    return;
                }


                else {
                    String str_invoice_date = edt_inv_date.getText().toString();


                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
                    dateFormat.applyPattern("dd/MM/yy");
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy:MM:dd");
                    dateFormat1.applyPattern("yyyy/MM/dd");

                    try {
                        Date date = dateFormat.parse(str_invoice_date);
                        str_dateval = dateFormat1.format(date);




                    }
                    catch(Exception e){

                    }


                    final String str_freight;
                    final String str_others;
                    final  String total_before_tax;
                    final String total_after_tax;
                    final String str_cgst, str_sgst, str_igstval;



                    double totbeforetax = 0, totaftertax = 0, cgst = 0, sgst = 0, igst = 0;

                    str_freight = edt_freight.getText().toString();
                    str_others = edt_others.getText().toString();
                    for (int i = 0; i < invoicedb.getaddproductitemscount(); i++) {
                        double TOT_BT = totbeforetax + Double.parseDouble(invoicedb.getaddproductitems().get(i).grossamount);
                        double TOT_AT = totaftertax + Double.parseDouble(invoicedb.getaddproductitems().get(i).tat);
                        double CGST = cgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).cgst);
                        double SGST = sgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).sgst);
                        double IGST = igst + Double.parseDouble(invoicedb.getaddproductitems().get(i).igst);
                        totbeforetax = TOT_BT;
                        totaftertax = TOT_AT;
                        cgst = CGST;
                        sgst = SGST;
                        igst = IGST;
                    }
                    if (TextUtils.isEmpty(str_freight)) {
                        freightbt=0.0;
                    } else {
                        freightbt=Double.parseDouble(str_freight);
                        double xy= temp/100;
                        double freighttax= Double.parseDouble(str_freight)*xy;
                        Double strfreightval=Double.parseDouble(str_freight)+freighttax;


                        if(str_igst.equals("N")){

                            double CGST = cgst + Double.parseDouble(String.valueOf(freighttax/2));
                            double SGST = sgst + Double.parseDouble(String.valueOf(freighttax/2));
                            cgst=CGST;
                            sgst=SGST;
                            freight=freightbt+Double.parseDouble(String.valueOf(freighttax/2))+Double.parseDouble(String.valueOf(freighttax/2));
                        }else{
                            double IGST = igst + Double.parseDouble(String.valueOf(freighttax));
                            igst=IGST;

                            freight=freightbt+Double.parseDouble(String.valueOf(freighttax));

                        }



                    }
                    if (TextUtils.isEmpty(str_others)){
                        othersbt=0.0;
                    }
                    else{
                        othersbt=Double.parseDouble(str_others);
                        double xy=temp/100;
                        double otherstax=Double.parseDouble(str_others)*xy;
                        Double strothersval=Double.parseDouble(str_others)+otherstax;

                        if(str_igst.equals("N")){

                            double CGST = cgst + Double.parseDouble(String.valueOf(otherstax/2));
                            double SGST = sgst + Double.parseDouble(String.valueOf(otherstax/2));
                            cgst=CGST;
                            sgst=SGST;
                            others=othersbt+Double.parseDouble(String.valueOf(otherstax/2))+Double.parseDouble(String.valueOf(otherstax/2));
                        }else{
                            double IGST = igst + Double.parseDouble(String.valueOf(otherstax));
                            igst=IGST;

                            others=othersbt+Double.parseDouble(String.valueOf(otherstax));

                        }
                    }

                    total_before_tax=String.valueOf(Math.round((totbeforetax+freightbt+othersbt)*100.0)/100.0);
                    total_after_tax=String.valueOf(Math.round((totaftertax+freight+others)*100.0)/100.0);
                    str_cgst=String.valueOf(Math.round(cgst*100.0)/100.0);
                    str_sgst=String.valueOf(Math.round(sgst*100.0)/100.0);
                    str_igstval=String.valueOf(Math.round(igst*100.0)/100.0);

                    url = Constants.PURCHASE_MAIN;



                    requestQueue = Volley.newRequestQueue(view.getContext());
                    progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
                    progressDialog.setMessage("Saving purchase details..");
                    progressDialog.show();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        String m=response.toString();
                                        String x=     removeUTF8BOM(m);
                                        JSONObject jsonRequest = new JSONObject(x);
                                        String statuscode = jsonRequest.getString("status_code");
                                        if (statuscode.equals("200")) {
                                            Toast toast = new Toast(view.getContext());
                                            toast.setDuration(Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                            toasttext.setText(jsonRequest.getString("status"));
                                            toast.setView(layouttoast);

                                            toast.show();
                                            progressDialog.dismiss();
                                        }
                                        else   if (statuscode.equals("400")){
                                            Toast toast = new Toast(view.getContext());
                                            toast.setDuration(Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                            toasttext.setText(jsonRequest.getString("status"));
                                            toast.setView(layouttoast);

                                            toast.show();
                                            progressDialog.dismiss();

                                        }
                                        else   if (statuscode.equals("800")){
                                            Toast toast = new Toast(view.getContext());
                                            toast.setDuration(Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                            toasttext.setText(jsonRequest.getString("status"));
                                            toast.setView(layouttoast);
                                            progressDialog.dismiss();
                                            toast.show();
                                            progressDialog.dismiss();

                                        }

                                        //    Toast.makeText(getApplicationContext(), "" +  cityPost.getCities() ,Toast.LENGTH_LONG).show();


                                    } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                                        progressDialog.dismiss();


                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    errorhandler e=new errorhandler();
                                    String msg=e.errorhandler(error);
                                    Toast toast = new Toast(view.getContext());
                                    toast.setDuration(Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                    toasttext.setText(msg);
                                    toast.setView(layouttoast);
                                    progressDialog.dismiss();
                                    toast.show();


                                }
                            }) {


                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();

                            //invoice details params
                            params.put("invoice_no",str_invoiceno);
                            params.put("user_id",prefs.getUserId());
                            params.put("invoice_date",str_dateval);

                            //supplier details params

                            params.put("supplier_name",str_customername);
                            params.put("gst_no",gst_no);
                            params.put("address",address);
                            params.put("address1",address1);
                            params.put("phone_no",phone_no);
                            params.put("mail_id",mail_id);
                            params.put("city",city);
                            params.put("state",state);
                            params.put("state_pos",state_pos);
                            params.put("supplier_igst",str_igst);
                            params.put("pincode",pincode);

                            // Product details params
                            ArrayList<Invoice_Product_model> items= invoicedb.getaddproductitems();
                            for(int i=0; i < items.size(); i++)
                            {
                                params.put("product_code["+i+"]",items.get(i).productcode);
                                params.put("product_desc["+i+"]",items.get(i).productdesc);
                                params.put("unit["+i+"]",items.get(i).unit);
                                params.put("hsn_code["+i+"]",items.get(i).hsncode);
                                params.put("gst_rate["+i+"]",items.get(i).gstrate);
                                params.put("cess["+i+"]",items.get(i).cess);
                                params.put("quantity["+i+"]",items.get(i).quantity);
                                params.put("amount["+i+"]",items.get(i).amount);
                                params.put("discount["+i+"]",items.get(i).discount);
                                params.put("discount_type["+i+"]",items.get(i).discountype);
                                params.put("totalbeforetax["+i+"]",items.get(i).grossamount);
                                params.put("cgst["+i+"]",items.get(i).cgst);
                                params.put("sgst["+i+"]",items.get(i).sgst);
                                params.put("igst["+i+"]",items.get(i).igst);
                                params.put("cgst_percent["+i+"]",items.get(i).cgst_percent);
                                params.put("sgst_percent["+i+"]",items.get(i).sgst_percent);
                                params.put("igst_percent["+i+"]",items.get(i).igst_percent);
                                params.put("tat["+i+"]",items.get(i).tat);
                            }
                            //calc params
                            params.put("total_before_tax_f",total_before_tax);
                            params.put("cgst_f",str_cgst);
                            params.put("sgst_f",str_sgst);
                            params.put("igst_f",str_igstval);
                            params.put("freight",str_freight);
                            params.put("other_charges",str_others);
                            params.put("tat_f",total_after_tax);
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                            return params;
                        }

                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);







                }
            }
        });



        btn_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int m=invoicedb.getaddproductitems().size();





                for(int i=0;i<m;i++){
                    int x= Integer.parseInt(invoicedb.getaddproductitems().get(i).gstrate);
                    if(x>temp){
                        temp=x;
                    }
                }


                String str_freight;
                String str_others;
                String total_before_tax;
                String total_after_tax;
                String str_cgst, str_sgst, str_igstval;



                double totbeforetax = 0, totaftertax = 0, cgst = 0, sgst = 0, igst = 0;

                str_freight = edt_freight.getText().toString();
                str_others = edt_others.getText().toString();
                for (int i = 0; i < invoicedb.getaddproductitemscount(); i++) {
                    double TOT_BT = totbeforetax + Double.parseDouble(invoicedb.getaddproductitems().get(i).grossamount);
                    double TOT_AT = totaftertax + Double.parseDouble(invoicedb.getaddproductitems().get(i).tat);
                    double CGST = cgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).cgst);
                    double SGST = sgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).sgst);
                    double IGST = igst + Double.parseDouble(invoicedb.getaddproductitems().get(i).igst);
                    totbeforetax = TOT_BT;
                    totaftertax = TOT_AT;
                    cgst = CGST;
                    sgst = SGST;
                    igst = IGST;
                }
                if (TextUtils.isEmpty(str_freight)) {
                    freightbt=0.00;
                } else {
                    freightbt=Double.parseDouble(str_freight);
                    double xy= temp/100;
                    double freighttax= Double.parseDouble(str_freight)*xy;



                    if(str_igst.equals("N")){

                        double CGST = cgst + Double.parseDouble(String.valueOf(freighttax/2));
                        double SGST = sgst + Double.parseDouble(String.valueOf(freighttax/2));
                        cgst=CGST;
                        sgst=SGST;
                        freight=freightbt+Double.parseDouble(String.valueOf(freighttax/2))+Double.parseDouble(String.valueOf(freighttax/2));
                    }else{
                        double IGST = igst + Double.parseDouble(String.valueOf(freighttax));
                        igst=IGST;

                        freight=freightbt+Double.parseDouble(String.valueOf(freighttax));

                    }



                }
                if (TextUtils.isEmpty(str_others)){
                    othersbt=0.00;
                }
                else{
                    othersbt=Double.parseDouble(str_others);
                    double xy=temp/100;
                    double otherstax=Double.parseDouble(str_others)*xy;


                    if(str_igst.equals("N")){

                        double CGST = cgst + Double.parseDouble(String.valueOf(otherstax/2));
                        double SGST = sgst + Double.parseDouble(String.valueOf(otherstax/2));
                        cgst=CGST;
                        sgst=SGST;
                        others=othersbt+Double.parseDouble(String.valueOf(otherstax/2))+Double.parseDouble(String.valueOf(otherstax/2));
                    }else{
                        double IGST = igst + Double.parseDouble(String.valueOf(otherstax));
                        igst=IGST;

                        others=othersbt+Double.parseDouble(String.valueOf(otherstax));

                    }
                }

                total_before_tax=String.valueOf(Math.round((totbeforetax+freightbt+othersbt)*100.0)/100.0);
                total_after_tax=String.valueOf(Math.round((totaftertax+freight+others)*100.0)/100.0);
                tv_totalaftertax.setText(total_after_tax);
                tv_totalbeforetax.setText(total_before_tax);
                if(str_igst.equals("N")){
                    double taxval=cgst+sgst;
                    str_cgst=String.valueOf(Math.round(cgst*100.0)/100.0);
                    str_sgst=String.valueOf(Math.round(sgst*100.0)/100.0);
                    tv_cgst.setText(str_cgst);
                    tv_sgst.setText(str_sgst);
                    tv_igst.setText("0.0");

                }else{
                    str_igstval=String.valueOf(Math.round(igst*100.0)/100.0);
                    tv_igst.setText(str_igstval);
                    tv_cgst.setText("0.0");
                    tv_sgst.setText("0.0");

                }


            }
        });
        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        args.putString("Invoicemindate","0");
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */

        date.setCallBack(ondate);

        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
                dateFormat.applyPattern("dd/MM/yy");
                str_date = dateFormat.format(calendar.getTime());
                edt_inv_date.setText(str_date);
             /*   SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy:MM:dd");
                dateFormat1.applyPattern("yyyy/MM/dd");
                str_dateval=dateFormat1.format(calendar.getTime());*/
            }
            catch (Exception e){

            }


        }
    };



    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.popup_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }






}

