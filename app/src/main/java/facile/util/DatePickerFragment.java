package facile.util;

/**
 * Created by pradeep on 1/5/17.
 */

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DatePickerFragment extends DialogFragment {
    OnDateSetListener ondateSet;
    public DatePickerFragment() {
    }
    public void setCallBack(OnDateSetListener ondate) {
        ondateSet = ondate;
    }
    private int year, month, day;
    private String mindate;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");

        mindate=args.getString("Invoicemindate");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DatePickerDialog d= new DatePickerDialog(getActivity(), ondateSet, year, month, day);
        d.getDatePicker().setMaxDate(new Date().getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if(mindate.equals("1")){

        }
        else {
            try {
                Date date = dateFormat.parse(mindate);
                long m = date.getTime();
                d.getDatePicker().setMinDate(m);
            } catch (Exception e) {


                String m=""+e;

            }
        }
        return d;
    }
}
