package facile.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Model.Product_model;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.errorhandler;

/**
 * Created by pradeep on 5/9/17.
 */

public class Product_adapter extends RecyclerView
        .Adapter<Product_adapter
        .DataObjectHolder> implements Filterable{
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Product_model> mDataset;
    private ArrayList<Product_model> filteredmDataset;
    private static MyClickListener myClickListener;

    ProgressDialog progressDialog;
    Context context;
    String url;
    private String tag_json_req = "json_req";
    RequestQueue requestQueue;
    public static final String UTF8_BOM = "\uFEFF";
    Invoicedatabase invoicedb;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView itemcode;
        TextView itemdesc;
        ImageView delete;


        public DataObjectHolder(View itemView) {
            super(itemView);
            itemcode = (TextView) itemView.findViewById(R.id.tv_itemcode);
            itemdesc=(TextView)itemView.findViewById(R.id.tv_itemdesc);
            delete=(ImageView)itemView.findViewById(R.id.iv_delete);


            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public Product_adapter(Context c, ArrayList<Product_model> myDataset) {
        mDataset = myDataset;
        filteredmDataset=myDataset;
        context=c;
        invoicedb=new Invoicedatabase(context);
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder,final int position) {

        holder.itemcode.setText(filteredmDataset.get(position).productcode);
        holder.itemdesc.setText(filteredmDataset.get(position).productdesc);





        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url= Constants.DELETE_PRODUCT;
                requestQueue = Volley.newRequestQueue(v.getContext());
                progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
                progressDialog.setMessage("Deleting Item...");
                progressDialog.show();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {

                                    String m=removeUTF8BOM(response);
                                    JSONObject jsonRequest = new JSONObject(m);
                                    String statuscode = jsonRequest.getString("status_code");
                                    if (statuscode.equals("200")) {
                                        invoicedb.deleteProductdetails( filteredmDataset.get(position).productcode);
                                        deleteItem(holder.getAdapterPosition());
                                        Toast.makeText(context, "success", Toast.LENGTH_LONG).show();

                                    }

                                    progressDialog.dismiss();


                                } catch (Exception e) {

                                    progressDialog.dismiss();


                                }
                            }
                        },


                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                                errorhandler e=new errorhandler();
                                String msg=e.errorhandler(error);
                                Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

                            }
                        }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", mDataset.get(position).user_id);
                        params.put("product_code", filteredmDataset.get(position).productcode);


                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("content-type", "application/x-www-form-urlencoded");
                        return params;
                    }
                };

                requestQueue.add(stringRequest);














            }
        });




    }

    public void addItem(Product_model dataObj, int index) {
        filteredmDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        String fm= filteredmDataset.get(index).productcode;
        filteredmDataset.remove(index);
        notifyItemRemoved(index);
        notifyItemRangeChanged(index, filteredmDataset.size());

        notifyDataSetChanged();

        int m1=mDataset.size();
        int m2=filteredmDataset.size();



        for(int i=0;i<m1;i++) {

            String hs=mDataset.get(i).productcode;

            if (hs.equals(fm)) {
                mDataset.remove(i);
                filteredmDataset.remove(hs);
                notifyDataSetChanged();

            }
        }


    }

    @Override
    public int getItemCount() {
        return filteredmDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    filteredmDataset = mDataset;
                } else {

                    ArrayList<Product_model> filteredList = new ArrayList<>();

                    for (Product_model product : mDataset) {

                        if (product.productcode.toLowerCase().contains(charString)||product.productcode.toUpperCase().contains(charString)) {

                            filteredList.add(product);
                        }
                    }

                    filteredmDataset = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredmDataset;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredmDataset = (ArrayList<Product_model>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }
   /* @Override
    public long getItemId(int position) {

        int itemID;

        // orig will be null only if we haven't filtered yet:
        if (mDataset == null)
        {
            itemID = position;
        }
        else
        {
            itemID = mDataset.indexOf(filteredmDataset.get(position));
        }
        return itemID;
    }
*/



}