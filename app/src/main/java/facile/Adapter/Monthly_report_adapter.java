package facile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import facile.invoice.R;

/**
 * Created by pradeep on 21/03/18.
 */

public class Monthly_report_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private ArrayList<HashMap<String, String>> mDataset;
    private static final String INVOICE_NO = "invoiveno";
    private static final String CUSTOMER_NAME = "customer_name";
    private static final String TOTAL = "tat";
    private static final String CN_NO = "credit_note_no";
    private static final String DN_NO = "debit_note_no";

    String strtype;

    Context context;

    public Monthly_report_adapter(Context c,ArrayList<HashMap<String, String>> myDataset,String type) {
        strtype=type;
        mDataset = myDataset;
        context=c;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_report_header, parent, false);
            return  new VHHeader(v);
        }
        else if(viewType == TYPE_ITEM)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_report_row, parent, false);
            return new VHItem(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof VHHeader)
        {
            VHHeader vhHeader = (VHHeader)holder;

        }
        else if(holder instanceof VHItem)
        {

            HashMap<String, String> map = mDataset.get(position-1);
            VHItem VHitem = (VHItem)holder;
            if(strtype.equals("S")){

                VHitem.invoiceno.setText(map.get(INVOICE_NO));
            }
            else if(strtype.equals("P")){
                VHitem.invoiceno.setText(map.get(INVOICE_NO));
            }

           else if(strtype.equals("CR")){
                VHitem.invoiceno.setText(map.get(CN_NO));

            }
            else if(strtype.equals("DR")){
                VHitem.invoiceno.setText(map.get(DN_NO));

            }

            VHitem.customername.setText(map.get(CUSTOMER_NAME));
            VHitem.total.setText(map.get(TOTAL));

        }
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        if(position==0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }







    //increasing getItemcount to 1. This will be the row of header.
    @Override
    public int getItemCount() {


        return (mDataset.size())+1;
    }

    class VHHeader extends RecyclerView.ViewHolder{

        TextView itemdesch,tvh_customername;

        public VHHeader(View itemView) {
            super(itemView);
            itemdesch = (TextView) itemView.findViewById(R.id.tvh_invoiceno);
            tvh_customername = (TextView) itemView.findViewById(R.id.tvh_customername);



            if(strtype.equals("S")){
                tvh_customername.setText("Customer Name");
            }
            else if(strtype.equals("P")){
                tvh_customername.setText("Supplier Name");
            }

    if(strtype.equals("CR")){
        itemdesch.setText("CREDIT NOTE NO");
            }
            else if(strtype.equals("DR")){
        itemdesch.setText("DEBIT NOTE NO");
            }
       }
    }

    class VHItem extends RecyclerView.ViewHolder{
        TextView invoiceno;
        TextView customername;
        TextView total;

        public VHItem(View itemView) {
            super(itemView);
            invoiceno = (TextView) itemView.findViewById(R.id.tv_invoiceno);
            customername=(TextView)itemView.findViewById(R.id.tv_customername);
            total=(TextView) itemView.findViewById(R.id.tv_total);

        }
    }
}
