package facile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import facile.invoice.R;

/**
 * Created by pradeep on 19/3/18.
 */

public class YearlistArrayAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final Context mContext;

    private final int mResource;
    ArrayList<String> items;

    public YearlistArrayAdapter(Context context, int resource,
                                 ArrayList<String> objects) {


        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView year = (TextView) view.findViewById(R.id.tv_year);

        year.setText(items.get(position).toString());

        year.setTextSize(12);
        return view;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}


