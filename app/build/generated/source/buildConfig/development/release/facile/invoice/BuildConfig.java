/**
 * Automatically generated file. DO NOT MODIFY
 */
package facile.invoice;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "fivecolors.invoice";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "development";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
