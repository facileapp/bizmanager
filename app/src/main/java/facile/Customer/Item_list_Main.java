package facile.Customer;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import facile.Adapter.Product_adapter;
import facile.Master.Item_creation;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;

/**
 * Created by pradeep on 12/2/18.
 */

public class Item_list_Main extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Invoicedatabase invoicedb;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    FloatingActionButton fab;
    EditText edt_search;

    private Customer_Main.OnFragmentInteractionListener mListener;

    public Item_list_Main() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Item_list_Main newInstance(String param1, String param2) {
        Item_list_Main fragment = new Item_list_Main();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_main, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_add_items));
        invoicedb=new Invoicedatabase(getContext());

        fab = (FloatingActionButton)view. findViewById(R.id.fab);
        edt_search=(EditText)view.findViewById(R.id.edt_search);
        mRecyclerView = (RecyclerView)view. findViewById(R.id.rv_productlist);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new Product_adapter(getContext(),invoicedb.getproductdetails());
        mRecyclerView.setAdapter(mAdapter);
        ((Product_adapter) mAdapter).setOnItemClickListener(new Product_adapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Item_creation fragment = new Item_creation();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("ITEM_CODE", invoicedb.getproductdetails().get(position).productcode);
                bundle.putString("PROD_DESC", invoicedb.getproductdetails().get(position).productdesc);
                bundle.putString("ITEM_RATE", invoicedb.getproductdetails().get(position).productrate);
                bundle.putString("GST_RATE", invoicedb.getproductdetails().get(position).gstrate);
                bundle.putString("HSN_CODE", invoicedb.getproductdetails().get(position).hsncode);
                bundle.putString("CESS", invoicedb.getproductdetails().get(position).cess);
                bundle.putString("UNIT_POS", invoicedb.getproductdetails().get(position).unit_pos);

                fragment.setArguments(bundle);
                fragmentTransaction.commit();


            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Item_creation fragment = new Item_creation();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentManager.popBackStack("Item_list_Main", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentTransaction.commit();

            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                ((Product_adapter) mAdapter).getFilter().filter(s.toString());

            }
        });
        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

