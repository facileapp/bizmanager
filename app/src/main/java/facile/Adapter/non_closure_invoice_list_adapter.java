package facile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.invoice_list_model;
import facile.invoice.R;

/**
 * Created by pradeep on 31/03/18.
 */

public class non_closure_invoice_list_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private ArrayList<invoice_list_model> mDataset;
    private static final String INVOICE_NO = "invoiveno";
    private static final String CUSTOMER_NAME = "customer_name";
    private static final String TOTAL = "tat";
    private static non_closure_invoice_list_adapter.MyClickListener myClickListener;


    Context context;

    public non_closure_invoice_list_adapter(Context c,ArrayList<invoice_list_model> myDataset) {
        mDataset = myDataset;
        context=c;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_report_header, parent, false);
            return  new VHHeader(v);
        }
        else if(viewType == TYPE_ITEM)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_report_row, parent, false);
            return new VHItem(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof VHHeader)
        {
            VHHeader vhHeader = (VHHeader)holder;

        }
        else if(holder instanceof VHItem)
        {


            VHItem VHitem = (VHItem)holder;

            VHitem.invoiceno.setText(mDataset.get(position-1).invoice_no);
            VHitem.customername.setText(mDataset.get(position-1).customer_name);
            VHitem.total.setText(mDataset.get(position-1).tat);


        }
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        if(position==0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }







    //increasing getItemcount to 1. This will be the row of header.
    @Override
    public int getItemCount() {


        return (mDataset.size())+1;
    }



    class VHHeader extends RecyclerView.ViewHolder{

        TextView itemdesch;

        public VHHeader(View itemView) {
            super(itemView);
            itemdesch = (TextView) itemView.findViewById(R.id.tvh_invoiceno);


        }
    }

    class VHItem extends RecyclerView.ViewHolder implements View
            .OnClickListener{
        TextView invoiceno;
        TextView customername;
        TextView total;

        public VHItem(View itemView) {
            super(itemView);
            invoiceno = (TextView) itemView.findViewById(R.id.tv_invoiceno);
            customername=(TextView)itemView.findViewById(R.id.tv_customername);
            total=(TextView) itemView.findViewById(R.id.tv_total);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(non_closure_invoice_list_adapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }
    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
