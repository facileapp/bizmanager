package facile.Settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import facile.invoice.R;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 14/2/18.
 */

public class Change_password extends AppCompatActivity {

    EditText edt_oldpwd,edt_newpwd;
    String url;
    RequestQueue requestQueue;
    TextView toasttext;
    View layout;
    Button btn_save;
    String str_oldpwd,str_newpwd;
    ProgressDialog progressDialog;
    public static final String UTF8_BOM = "\uFEFF";
    Context con;
    prefManager prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        edt_oldpwd=(EditText)findViewById(R.id.edt_oldpwd);
        edt_newpwd=(EditText)findViewById(R.id.edt_newpwd);
        btn_save=(Button)findViewById(R.id.btn_save);
        con=this;
        prefs=new prefManager(con);
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        layout = li.inflate(R.layout.customtoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layout.findViewById(R.id.custom_toast_message);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_oldpwd=edt_oldpwd.getText().toString();
                str_newpwd=edt_newpwd.getText().toString();




                if(TextUtils.isEmpty(str_oldpwd))
                {

                    edt_oldpwd.requestFocus();
                    edt_oldpwd.setError("Please enter your old pwd");
                    return;
                }

                else  if(TextUtils.isEmpty(str_newpwd))
                {
                    edt_newpwd.requestFocus();
                    edt_newpwd.setError("Please enter your new pwd");

                    return;
                }
                else{



                    url = Constants.CHANGE_PASSWORD;
                    requestQueue = Volley.newRequestQueue(getApplicationContext());
                    progressDialog = new ProgressDialog(con, R.style.MyAlertDialogStyle);
                    progressDialog.setMessage("Changing password..");
                    progressDialog.show();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {

                                        String m=response.toString();
                                        String x=         removeUTF8BOM(m);

                                        JSONObject jsonRequest = new JSONObject(x);
                                        String statuscode = jsonRequest.getString("status_code");
                                        if (statuscode.equals("200")) {
                                            Toast toast = new Toast(getApplicationContext());
                                            toast.setDuration(Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                            toasttext.setText(jsonRequest.getString("status"));
                                            toast.setView(layout);
                                            progressDialog.dismiss();
                                            toast.show();

                                        }

                                        if (statuscode.equals("600")) {
                                            Toast toast = new Toast(getApplicationContext());
                                            toast.setDuration(Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                            toasttext.setText(jsonRequest.getString("status"));
                                            toast.setView(layout);
                                            progressDialog.dismiss();
                                            toast.show();


                                        }
                                        else{

                                            Toast toast = new Toast(getApplicationContext());
                                            toast.setDuration(Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                            toasttext.setText(jsonRequest.getString("status"));
                                            toast.setView(layout);
                                            progressDialog.dismiss();
                                            toast.show();

                                        }


                                    } catch (Exception e) {
                                        progressDialog.dismiss();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    errorhandler e=new errorhandler();
                                    String msg=e.errorhandler(error);
                                    Toast toast = new Toast(getApplicationContext());
                                    toast.setDuration(Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                    toasttext.setText(msg);
                                    toast.setView(layout);
                                    progressDialog.dismiss();
                                    toast.show();


                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id",prefs.getUserId());
                            params.put("old_pwd",str_oldpwd);
                            params.put("new_pwd",str_newpwd);

                            return params;
                        }
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);
                }
            }
        });

    }

    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

}