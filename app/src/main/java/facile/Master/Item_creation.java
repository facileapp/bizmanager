package facile.Master;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.UnitslistArrayadapter;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 12/2/18.
 */

public class Item_creation extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText edt_itemcode,edt_proddesc,edt_itemrate,edt_hsncode,edt_gstrate,edt_cess;
    String str_itemcode,str_proddesc,str_unit,str_hsncode,str_gstrate,str_cess,str_unitpos,str_insertorupdate,str_itemrate,str_prodid;
    String url;
    public static final String UTF8_BOM = "\uFEFF";
    RequestQueue requestQueue;
    private customer_creation.OnFragmentInteractionListener mListener;
    ProgressDialog progressDialog;
    TextView toasttext;
    View layouttoast;
    Button btn_add;
    Spinner spn_units;
    LinearLayout ll_productmain;
    ImageView iv_edit;
    Invoicedatabase invoicedb;
    prefManager prefs;
    ArrayList<HashMap<String, String>> productdetailHashMap;

    public Item_creation() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Item_creation newInstance(String param1, String param2) {
        Item_creation fragment = new Item_creation();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.create_customer_items, container,false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_add_items));
        edt_itemcode=(EditText)view.findViewById(R.id.edt_itemcode);
        edt_proddesc=(EditText)view.findViewById(R.id.edt_proddesc);
        spn_units=(Spinner)view.findViewById(R.id.spn_units);
        edt_itemrate=(EditText)view.findViewById(R.id.edt_itemrate);
        edt_hsncode=(EditText)view.findViewById(R.id.edt_hsncode);
        edt_gstrate=(EditText)view.findViewById(R.id.edt_gstrate);
        edt_cess=(EditText)view.findViewById(R.id.edt_cess);
        btn_add=(Button)view.findViewById(R.id.btn_save);
        iv_edit=(ImageView)view.findViewById(R.id.iv_edit);
        ll_productmain=(LinearLayout)view.findViewById(R.id.ll_productmain);
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        prefs=new prefManager(getContext());
        invoicedb=new Invoicedatabase(getContext());
        final UnitslistArrayadapter adapter = new UnitslistArrayadapter(getActivity(),
                R.layout.unit_list_row, invoicedb.getunits());
        spn_units.setAdapter(adapter);
        Bundle bundle = this.getArguments();



        if (bundle != null) {
            str_itemcode = bundle.getString("ITEM_CODE", "");
            str_proddesc= bundle.getString("PROD_DESC", "");
            str_itemrate=bundle.getString("ITEM_RATE", "");
            str_gstrate=bundle.getString("GST_RATE", "");
            str_hsncode=bundle.getString("HSN_CODE", "");
            str_cess=bundle.getString("CESS", "");
            str_unitpos=bundle.getString("UNIT_POS", "");
            str_prodid=bundle.getString("PROD_ID", "");
            edt_itemcode.setText(str_itemcode);
            edt_proddesc.setText(str_proddesc);
            edt_itemrate.setText(str_itemrate);
            edt_hsncode.setText(str_hsncode);
            edt_gstrate.setText(str_gstrate);
            edt_cess.setText(str_cess);
            spn_units.setSelection(Integer.parseInt(str_unitpos)-1);
            edt_itemcode.setEnabled(false);
            edt_proddesc.setEnabled(false);
            edt_itemrate.setEnabled(false);
            edt_hsncode.setEnabled(false);
            edt_gstrate.setEnabled(false);
            edt_cess.setEnabled(false);
            spn_units.setEnabled(false);
            str_insertorupdate="U";
            ll_productmain.setAlpha(0.5f);
            btn_add.setVisibility(View.GONE);



        }
        else{
            ll_productmain.setAlpha(1);
            str_insertorupdate="I";
            edt_itemcode.requestFocus();
           iv_edit.setVisibility(View.GONE);

        }


        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_itemcode.setEnabled(false);
                edt_itemrate.setEnabled(true);
                edt_proddesc.setEnabled(false);
                edt_hsncode.setEnabled(true);
                edt_gstrate.setEnabled(true);
                edt_cess.setEnabled(true);
                spn_units.setEnabled(true);
                ll_productmain.setAlpha(1);
                btn_add.setVisibility(View.VISIBLE);
                btn_add.setText("UPDATE");

            }
        });


        edt_gstrate.setFilters(new InputFilter[] {
                new DigitsKeyListener(Boolean.FALSE, Boolean.TRUE) {
                    int beforeDecimal = 2, afterDecimal = 2;

                    @Override
                    public CharSequence filter(CharSequence source, int start, int end,
                                               Spanned dest, int dstart, int dend) {
                        String temp = edt_gstrate.getText() + source.toString();

                        if (temp.equals(".")) {
                            return "0.";
                        }
                        else if (temp.toString().indexOf(".") == -1) {
                            // no decimal point placed yet
                            if (temp.length() > beforeDecimal) {
                                return "";
                            }
                        } else {
                            temp = temp.substring(temp.indexOf(".") + 1);
                            if (temp.length() > afterDecimal) {
                                return "";
                            }
                        }

                        return super.filter(source, start, end, dest, dstart, dend);
                    }
                }
        });



        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addproduct(view);
            }
        });



        spn_units.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                str_unit=((TextView) v.findViewById(R.id.tv_unitcode)).getText().toString();
                str_unitpos=String.valueOf(position+1);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

     /*   ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
             /*   MobileAds.initialize(getActivity(), "ca-app-pub-2213723827168530~3447777035");*/
        // Setting ViewPager for each Tabs

        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public  void addproduct(final View view){

        url = Constants.ADD_ITEM;

        str_itemcode=edt_itemcode.getText().toString();
        str_proddesc=edt_proddesc.getText().toString();



        str_gstrate=edt_gstrate.getText().toString();
        str_hsncode=edt_hsncode.getText().toString();
        str_itemrate=edt_itemrate.getText().toString();
        str_cess=edt_cess.getText().toString();

        if(TextUtils.isEmpty(str_itemcode))
        {
            edt_itemcode.requestFocus();
            edt_itemcode.setError("Item code can't be empty");
            return;
        }
        else    if(TextUtils.isEmpty(str_proddesc))
        {
            edt_proddesc.requestFocus();
            edt_proddesc.setError("Product description can't be empty");
            return;
        }
        else   if(TextUtils.isEmpty(str_hsncode))
        {
            edt_hsncode.requestFocus();
            edt_hsncode.setError("Hsn code can't be empty");
            return;
        }
        else  if(TextUtils.isEmpty(str_gstrate))
        {
            edt_gstrate.requestFocus();
            edt_gstrate.setError("Gst rate can't be empty");
            return;
        }

        requestQueue = Volley.newRequestQueue(view.getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Adding Items...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String m=response.toString();
                            String x=     removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                            String status = jsonRequest.getString("status");
                            if (statuscode.equals("200")) {

                                productdetailHashMap = new ArrayList<HashMap<String,String>>();
                                HashMap<String, String> userhashmap = new HashMap<String, String>();
                                userhashmap.put("product_code", str_itemcode);
                                userhashmap.put("product_desc", str_proddesc);
                                userhashmap.put("product_rate", str_itemrate);

                                userhashmap.put("user_id", prefs.getUserId());
                                userhashmap.put("product_unit",str_unit);
                                userhashmap.put("Hsn", str_hsncode);
                                userhashmap.put("prod_gst_rate",str_gstrate);
                                userhashmap.put("cess", str_cess);
                                userhashmap.put("unit_pos", str_unitpos);
                                productdetailHashMap.add(userhashmap);
                                if(str_insertorupdate.equals("I")){
                                    invoicedb.insertProductdetails(productdetailHashMap);
                                }
                               else{
                                    invoicedb.updateProductdetails(productdetailHashMap);
                                }

                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                if(str_insertorupdate.equals("I")) {
                                    toasttext.setText("Item Added Successfully");
                                    edt_itemcode.setText("");
                                    edt_proddesc.setText("");
                                    edt_itemrate.setText("");
                                    edt_hsncode.setText("");
                                    edt_gstrate.setText("");
                                    edt_cess.setText("");
                                    spn_units.setSelection(0);

                                }
                                else{
                                    toasttext.setText("Item Updated Successfully");
                                }
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();

                                progressDialog.dismiss();

                            }

                            else    if (statuscode.equals("600")) {

                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(status);
                                toast.setView(layouttoast);

                                toast.show();
                                progressDialog.dismiss();

                            }

                            else    if (statuscode.equals("800")) {

                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(status);
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();
                                progressDialog.dismiss();

                            }
                            else   if (statuscode.equals("400")){
                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(jsonRequest.getString("status"));
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();

                            }

                            //    Toast.makeText(getApplicationContext(), "" +  cityPost.getCities() ,Toast.LENGTH_LONG).show();


                        } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                            progressDialog.dismiss();


                        }
                    }
                },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(view.getContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layouttoast);
                        progressDialog.dismiss();
                        toast.show();


                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_code",str_itemcode);
                    params.put("product_desc", str_proddesc);
                if(str_itemrate==null){
                    str_itemrate="0";
                }
                params.put("product_rate", str_itemrate);
                params.put("product_unit",str_unit);
                params.put("hsn",str_hsncode);
                params.put("gst_rate",str_gstrate);
                params.put("user_id",prefs.getUserId());
                params.put("cess",str_cess);
                params.put("unit_pos",str_unitpos);
                params.put("insertorupdate",str_insertorupdate);




                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }


    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

}

