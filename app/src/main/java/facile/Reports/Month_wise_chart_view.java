package facile.Reports;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.YearlistArrayAdapter;
import facile.invoice.R;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Month_wise_chart_view.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Month_wise_chart_view#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Month_wise_chart_view extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RequestQueue requestQueue;
    public static final String UTF8_BOM = "\uFEFF";
    ProgressDialog progressDialog;
    String url;
    prefManager prefs;
    View layouttoast;
    TextView toasttext;
    String str_closuredate;
    ArrayList<HashMap<String, String>> reporthashmap;
    Spinner spn_year;
    String str_year,str_sorp;
    BarChart chart;

    private Month_wise_report.OnFragmentInteractionListener mListener;


    public Month_wise_chart_view() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Month_wise_chart_view.
     */
    // TODO: Rename and change types and number of parameters
    public static Month_wise_chart_view newInstance(String param1, String param2) {
        Month_wise_chart_view fragment = new Month_wise_chart_view();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.chartview, container, false);

        spn_year=(Spinner)view.findViewById(R.id.spn_year);
        prefs = new prefManager(getContext());
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
         chart = (BarChart)view. findViewById(R.id.chart);
        Bundle bundle = this.getArguments();
        str_sorp = bundle.getString("SORP", "");

        final ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);

        for (int i = thisYear-5; i <= thisYear; i++) {
            years.add(Integer.toString(i));


            str_year=String.valueOf(thisYear);
        }

        final YearlistArrayAdapter adapter = new YearlistArrayAdapter(getActivity(),
                R.layout.year_row, years);
        spn_year.setAdapter(adapter);
        spn_year.setSelection(5);


        spn_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                str_year=years.get(position);
                spn_year.setSelection(position);
                getchart(view);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });





        return view;
    }



    public void getchart(final View view){

if(str_sorp.equals("S")) {
    url = Constants.MONTH_WISE_CHART;
}
else if(str_sorp.equals("P")) {
    url = Constants.MONTH_WISE_PURCHASE_CHART;

}
        requestQueue = Volley.newRequestQueue(view.getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Generating report...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<BarEntry> entries = new ArrayList<>();

                            ArrayList<String> labels = new ArrayList<>();
                            String m=response.toString();
                            String x=     removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                            String   val = jsonRequest.getString("status");
                            if (statuscode.equals("200")) {
                                JSONObject json = new JSONObject(jsonRequest.toString());
                                if (json.isNull("report")) {
                                    progressDialog.dismiss();

                                } else {
                                    JSONArray report_list = jsonRequest.getJSONArray("report");
                                    reporthashmap = new ArrayList<HashMap<String, String>>();

                                    for (int iStatusList = 0; iStatusList < report_list.length(); iStatusList++) {
                                        JSONObject report = report_list.getJSONObject(iStatusList);
                                        HashMap<String, String> userhashmap = new HashMap<String, String>();

                                        if(str_sorp.equals("S")) {
                                            userhashmap.put("SalesMonth", report.getString("SalesMonth"));
                                        }else  if(str_sorp.equals("P")){
                                            userhashmap.put("PurchaseMonth", report.getString("PurchaseMonth"));
                                        }
                                        userhashmap.put("Total", report.getString("Total"));

                                        entries.add(new BarEntry(Integer.parseInt(
                                                report.getString("Total")), iStatusList));

                                        if(str_sorp.equals("S")) {

                                            labels.add(report.getString("SalesMonth"));
                                        }
                                        else  if(str_sorp.equals("P")){
                                            labels.add(report.getString("PurchaseMonth"));
                                        }

                                    }

                                }

                                BarDataSet dataset = new BarDataSet(entries, "Amount");
                                BarData data = new BarData(labels,dataset);
                                chart.setData(data);
                                if(str_sorp.equals("S")) {
                                    chart.setDescription("Sales Chart");
                                }
                               else if(str_sorp.equals("P")) {
                                    chart.setDescription("Purchase Chart");
                                }
                                chart.animateXY(2000, 2000);
                                chart.invalidate();
                                progressDialog.dismiss();


                            }
                            else   if (statuscode.equals("600")){
                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(val);
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();

                            }

                        } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                            progressDialog.dismiss();


                        }
                    }
                },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(view.getContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layouttoast);
                        progressDialog.dismiss();
                        toast.show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",prefs.getUserId());

                params.put("year",str_year);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }







    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }
 /*   @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.popup_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_csv:

                progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
                progressDialog.setMessage("Generating CSV...");
                progressDialog.show();

                File mypath;

                File filepath = Environment.getExternalStorageDirectory();

                // Create a new folder in SD Card
                File dir = new File(filepath.getAbsolutePath()
                        + "/BizManager/Reports/Monthly_report/Invoice/");
                dir.mkdirs();

                mypath = new File(dir, str_month+"_"+str_year+".csv");

                try {

                    FileWriter fw = new FileWriter(mypath);

                    fw.append("invoiveno");
                    fw.append(',');

                    fw.append("invoice_date");
                    fw.append(',');

                    fw.append("customer_name");
                    fw.append(',');

                    fw.append("gst_no");
                    fw.append(',');

                    fw.append("hsn_code");
                    fw.append(',');

                    fw.append("gst_rate");
                    fw.append(',');


                    fw.append("total_before_tax");
                    fw.append(',');

                    fw.append("igst");
                    fw.append(',');

                    fw.append("cgst");
                    fw.append(',');

                    fw.append("sgst");
                    fw.append(',');

                    fw.append("tat");
                    fw.append(',');

                    fw.append('\n');


                    for (int i = 0; i < reporthashmap.size(); i++) {
                        fw.append(reporthashmap.get(i).get("invoiveno").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("invoice_date").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("customer_name").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("gst_no").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("hsn_code").toString());
                        fw.append(',');

                        fw.append(reporthashmap.get(i).get("gst_rate").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("total_before_tax").toString());
                        fw.append(',');



                        fw.append(reporthashmap.get(i).get("igst").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("cgst").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("sgst").toString());
                        fw.append(',');
                        fw.append(reporthashmap.get(i).get("tat").toString());
                        fw.append(',');


                        fw.append('\n');

                    }
                    // fw.flush();
                    fw.close();
                    progressDialog.dismiss();

                } catch (Exception e) {
                    progressDialog.dismiss();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);


        }
    }*/
}


