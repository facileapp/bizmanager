package facile.Signin;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Settings.profile;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 11/2/18.
 */

public class SigninActivity extends AppCompatActivity {

    EditText username,password;
    String str_username,str_pwd;
    RequestQueue requestQueue;
    Context con;
    ProgressDialog progressDialog;
    TextView toasttext;
    String url;
    View layout;
    public static final String UTF8_BOM = "\uFEFF";
    Button signin;
    ArrayList<HashMap<String, String>> userdetailHashMap;
    ArrayList<HashMap<String, String>> customerdetailHashMap;
    ArrayList<HashMap<String, String>> supplierdetailHashMap;
    ArrayList<HashMap<String, String>> productdetailHashMap;
    ArrayList<HashMap<String, String>> bankdetailHashMap;
    Invoicedatabase invoicedb;
    prefManager prefs;
    private static final int REQUEST_PERMISSIONS = 20;
    ImageView iv_watsapp,iv_phone,iv_email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        con=this;
        invoicedb=new Invoicedatabase(this);
        prefs = new prefManager(con);
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        layout = li.inflate(R.layout.customtoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layout.findViewById(R.id.custom_toast_message);
        username=(EditText)findViewById(R.id.edt_username);
        password=(EditText)findViewById(R.id.passwordEditText);
        iv_watsapp=(ImageView) findViewById(R.id.iv_watsapp);
        iv_phone=(ImageView) findViewById(R.id.iv_phone);
        iv_email=(ImageView) findViewById(R.id.iv_email);
        signin=(Button)findViewById(R.id.signinBtn);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(con, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {




                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE},
                        REQUEST_PERMISSIONS);

            } else {
                signin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        signin();
                    }
                });

            }
        }
        else{
            signin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signin();
                }
            });
        }


        iv_watsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String formattedNumber = "919787335780";
                try{
                    Intent sendIntent =new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT,"");
                    sendIntent.putExtra("jid", formattedNumber +"@s.whatsapp.net");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);
                }
                catch(Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Error/n"+ e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        });


  /*      iv_phone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:0377778888"));


                startActivity(callIntent);
            }
        });
*/
        iv_email.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Customer Support");

                emailIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"facilebizmanager@gmail.com"});

                emailIntent.setType("message/rfc822");

                startActivity(Intent.createChooser(emailIntent, "Choose an Email client :"));


                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));


                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(SigninActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }


            }
        });







   }




    public  void signin() {

        str_username = username.getText().toString();
        str_pwd = password.getText().toString();

        if(str_username.length()==0){

            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toasttext.setText("User Name can't be empty");
            toast.setView(layout);

            toast.show();

        }
        else if(str_pwd.length()==0){

            Toast toast = new Toast(getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toasttext.setText("Password can't be empty");
            toast.setView(layout);

            toast.show();

        }
        else {
            url = Constants.SIGNIN_URL;
            requestQueue = Volley.newRequestQueue(getApplicationContext());
            progressDialog = new ProgressDialog(con, R.style.MyAlertDialogStyle);
            progressDialog.setMessage("Signing in..");
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                String m = fixEncoding(response);
                                String x = removeUTF8BOM(m);

                                JSONObject jsonRequest = new JSONObject(x);
                                String statuscode = jsonRequest.getString("status_code");
                                String status = jsonRequest.getString("status");
                                if (statuscode.equals("400")) {
                                    Toast toast = new Toast(getApplicationContext());
                                    toast.setDuration(Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                    toasttext.setText(status);
                                    toast.setView(layout);
                                    progressDialog.dismiss();
                                    toast.show();

                                }

                                if (statuscode.equals("200")) {

                                    JSONObject json = new JSONObject(jsonRequest.toString());
                                    prefs.setUserId(jsonRequest.getString("user_id"));

                                    invoicedb.delete();
                                    if (json.isNull("user_list")) {
                                        prefs.setLogInType("0");

                                    } else {
                                        JSONArray user_list = jsonRequest.getJSONArray("user_list");
                                        prefs.setLogInType("1");
                                        userdetailHashMap = new ArrayList<HashMap<String, String>>();
                                        invoicedb.delete();

                                        for (int iStatusList = 0; iStatusList < user_list.length(); iStatusList++) {
                                            JSONObject user = user_list.getJSONObject(iStatusList);
                                            HashMap<String, String> userhashmap = new HashMap<String, String>();
                                            userhashmap.put("id", user.getString("id"));
                                            userhashmap.put("address", user.getString("Address"));
                                            userhashmap.put("address1", user.getString("Address1"));
                                            userhashmap.put("phone_no", user.getString("Phone_no"));
                                            userhashmap.put("mail_id", user.getString("mail_id"));
                                            userhashmap.put("city", user.getString("city"));
                                            userhashmap.put("state", user.getString("state"));
                                            userhashmap.put("pincode", user.getString("pincode"));
                                            userhashmap.put("dealer_type", user.getString("dealer_type"));
                                            userhashmap.put("dealer_nature", user.getString("dealer_nature"));
                                            prefs.setBusinessNature(user.getString("dealer_nature"));
                                            userhashmap.put("gst_no", user.getString("user_gst_no"));
                                           /* userhashmap.put("bank_ac_no", user.getString("bank_ac_no"));
                                            userhashmap.put("bank_name", user.getString("bank_ac_name"));
                                            userhashmap.put("bank_branch", user.getString("bank_branch"));
                                            userhashmap.put("bank_ifsc", user.getString("ifsc_code"));*/
                                            userhashmap.put("user_id", user.getString("user_id"));
                                            userhashmap.put("user_trade_name", user.getString("user_trade_name"));
                                            prefs.setCompanyName(user.getString("user_trade_name"));


                                         String logo=user.getString("logo");

                                            if(user.isNull("logo")){
                                                prefs.setLogo("");


                                            }else {
                                                prefs.setLogo(logo);
                                            }
                                            userhashmap.put("spn_state_position", user.getString("spn_state_position"));
                                            prefs.setState(user.getString("spn_state_position"));
                                            userhashmap.put("invoice_start_no", user.getString("invoice_start_no"));
                                            userhashmap.put("invoice_no_prefix", user.getString("invoice_no_prefix"));
                                            userhashmap.put("invoice_no_suffix", user.getString("invoice_no_suffix"));
                                            userhashmap.put("terms_of_use", user.getString("terms_of_use"));
                                            userdetailHashMap.add(userhashmap);
                                            invoicedb.insertUserdetails(userdetailHashMap);
                                        }
                                    }


                                    if (json.isNull("customer_detail")) {

                                    } else {
                                        JSONArray customer_list = jsonRequest.getJSONArray("customer_detail");
                                        customerdetailHashMap = new ArrayList<HashMap<String, String>>();
                                        for (int iStatusList = 0; iStatusList < customer_list.length(); iStatusList++) {
                                            JSONObject customer = customer_list.getJSONObject(iStatusList);
                                            HashMap<String, String> userhashmap = new HashMap<String, String>();
                                            userhashmap.put("id", customer.getString("id"));
                                            userhashmap.put("customer_name", customer.getString("customer_name"));
                                            userhashmap.put("gst_no", customer.getString("Gst_no"));
                                            userhashmap.put("user_id", customer.getString("user_id"));
                                            userhashmap.put("address", customer.getString("address"));
                                            userhashmap.put("address1", customer.getString("address1"));
                                            userhashmap.put("city", customer.getString("city"));
                                            userhashmap.put("state", customer.getString("state"));
                                            userhashmap.put("pincode", customer.getString("pincode"));
                                            userhashmap.put("phone_no", customer.getString("phone_no"));
                                            userhashmap.put("mail_id", customer.getString("mail_id"));
                                            userhashmap.put("spn_state_position", customer.getString("spn_state_position"));
                                            userhashmap.put("opening_balance", customer.getString("opening_balance"));
                                            userhashmap.put("opening_balance_crordr", customer.getString("openingbal_crordr"));
                                            userhashmap.put("shipping_id", customer.getString("shipping_id"));
                                            userhashmap.put("cust_id", customer.getString("cust_id"));
                                            userhashmap.put("s_address_line_1", customer.getString("s_address_line_1"));
                                            userhashmap.put("s_address_line_2", customer.getString("s_address_line_2"));
                                            userhashmap.put("s_city", customer.getString("s_city"));
                                            userhashmap.put("s_pincode", customer.getString("s_pincode"));
                                            userhashmap.put("s_state", customer.getString("s_state"));
                                            userhashmap.put("s_country", customer.getString("s_country"));
                                            customerdetailHashMap.add(userhashmap);

                                        }
                                        invoicedb.insertCustomerdetails(customerdetailHashMap);
                                    }


                                    if (json.isNull("Bank_details")) {

                                    }
                               /* if(product_list.length()==0){


                                }*/
                                    else {
                                        JSONArray product_list = jsonRequest.getJSONArray("Bank_details");
                                        bankdetailHashMap = new ArrayList<HashMap<String, String>>();
                                        for (int iStatusList = 0; iStatusList < product_list.length(); iStatusList++) {
                                            JSONObject product = product_list.getJSONObject(iStatusList);
                                            HashMap<String, String> userhashmap = new HashMap<String, String>();
                                            userhashmap.put("id", product.getString("id"));
                                            userhashmap.put("ac_no", product.getString("bank_ac_no"));
                                            userhashmap.put("branch",  product.getString("bank_branch"));
                                            userhashmap.put("bank_name", product.getString("bank_name"));
                                            userhashmap.put("ifsc", product.getString("bank_ifsc"));
                                            bankdetailHashMap.add(userhashmap);

                                        }
                                        invoicedb.insertbankdetails(bankdetailHashMap);
                                    }


                                    if (json.isNull("Item_list")) {

                                    }
                               /* if(product_list.length()==0){


                                }*/
                                    else {
                                        JSONArray product_list = jsonRequest.getJSONArray("Item_list");
                                        productdetailHashMap = new ArrayList<HashMap<String, String>>();
                                        for (int iStatusList = 0; iStatusList < product_list.length(); iStatusList++) {
                                            JSONObject product = product_list.getJSONObject(iStatusList);
                                            HashMap<String, String> userhashmap = new HashMap<String, String>();
                                            userhashmap.put("product_code", product.getString("product_code"));
                                            userhashmap.put("product_desc", product.getString("product_desc"));
                                            userhashmap.put("product_rate",  product.getString("product_rate"));

                                            userhashmap.put("user_id", product.getString("user_id"));
                                            userhashmap.put("product_unit", product.getString("product_unit"));
                                            userhashmap.put("Hsn", product.getString("Hsn"));
                                            userhashmap.put("prod_gst_rate", product.getString("prod_gst_rate"));
                                            userhashmap.put("cess", product.getString("cess"));
                                            userhashmap.put("unit_pos", product.getString("unit_pos"));
                                            productdetailHashMap.add(userhashmap);

                                        }
                                       invoicedb.insertProductdetails(productdetailHashMap);
                                    }


                                    if (json.isNull("supplier_details")) {

                                    } else {
                                        JSONArray customer_list = jsonRequest.getJSONArray("supplier_details");
                                        supplierdetailHashMap = new ArrayList<HashMap<String, String>>();
                                        for (int iStatusList = 0; iStatusList < customer_list.length(); iStatusList++) {
                                            JSONObject supplier = customer_list.getJSONObject(iStatusList);
                                            HashMap<String, String> userhashmap = new HashMap<String, String>();

                                            userhashmap.put("id", supplier.getString("id"));
                                            userhashmap.put("supplier_name", supplier.getString("supplier_name"));
                                            userhashmap.put("gst_no", supplier.getString("Gst_no"));
                                            userhashmap.put("user_id", supplier.getString("user_id"));
                                            userhashmap.put("address", supplier.getString("address"));
                                            userhashmap.put("address1", supplier.getString("address1"));
                                            userhashmap.put("city", supplier.getString("city"));
                                            userhashmap.put("state", supplier.getString("state"));
                                            userhashmap.put("pincode", supplier.getString("pincode"));
                                            userhashmap.put("phone_no", supplier.getString("phone_no"));
                                            userhashmap.put("mail_id", supplier.getString("mail_id"));
                                            userhashmap.put("spn_state_position", supplier.getString("spn_state_position"));
                                            userhashmap.put("opening_balance", supplier.getString("opening_balance"));
                                            userhashmap.put("opening_balance_crordr", supplier.getString("openingbal_crordr"));

                                            supplierdetailHashMap.add(userhashmap);

                                        }
                                        invoicedb.insertSupplierdetails(supplierdetailHashMap);
                                    }


                                    if(prefs.getLogo().isEmpty()) {

                                    }
                                    else{

                                        new DownloadImage().execute(prefs.getLogo());
                                    }

                                    if(prefs.getLogInType().equals("0")){
                                        Intent i = new Intent(SigninActivity.this, profile.class);
                                        startActivity(i);
                                        Toast toast = new Toast(getApplicationContext());
                                        toast.setDuration(Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                        toasttext.setText(status);
                                        toast.setView(layout);
                                        progressDialog.dismiss();
                                        toast.show();

                                    }
                                    else {
                                        Intent i = new Intent(SigninActivity.this, MainActivity.class);
                                        prefs.setIsLoggedIn("2");
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(i);
                                        Toast toast = new Toast(getApplicationContext());
                                        toast.setDuration(Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                        toasttext.setText(status);
                                        toast.setView(layout);
                                        progressDialog.dismiss();
                                        toast.show();
                                        finish();
                                    }


                                }


                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), "Error parsing", Toast.LENGTH_LONG).show();
                            }


                            progressDialog.dismiss();
                          /*  LongOperation longOperation=new LongOperation();
                            longOperation.execute();*/

                        }
                    },


                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            errorhandler e = new errorhandler();
                            String msg = e.errorhandler(error);

                            Toast toast = new Toast(getApplicationContext());
                            toast.setDuration(Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toasttext.setText(msg);
                            toast.setView(layout);
                            progressDialog.dismiss();
                            toast.show();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_name", str_username);
                    params.put("password", str_pwd);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };

            requestQueue.add(stringRequest);
        }

        }
    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }
    public static String fixEncoding(String response) {
        try {
            byte[] u = response.toString().getBytes(
                    "ISO-8859-1");
            response = new String(u, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return response;
    }

  /*  private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            invoicedb.insertUserdetails(userdetailHashMap);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            Intent i=new Intent(SigninActivity.this, MainActivity.class);
            startActivity(i);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(con, R.style.MyAlertDialogStyle);
            progressDialog.setMessage("inserting data");
            progressDialog.show();


        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
*/
  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String permissions[], int[] grantResults) {
      switch (requestCode) {
          case REQUEST_PERMISSIONS: {
              if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                  signin.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View v) {
                          signin();

                      }
                  });
              }
              else {
                  Toast.makeText(getApplicationContext(),"Please give storage permission ",Toast.LENGTH_LONG).show();
                  finish();

              }
              return;
          }


      }
  }


    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        private String TAG = "DownloadImage";
        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d(TAG, "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        protected void onPostExecute(Bitmap result) {
            saveToExternalStorage( result);
        }
    }


    private String saveToExternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File mypath;

        File filepath = Environment.getExternalStorageDirectory();

        // Create a new folder in SD Card
        File dir = new File(filepath.getAbsolutePath()
                + "/BizManager/");
        dir.mkdirs();

        File dir1 = new File(filepath.getAbsolutePath()
                + "/BizManager/Invoice/");
        dir1.mkdirs();

        mypath = new File(dir, "profilepic.png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                fos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dir.getAbsolutePath();
    }





    }






