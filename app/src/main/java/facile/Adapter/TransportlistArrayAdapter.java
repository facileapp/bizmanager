package facile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.Transport_model;
import facile.invoice.R;

/**
 * Created by pradeep on 10/10/17.
 */
public class TransportlistArrayAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<Transport_model> items;
    private final int mResource;

    public TransportlistArrayAdapter(Context context, int resource,
                                  ArrayList<Transport_model> objects) {

        items = objects;
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;

    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView id = (TextView) view.findViewById(R.id.tv_dealerid);
        TextView mode = (TextView) view.findViewById(R.id.tv_dealertype);
        mode.setText(items.get(position).mode);
        id.setText(items.get(position).id);

        mode.setTextSize(12);
        return view;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}

