package facile.creditanddebitnote;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.AutocompletesupplierAdapter;
import facile.Adapter.additemadapter;
import facile.Model.Debit_note_purchase_details;
import facile.Model.Invoice_calc;
import facile.Model.Supplier_model;
import facile.Model.invoice_customer_model;
import facile.invoice.Additem_fragment;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.SeparatorDecoration;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 27/8/17.
 */

public class Debit_note_purchase extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public  String check_first_time="0";
    Invoicedatabase invoicedb;
    String Str_customer_name,gst_no,address,address1,phone_no,mail_id,city,state,pincode,state_pos,
            str_invoiceno;
    EditText edt_address,edt_address1,edt_pincode,edt_city,edt_state;
    TextView toasttext;
    String str_date;
    View layouttoast;
    Button submit, additem;
    EditText edt_inv_date;
    TextView txt_gstno;
    RecyclerView mRecyclerView;
    AutocompletesupplierAdapter adapter;
    TextView tv_debitnoteno,tv_debitnotedate;

    RequestQueue requestQueue;

    String  str_dateval;
    EditText edt_invoiceno;
    ProgressDialog progressDialog;
    String url;
    additemadapter Itemadapter;
    String str_igst="N";
    double temp;

    prefManager prefs;
    public static final String UTF8_BOM = "\uFEFF";
    private OnFragmentInteractionListener mListener;
    private RecyclerView.LayoutManager mLayoutManager;
    String str_Debitnoteno;



    public Debit_note_purchase() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Debit_note_purchase newInstance(String param1, String param2) {
        Debit_note_purchase fragment = new Debit_note_purchase();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.debit_note_purchase,container, false);
        invoicedb=new Invoicedatabase(getContext());
        prefs=new prefManager(getContext());
        additem = (Button) view.findViewById(R.id.btn_additem);
        submit = (Button) view.findViewById(R.id.btn_submit);
        edt_invoiceno=(EditText)view.findViewById(R.id.tv_invoicenumber);
        tv_debitnoteno=(TextView)view.findViewById(R.id.tv_debitnotenumber) ;
        tv_debitnotedate=(TextView)view.findViewById(R.id.tv_debitnotedate);
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_invoiceitems);
        txt_gstno = (TextView) view.findViewById(R.id.txt_gstno);
        txt_gstno.setText(gst_no);
        edt_address=(EditText)view.findViewById(R.id.edt_address);
        edt_address1=(EditText)view.findViewById(R.id.edt_address1);
        edt_city=(EditText)view.findViewById(R.id.edt_city);
        edt_pincode=(EditText)view.findViewById(R.id.edt_pincode);
        edt_state=(EditText)view.findViewById(R.id.edt_state);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        SeparatorDecoration decoration = new SeparatorDecoration(getContext(), Color.GRAY,0.5f);
        mRecyclerView.addItemDecoration(decoration);
        final AutoCompleteTextView edt_customername = (AutoCompleteTextView) view.findViewById(R.id.edt_suppliername);
        edt_customername.setThreshold(1);
        long date = System.currentTimeMillis();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
        dateFormat.applyPattern("dd/MM/yy");
        String dateString = dateFormat.format(date);
        tv_debitnotedate.setText(dateString);
        str_date=dateString;
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy:MM:dd");
        dateFormat1.applyPattern("yyyy/MM/dd");
        str_dateval=dateFormat1.format(date);


        if(check_first_time.equals("0")){
            getdebitnoteno(view);
            check_first_time="1";
        }


        tv_debitnoteno.setText(str_Debitnoteno);

        Itemadapter = new additemadapter(invoicedb.getaddproductitems(),getActivity());
        mRecyclerView.setAdapter(Itemadapter);
        ((additemadapter) Itemadapter).setOnItemClickListener(new additemadapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Additem_fragment fragment = new Additem_fragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("prod_code",invoicedb.getaddproductitems().get(position).productcode);
                bundle.putString("prod_desc",invoicedb.getaddproductitems().get(position).productdesc);
                bundle.putString("prod_fees",invoicedb.getaddproductitems().get(position).amount);
                bundle.putString("prod_qty",invoicedb.getaddproductitems().get(position).quantity);
                bundle.putString("prod_disc",invoicedb.getaddproductitems().get(position).discount);
                bundle.putString("prod_disc_type",invoicedb.getaddproductitems().get(position).discountype);
                bundle.putString("prod_unit",invoicedb.getaddproductitems().get(position).unit);
                bundle.putString("gst_rate",invoicedb.getaddproductitems().get(position).gstrate);
                bundle.putString("Igst",str_igst);
                bundle.putString("type","I");
                bundle.putString("business_type",prefs.getBusinessType());
                bundle.putString("type","dr_note_p");
                fragment.setArguments(bundle);
                fragmentTransaction.commit();
            }
        });

        additem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strcustomer=edt_customername.getText().toString();
                if(TextUtils.isEmpty(strcustomer)){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Please select customer to add items");
                    toast.setView(layouttoast);
                    toast.show();
                    return;

                }
                else  if (invoicedb.searchsuppliername(edt_customername.getText().toString()) == false) {
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Supplier not found");
                    edt_address.setText("");
                    edt_address1.setText("");
                    edt_city.setText("");
                    edt_state.setText("");
                    edt_pincode.setText("");
                    txt_gstno.setText("");
                    toast.setView(layouttoast);
                    toast.show();
                    return;
                }
                else {
                    Additem_fragment fragment = new Additem_fragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("Igst",str_igst);
                    bundle.putString("type","dr_note_p");
                    bundle.putString("business_type",prefs.getBusinessType());
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();
                }

            }
        });



        adapter = new AutocompletesupplierAdapter(getContext(),R.layout.autocomplete_row,invoicedb.getsupplierdetails());
        edt_customername.setAdapter(adapter);
        edt_customername.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                Supplier_model model = (Supplier_model) view.getTag();
                edt_customername.setText(model.supplier_name);
                if(model.state_pos.equals(prefs.getState())){
                    str_igst="N";
                }
                else{
                    str_igst="Y";
                }
                Str_customer_name=model.supplier_name;
                gst_no=model.gst_no;
                phone_no=model.phone_no;
                mail_id=model.mail_id;
                address=model.address;
                address1=model.address1;
                city=model.city;
                state=model.state;
                pincode=model.pincode;
                state_pos=model.state_pos;
               invoicedb.deleteaddproductitems();
                Itemadapter = new additemadapter(invoicedb.getaddproductitems(),getActivity());
                mRecyclerView.setAdapter(Itemadapter);
                txt_gstno.setText(model.gst_no);
                edt_address.setText(model.address);
                edt_address1.setText(model.address1);
                edt_city.setText(model.city);
                edt_state.setText(model.state);
                edt_pincode.setText(model.pincode);
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              str_invoiceno=edt_invoiceno.getText().toString();

                if(TextUtils.isEmpty(str_invoiceno)){


                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Invoice No can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    return;

                }

                int m=invoicedb.getaddproductitems().size();
                for(int i=0;i<m;i++){
                    int x= Integer.parseInt(invoicedb.getaddproductitems().get(i).gstrate);
                    if(x>temp){
                        temp=x;
                    }
                }


                if(invoicedb.getaddproductitemscount()==0){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Add Items can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    return;
                }


                else {


                    String total_before_tax;
                    String total_after_tax;
                    String str_cgst, str_sgst, str_igstval;

                    double totbeforetax = 0, totaftertax = 0, cgst = 0, sgst = 0, igst = 0;


                    for (int i = 0; i < invoicedb.getaddproductitemscount(); i++) {
                        double TOT_BT = totbeforetax + Double.parseDouble(invoicedb.getaddproductitems().get(i).grossamount);
                        double TOT_AT = totaftertax + Double.parseDouble(invoicedb.getaddproductitems().get(i).tat);
                        double CGST = cgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).cgst);
                        double SGST = sgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).sgst);
                        double IGST = igst + Double.parseDouble(invoicedb.getaddproductitems().get(i).igst);
                        totbeforetax = TOT_BT;
                        totaftertax = TOT_AT;
                        cgst = CGST;
                        sgst = SGST;
                        igst = IGST;
                    }
                    total_before_tax=String.valueOf(Math.round((totbeforetax)*100.0)/100.0);
                    total_after_tax=String.valueOf(Math.round((totaftertax)*100.0)/100.0);
                    str_cgst=String.valueOf(Math.round(cgst*100.0)/100.0);
                    str_sgst=String.valueOf(Math.round(sgst*100.0)/100.0);
                    str_igstval=String.valueOf(Math.round(igst*100.0)/100.0);
                    invoice_customer_model invoice_customer_model = new invoice_customer_model
                            ( Str_customer_name, gst_no, address,address1, phone_no, mail_id,city, state, pincode, state_pos, str_igst);
                    Debit_note_purchase_details debit_note=new Debit_note_purchase_details(str_Debitnoteno,str_dateval,str_invoiceno);
                    Invoice_calc credit_note_calc=new Invoice_calc("0","0",str_cgst,str_sgst,str_igstval,total_before_tax,total_after_tax);
                    Intent myIntent = new Intent(getActivity(),Credit_note_preview.class);
                    myIntent.putExtra("Debitp","P");
                    myIntent.putExtra("customer",invoice_customer_model);
                    myIntent.putExtra("debit_note",debit_note);
                    myIntent.putExtra("credit_note_calc",credit_note_calc);
                    getActivity().startActivity(myIntent);

                }
            }
        });

     return view;

    }






    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
    @Override
    public void onResume() {
        super.onResume();
      /*  AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle("Invoice");*/
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public  void getdebitnoteno(final View view){
        url = Constants.DEBIT_NOTE_NUMBER_PURCHASE;
        requestQueue = Volley.newRequestQueue(getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Generating invoice no...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String m=response.toString();
                            String x=     removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                          
                            if (statuscode.equals("200")) {
                                str_Debitnoteno=jsonRequest.getString("debit_note_no");

                                tv_debitnoteno.setText(str_Debitnoteno);
                               progressDialog.dismiss();

                            }


                            else   if (statuscode.equals("400")){
                                Toast.makeText(getContext(),jsonRequest.getString("status"),Toast.LENGTH_LONG).show();
                            }


                        } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                            progressDialog.dismiss();


                        }
                    }
                },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    /*    errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layout);
                        progressDialog.dismiss();
                        toast.show();*/

                        errorhandler e = new errorhandler();
                        String msg = e.errorhandler(error);
                        progressDialog.dismiss();
                        Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();

                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",prefs.getUserId());
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }



}
