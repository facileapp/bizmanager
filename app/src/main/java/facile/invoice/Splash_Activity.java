package facile.invoice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import facile.Signin.SigninActivity;
import facile.util.prefManager;

/**
 * Created by pradeep on 13/4/18.
 */

public class Splash_Activity extends Activity {

    prefManager prefs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        prefs=new prefManager(this);

// decide here whether to navigate to Login or Main Activity


        if (prefs.getIsLoggedIn().equals("2")) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, SigninActivity.class);
            startActivity(intent);
            finish();
        }
    }

}