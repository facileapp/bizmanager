package facile.Reports;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;

import facile.Adapter.MonthlistArrayAdapter;
import facile.Adapter.YearlistArrayAdapter;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Report_closure.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Report_closure#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Month_wise_report extends Fragment   {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    Spinner spn_month,spn_year;
    Button btn_reportcloasure;
    String str_year,str_month;
    Invoicedatabase invoicedatabase;
    String str_crordr,str_chart,str_sorp,str_type,str_cr_sorp;


    private Report_closure.OnFragmentInteractionListener mListener;

    public Month_wise_report() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Report_closure.
     */
    // TODO: Rename and change types and number of parameters
    public static Month_wise_report newInstance(String param1, String param2) {
        Month_wise_report fragment = new Month_wise_report();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_report_closure, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.monthwise_report));
        spn_month=(Spinner)view.findViewById(R.id.spn_month);
        spn_year=(Spinner)view.findViewById(R.id.spn_year);
        btn_reportcloasure=(Button)view.findViewById(R.id.btn_reportcloasure);
        btn_reportcloasure.setText(R.string.generatereport);
        Bundle bundle = this.getArguments();
        if(bundle!=null){
            if(bundle.containsKey("CRORDR")) {
                str_crordr = bundle.getString("CRORDR","");
                str_type=bundle.getString("str_type", "");
                if(str_crordr.equals("DR")) {
                    str_cr_sorp = bundle.getString("DR_SORP", "");
               }

            }

            else if(bundle.containsKey("SORP")) {
                str_sorp= bundle.getString("SORP", "");
                str_type= bundle.getString("str_type", "");
           }

        }


        invoicedatabase=new Invoicedatabase(getContext());
        final ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        int thisMonth = Calendar.getInstance().get(Calendar.MONTH);
        for (int i = thisYear-1; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
      final  MonthlistArrayAdapter adapter1 = new MonthlistArrayAdapter(getContext(),
                R.layout.state_list_row, invoicedatabase.getmonth(12));
        spn_month.setAdapter(adapter1);
        spn_month.setSelection(thisMonth-1);

 spn_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

               str_month=String.valueOf(position+1);
                spn_month.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });



      final   YearlistArrayAdapter adapter = new YearlistArrayAdapter(getActivity(),
                R.layout.year_row, years);
        spn_year.setAdapter(adapter);
        spn_year.setSelection(1);


   spn_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                str_year=years.get(position);
                spn_year.setSelection(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        btn_reportcloasure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  if(str_chart!=null||!str_chart.isEmpty()){
                      Month_wise_chart_view fragment = new Month_wise_chart_view();
                      FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                      FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                      fragmentTransaction.replace(R.id.container_body, fragment);
                      fragmentTransaction.addToBackStack(null);
                      Bundle bundle = new Bundle();
                      bundle.putString("str_year", str_year);
                      bundle.putString("str_month", str_month);

                      fragment.setArguments(bundle);
                      fragmentTransaction.commit();
                }
                  else */
              if(str_type.equals("S")){

                          Month_wise_report_view fragment = new Month_wise_report_view();
                          FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                          FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                          fragmentTransaction.replace(R.id.container_body, fragment);
                          fragmentTransaction.addToBackStack(null);
                          Bundle bundle = new Bundle();
                          bundle.putString("str_year", str_year);
                          bundle.putString("str_month", str_month);
                          bundle.putString("SORP", str_sorp);

                          fragment.setArguments(bundle);
                          fragmentTransaction.commit();
                      } else  if(str_type.equals("CR")) {
                          CrDr_month_wise_report_view fragment = new CrDr_month_wise_report_view();
                          FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                          FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                          fragmentTransaction.replace(R.id.container_body, fragment);
                          fragmentTransaction.addToBackStack(null);
                          Bundle bundle = new Bundle();
                          bundle.putString("str_year", str_year);
                          bundle.putString("str_month", str_month);
                          bundle.putString("CRORDR", str_crordr);


                  if(str_crordr.equals("DR")) {

                  bundle.putString("DR_SORP",str_cr_sorp);
                  }



                  fragment.setArguments(bundle);
                          fragmentTransaction.commit();

                      }






            }
        });
        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onResume() {
        super.onResume();

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }




}
